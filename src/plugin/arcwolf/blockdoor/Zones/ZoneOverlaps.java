package plugin.arcwolf.blockdoor.Zones;

public class ZoneOverlaps{

    public String type = "", trigger = "", creator = "", name = "", world = "";
    public Zone zone;

    public ZoneOverlaps(Zone in_zone) {
        zone = in_zone;
        type = zone.zone_Type;
        trigger = zone.uzone_trigger;
        creator = zone.zone_creator;
        name = zone.zone_name;
        world = zone.zone_world;
    }

    public ZoneOverlaps(String in_string) {
        String[] split = in_string.split(",");
        creator = split[0];
        name = split[1];
        world = split[2];
        type = split[3];
        if (split.length == 5) {
            trigger = split[4];
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((world == null) ? 0 : world.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ZoneOverlaps other = (ZoneOverlaps) obj;
        if (creator == null) {
            if (other.creator != null) return false;
        }
        else if (!creator.equals(other.creator)) return false;
        if (name == null) {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (trigger == null) {
            if (other.trigger != null) return false;
        }
        else if (!trigger.equals(other.trigger)) return false;
        if (type == null) {
            if (other.type != null) return false;
        }
        else if (!type.equals(other.type)) return false;
        if (world == null) {
            if (other.world != null) return false;
        }
        else if (!world.equals(other.world)) return false;
        return true;
    }
}
