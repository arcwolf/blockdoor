package plugin.arcwolf.blockdoor.Zones;

/*
 * Handles searching of zones, adding new zones to correct lists, and creating zones.
 */

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;

public class ZoneHelper {

    public BlockDoor plugin;
    public DataWriter dataWriter;

    private enum zoneType {
        ZONE, MYZONE, UZONE, AZONE, MZONE, EZONE, PZONE
    }

    public ZoneHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public Zone addZone(String in_name, String in_creator, String in_trigger, String in_world, Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        String zoneType = friendlyCommandToType(settings);
        Zone newZone = findZone(zoneType, in_name, in_creator, in_trigger, in_world);
        if (newZone == null) {
            newZone = new Zone(zoneType, in_name, in_creator, in_trigger, in_world);
            dataWriter.allZonesMap.put(newZone, newZone);
            settings.notFound = 1;
        }
        return newZone;
    }

    /**
     * 
     * @param in_type
     * @param in_name
     * @param in_creator
     * @param in_trigger
     * @param in_world
     * @return Zone or null
     */
    public Zone findZone(String in_type, String in_name, String in_creator, String in_trigger, String in_world) {
        return dataWriter.allZonesMap.get(new Zone(in_type, in_name, in_creator, in_trigger, in_world));
    }

    public Zone findZone(Zone zone) {
        return findZone(zone.zone_Type, zone.zone_name, zone.zone_creator, zone.uzone_trigger, zone.zone_world);
    }

    public Zone findZone(ZoneOverlaps zo) {
        return findZone(zo.type, zo.name, zo.creator, zo.trigger, zo.world);
    }

    public int findLink(Zone z, Door d) {
        int index = -1;
        for(int j = 0; j < z.links.size(); j++)
            if (z.links.get(j).door.equals(d))
                return j;
        return index;
    }

    public void deleteOverlaps(Zone zone) {
        for(ZoneOverlaps overlap : zone.overlaps.values()) {
            for(ZoneOverlaps o : overlap.zone.overlaps.values()) {
                if (o.zone.equals(zone)) {
                    overlap.zone.overlaps.remove(o);
                    break;
                }
            }
        }
    }

    /**
     * Creates a zone in the specified zone list.
     * 
     * @param zoneList
     * @param settings
     * @param player
     * @param blockClicked
     * @param isUzone
     */
    public void createZone(BlockDoorSettings settings, Player player, Block blockClicked) {
        Zone foundZone = addZone(settings.name, player.getName(), settings.trigger, player.getWorld().getName(), player);
        int blockX = (int) blockClicked.getLocation().getX();
        int blockY = (int) blockClicked.getLocation().getY();
        int blockZ = (int) blockClicked.getLocation().getZ();
        if (settings.select == 1) {
            settings.zone = foundZone;
            player.sendMessage("Setting " + foundZone.zone_Type.toLowerCase() + " start: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
            settings.startX = blockX;
            settings.startY = blockY;
            settings.startZ = blockZ;

            settings.select = 2;
        }
        else if (settings.select == 2) {
            player.sendMessage("Setting " + foundZone.zone_Type.toLowerCase() + " end: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
            if (settings.startX > blockX) {
                settings.endX = settings.startX;
                settings.startX = blockX;
            }
            else {
                settings.endX = blockX;
            }

            if (settings.startY > blockY) {
                settings.endY = settings.startY;
                settings.startY = blockY;
            }
            else {
                settings.endY = blockY;
            }

            if (settings.startZ > blockZ) {
                settings.endZ = settings.startZ;
                settings.startZ = blockZ;
            }
            else {
                settings.endZ = blockZ;
            }

            int xDist = Math.abs(settings.endX - settings.startX);
            int yDist = Math.abs(settings.endY - settings.startY);
            int zDist = Math.abs(settings.endZ - settings.startZ);

            if (dataWriter.getMax_ZoneSize() != -1 && ((xDist + 1 > dataWriter.getMax_ZoneSize()) || (yDist + 1 > dataWriter.getMax_ZoneSize()) || (zDist + 1 > dataWriter.getMax_ZoneSize()))) {
                player.sendMessage(ChatColor.RED + foundZone.zone_Type + " DIMENSIONS REJECTED for: '" + ChatColor.WHITE + settings.name + ChatColor.RED + "'");
                if (!foundZone.coordsSet) {
                    dataWriter.allZonesMap.remove(foundZone);
                }
                BlockDoorSettings.clearSettings(player);
            }
            else {
                if (foundZone.coordsSet) { // Update blockmap with old Zone
                    dataWriter.updateOldZoneBlockMap(foundZone);
                }
                boolean found = false;
                Map<Block, Zone> tempBlockMap = new HashMap<Block, Zone>();
                HashMap<ZoneOverlaps, ZoneOverlaps> tempOverlaps = new HashMap<ZoneOverlaps, ZoneOverlaps>();
                Zone tempZone = null;
                search:
                for(int x = settings.startX; x <= settings.endX; x++)
                    for(int z = settings.startZ; z <= settings.endZ; z++)
                        for(int y = settings.startY; y <= settings.endY; y++) {
                            Block block = foundZone.world.getBlockAt(x, y, z);
                            Zone zoneBlock = dataWriter.zoneBlockMap.get(block);
                            if (zoneBlock == null) {
                                tempBlockMap.put(block, foundZone);
                                continue;
                            }
                            else {
                                Zone testZone = zoneBlock;
                                if (!testZone.equals(foundZone)) {
                                    if (!dataWriter.isOverlapZones() || !testZone.zone_creator.equals(foundZone.zone_creator)) {
                                        tempZone = testZone;
                                        found = true;
                                        break search;
                                    }
                                    else {
                                        for(Zone zones : dataWriter.allZonesMap.values()) {
                                            if (!zones.equals(foundZone) && zones.zoneOverlap(x, y, z)) {
                                                ZoneOverlaps zo = new ZoneOverlaps(zones);
                                                tempOverlaps.put(zo, zo);
                                            }
                                        }
                                        tempBlockMap.put(block, foundZone);
                                    }
                                }
                            }
                        }
                if (found) {
                    if (tempZone != null) {
                        showOverlapMsg(tempZone, player);
                    }
                    player.sendMessage(ChatColor.RED + foundZone.zone_Type + " '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' REJECTED");
                    if (!foundZone.coordsSet) {
                        dataWriter.allZonesMap.remove(foundZone);
                    }
                    BlockDoorSettings.clearSettings(player);
                }
                else {
                    // Update zone coordinates
                    foundZone.updateCoordinates(settings);
                    
                    // Remove old overlaps
                    for(ZoneOverlaps zo : foundZone.overlaps.values()) {
                        zo.zone.overlaps.remove(new ZoneOverlaps(foundZone));
                    }

                    // Update zone overlaps
                    foundZone.overlaps = tempOverlaps;

                    // Update new overlaps of doors
                    for(ZoneOverlaps zo : foundZone.overlaps.values()) {
                        Zone z = zo.zone;
                        ZoneOverlaps o = new ZoneOverlaps(foundZone);
                        z.overlaps.put(o, o);
                    }

                    // Update zoneblockmap
                    dataWriter.zoneBlockMap.putAll(tempBlockMap);
                    foundZone.setInitialized(false);

                    // Cleanup and save
                    foundZone.coordsSet = true;
                    foundZone.occupants = 0;
                    BlockDoorSettings.clearSettings(player);
                    dataWriter.saveDatabase(false);
                }
            }
        }
    }

    private void showOverlapMsg(Zone foundZone, Player player) {
        player.sendMessage("Overlap of " + getFriendlyZoneName(foundZone) + " [" + ChatColor.GREEN + foundZone.zone_name + ChatColor.WHITE + "] Created by [" +
                ChatColor.GREEN + foundZone.zone_creator + ChatColor.WHITE + "]");
        player.sendMessage("Starting at [X= " + ChatColor.AQUA + foundZone.zone_start_x + ChatColor.WHITE + " Y= " +
                ChatColor.AQUA + foundZone.zone_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                foundZone.zone_start_z + ChatColor.WHITE + "]");
        player.sendMessage("Ending at [X= " + ChatColor.AQUA + foundZone.zone_end_x + ChatColor.WHITE + " Y= " +
                ChatColor.AQUA + foundZone.zone_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                foundZone.zone_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                foundZone.zone_world + ChatColor.WHITE + "]");
    }

    public String getFriendlyZoneName(Zone foundZone) {
        if (foundZone.zone_Type.equals("AZONE"))
            return "Aggressive Mob Zone";
        if (foundZone.zone_Type.equals("EZONE"))
            return "Living Entity Zone";
        if (foundZone.zone_Type.equals("MZONE"))
            return "Mob Zone";
        if (foundZone.zone_Type.equals("MYZONE"))
            return "My Zone";
        if (foundZone.zone_Type.equals("PZONE"))
            return "Passive Mob Zone";
        if (foundZone.zone_Type.equals("ZONE"))
            return "Player Zone";
        if (foundZone.zone_Type.equals("UZONE"))
            return "Selected Entity Zone";
        return "";
    }

    public int getNumberOfZoneTypes() {
        zoneType[] z = zoneType.values();
        return z.length;
    }

    private String friendlyCommandToType(BlockDoorSettings settings) {
        if (settings.friendlyCommand.equals("dazone"))
            return "AZONE";
        else if (settings.friendlyCommand.equals("dezone"))
            return "EZONE";
        else if (settings.friendlyCommand.equals("dmzone"))
            return "MZONE";
        else if (settings.friendlyCommand.equals("dmyzone"))
            return "MYZONE";
        else if (settings.friendlyCommand.equals("dpzone"))
            return "PZONE";
        else if (settings.friendlyCommand.equals("duzone"))
            return "UZONE";
        else
            return "ZONE";
    }
}