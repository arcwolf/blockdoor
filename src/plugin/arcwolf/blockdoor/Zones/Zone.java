package plugin.arcwolf.blockdoor.Zones;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Link;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.ChunkCoords;

public class Zone {

    public String zone_Type = "";
    public String zone_name = "";
    public String zone_creator = "";
    public String uzone_trigger = "";
    public int occupants;

    public int zone_start_x;
    public int zone_start_y;
    public int zone_start_z;
    public int zone_end_x;
    public int zone_end_y;
    public int zone_end_z;
    public boolean coordsSet;
    public Map<ChunkCoords, ChunkCoords> zoneChunk = new HashMap<ChunkCoords, ChunkCoords>();
    private boolean initialized = false;
    //private boolean chunkUnloaded = false;

    public World world;

    public List<Link> links = new ArrayList<Link>();
    public Map<ZoneOverlaps, ZoneOverlaps> overlaps = new HashMap<ZoneOverlaps, ZoneOverlaps>();
    public String zone_world = "";

    public Zone(String in_Type, String in_name, String in_creator, String in_trigger, String in_world) {
        zone_Type = in_Type;
        zone_name = in_name;
        zone_creator = in_creator;
        zone_world = in_world;
        world = BlockDoor.plugin.getWorld(zone_world);
        uzone_trigger = in_trigger;
        coordsSet = false;
    }

    public Zone(Zone otherZone) {
        zone_Type = otherZone.zone_Type;
        zone_name = otherZone.zone_name;
        zone_creator = otherZone.zone_creator;
        uzone_trigger = otherZone.uzone_trigger;
        occupants = otherZone.occupants;
        zone_start_x = otherZone.zone_start_x;
        zone_start_y = otherZone.zone_start_y;
        zone_start_z = otherZone.zone_start_z;
        zone_end_x = otherZone.zone_end_x;
        zone_end_y = otherZone.zone_end_y;
        zone_end_z = otherZone.zone_end_z;
        links.addAll(otherZone.links);
        zone_world = otherZone.zone_world;
        world = BlockDoor.plugin.getWorld(zone_world);
        coordsSet = otherZone.coordsSet;
    }

    // Tests for overlap with provided coordinates.
    public boolean zoneOverlap(int x, int y, int z) {
        return (zone_start_x <= x && x <= zone_end_x &&
                zone_start_y <= y && y <= zone_end_y &&
                zone_start_z <= z && z <= zone_end_z);
    }

    public Zone(String in_string) {
        String[] split = in_string.split(":");
        zone_Type = split[0];

        zone_creator = split[1];
        zone_name = split[2];

        uzone_trigger = split[3];

        zone_start_x = Integer.parseInt(split[4]);
        zone_start_y = Integer.parseInt(split[5]);
        zone_start_z = Integer.parseInt(split[6]);

        zone_end_x = Integer.parseInt(split[7]);
        zone_end_y = Integer.parseInt(split[8]);
        zone_end_z = Integer.parseInt(split[9]);
        coordsSet = Boolean.parseBoolean(split[10]);
        occupants = Integer.parseInt(split[11]);

        zone_world = split[12];
        world = BlockDoor.plugin.getWorld(zone_world);
        if (split.length == 14) {
            String[] linksplit = split[13].split("\\|");
            Link l;
            for(int i = 0; i < linksplit.length; i++) {
                l = new Link(linksplit[i], zone_world);
                if (l.link_creator != "FAILED")
                        links.add(l);
            }
        }
        else if (split.length == 15) {
            String[] linksplit = split[13].split("\\|");
            Link l;
            for(int i = 0; i < linksplit.length; i++) {
                l = new Link(linksplit[i], zone_world);
                if (l.link_creator != "FAILED")
                    links.add(l);
            }
            String[] overlapSplit = split[14].split("\\|");
            ZoneOverlaps zo;
            for(int i = 0; i < overlapSplit.length; i++) {
                zo = new ZoneOverlaps(overlapSplit[i]);
                overlaps.put(zo, zo);
            }
        }
        else if (split.length < 13 || split.length > 15) {
            zone_creator = "FAILED";
            coordsSet = false;
        }
    }

    public void updateCoordinates(BlockDoorSettings settings) {
        zone_start_x = settings.startX;
        zone_start_y = settings.startY;
        zone_start_z = settings.startZ;
        zone_end_x = settings.endX;
        zone_end_y = settings.endY;
        zone_end_z = settings.endZ;
    }

    public void processLinks() {
        for(Link l : links) {
            l.process();
        }
    }

    public boolean isInZone(Location l) {
        int x = l.getBlockX();
        int y = l.getBlockY();
        int z = l.getBlockZ();
        return isInZone(x, y, z);
    }

    public boolean isInZone(int x, int y, int z) {
        return (zone_start_x <= x && x <= zone_end_x &&
                zone_start_y <= y && y <= zone_end_y &&
                zone_start_z <= z && z <= zone_end_z);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(zone_Type);
        builder.append(":");
        builder.append(zone_creator);
        builder.append(":");
        builder.append(zone_name);
        builder.append(":");

        builder.append(uzone_trigger);
        builder.append(":");

        builder.append(Integer.toString(zone_start_x));
        builder.append(":");
        builder.append(Integer.toString(zone_start_y));
        builder.append(":");
        builder.append(Integer.toString(zone_start_z));
        builder.append(":");

        builder.append(Integer.toString(zone_end_x));
        builder.append(":");
        builder.append(Integer.toString(zone_end_y));
        builder.append(":");
        builder.append(Integer.toString(zone_end_z));
        builder.append(":");

        builder.append(Boolean.toString(coordsSet));
        builder.append(":");

        builder.append(Integer.toString(occupants));
        builder.append(":");

        builder.append(zone_world);
        builder.append(":");

        for(Link l : links) {
            if (l.door == null) {
                BlockDoor.LOGGER.info(zone_Type + " named: " + zone_name + (zone_Type.equals("UZONE") ? "Triggerd by " + uzone_trigger : "") + " linked to " + l.doorType.name() + " door called " + l.link_name);
                BlockDoor.LOGGER.info("Location: " + zone_world + ": " + zone_start_x + "," + zone_start_y + "," + zone_start_z + " to " + zone_end_x + "," + zone_end_y + "," + zone_end_z + " and was set to " + l.linkType.name());
                BlockDoor.LOGGER.info("The door doesnt exist! The link was not saved!");
                BlockDoor.LOGGER.info("*************************");
                continue;
            }
            builder.append(l.link_name);
            builder.append(" ");
            builder.append(l.link_creator);
            builder.append(" ");
            builder.append(l.linkType.name());
            builder.append(" ");
            builder.append(l.door.door_type.name());
            builder.append("|");
        }
        builder.append(":");

        for(ZoneOverlaps zo : overlaps.values()) {
            builder.append(zo.creator);
            builder.append(",");
            builder.append(zo.name);
            builder.append(",");
            builder.append(zo.world);
            builder.append(",");
            builder.append(zo.type);
            builder.append(",");
            builder.append(zo.trigger);
            builder.append("|");
        }
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uzone_trigger == null) ? 0 : uzone_trigger.hashCode());
        result = prime * result + ((zone_Type == null) ? 0 : zone_Type.hashCode());
        result = prime * result + ((zone_creator == null) ? 0 : zone_creator.hashCode());
        result = prime * result + ((zone_name == null) ? 0 : zone_name.hashCode());
        result = prime * result + ((zone_world == null) ? 0 : zone_world.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Zone other = (Zone) obj;
        if (uzone_trigger == null) {
            if (other.uzone_trigger != null) return false;
        }
        else if (!uzone_trigger.equals(other.uzone_trigger)) return false;
        if (zone_Type == null) {
            if (other.zone_Type != null) return false;
        }
        else if (!zone_Type.equals(other.zone_Type)) return false;
        if (zone_creator == null) {
            if (other.zone_creator != null) return false;
        }
        else if (!zone_creator.equals(other.zone_creator)) return false;
        if (zone_name == null) {
            if (other.zone_name != null) return false;
        }
        else if (!zone_name.equals(other.zone_name)) return false;
        if (zone_world == null) {
            if (other.zone_world != null) return false;
        }
        else if (!zone_world.equals(other.zone_world)) return false;
        return true;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    /*Not needed now. Here for Reference.
     * public void setChunkUnloaded(boolean chunkUnloaded) {
        this.chunkUnloaded = chunkUnloaded;
    }

    public boolean isChunkUnloaded() {
        return chunkUnloaded;
    }*/
}
