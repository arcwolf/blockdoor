package plugin.arcwolf.blockdoor.Utils;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class BlockDoorSettings {

    public static Map<String, BlockDoorSettings> playerSettings = new HashMap<String, BlockDoorSettings>();
    
    //command used
    public String command = "";
    public String friendlyCommand = "";
    
    //users name
    public String name = "";
    
    //trigger name for uzones
    public String trigger = "";

    //control variable for the different functions
    //ie. Door start / end selection
    public int select = 0;

    //find control for commands
    public int notFound = 0;

    //For temporary coordinate storage
    public int startX, startY, startZ, endX, endY, endZ;

    // Temp door variable
    public Door door = null;
    
    // Temp zone variable
    public Zone zone = null;

    //Returns a BlockDoorSettings for the player, making a new one if needed
    public static BlockDoorSettings getSettings(Player player) {
        BlockDoorSettings settings = (BlockDoorSettings) playerSettings.get(player.getName());
        if (settings == null) {
            playerSettings.put(player.getName(), new BlockDoorSettings());
            settings = (BlockDoorSettings) playerSettings.get(player.getName());
        }
        return (settings);
    }

    //Clears the player from the hashmap
    public static void clearSettings(Player player) {
        playerSettings.remove(player.getName());
    }
}