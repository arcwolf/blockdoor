package plugin.arcwolf.blockdoor.Utils;

import java.util.Comparator;

import plugin.arcwolf.blockdoor.DLister;

public class PlayerNameComparator implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {
        DLister N1 = (DLister) o1;
        DLister N2 = (DLister) o2;
        return N1.getPlayerName().compareToIgnoreCase(N2.getPlayerName());
    }
}