package plugin.arcwolf.blockdoor.Utils;

import org.bukkit.Location;

//Used in conjunction with Twostate doors and HDoors to help prevent item popping

public class CustomLocation{

    private int x;
    private int y;
    private int z;

    private String world_Name = "";

    public CustomLocation(String world, int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        world_Name = world;
    }

    public CustomLocation(Location location) {
        world_Name = location.getWorld().getName();
        x = location.getBlockX();
        y = location.getBlockY();
        z = location.getBlockZ();
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @return the z
     */
    public int getZ() {
        return z;
    }

    /**
     * @return the worldName
     */
    public String getWorldName() {
        return world_Name;
    }

    /**
     * @param x
     *            the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @param y
     *            the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @param z
     *            the z to set
     */
    public void setZ(int z) {
        this.z = z;
    }

    /**
     * @param worldName
     *            the worldName to set
     */
    public void setWorldName(String world_Name) {
        this.world_Name = world_Name;
    }

    public String toString() {
        return "CustomLocation[" + world_Name + "," + x + "," + y + "," + z + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((world_Name == null) ? 0 : world_Name.hashCode());
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        CustomLocation other = (CustomLocation) obj;
        if (world_Name == null) {
            if (other.world_Name != null) return false;
        }
        else if (!world_Name.equals(other.world_Name)) return false;
        if (x != other.x) return false;
        if (y != other.y) return false;
        if (z != other.z) return false;
        return true;
    }

}
