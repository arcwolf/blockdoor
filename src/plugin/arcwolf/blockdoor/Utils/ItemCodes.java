package plugin.arcwolf.blockdoor.Utils;

// Defines what a users input from words to game readable block id codes would be

public class ItemCodes {

    public String objectName;
    public String objectID;

    public ItemCodes(String item_name, String item_ID) {
        objectName = item_name;
        objectID = item_ID;
    }
}