package plugin.arcwolf.blockdoor.Utils;

import plugin.arcwolf.blockdoor.BlockDoor;

//Turns a users input from words to game readable block id codes and if necessary their offset codes

public class ItemCodesHelper {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public ItemCodesHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public int fillDatabase(String item_name, String item_ID) {
        int index = -1;
        for (int i = 0; i < dataWriter.itemcodes.size(); i++)
            if (dataWriter.itemcodes.get(i).objectName.equals(item_name) && dataWriter.itemcodes.get(i).objectID.equals(item_ID))
                index = i;
        if (index == -1) {
            dataWriter.itemcodes.add(new ItemCodes(item_name, item_ID));
        }
        return index;
    }

    public int findItemByID(String item_ID) {
        int index = -1;
        for (int i = 0; i < dataWriter.itemcodes.size(); i++)
            if (dataWriter.itemcodes.get(i).objectID.equals(item_ID))
                index = i;
        return index;
    }

    public int findItemByName(String item_Name) {
        int index = -1;
        finishSearch:
        for (int i = 0; i < dataWriter.itemcodes.size(); i++) {
            String[] split = dataWriter.itemcodes.get(i).objectName.split(",");
            for (int j = 0; j < split.length; j++)
                if (split[j].equalsIgnoreCase(item_Name)) {
                    index = i;
                    break finishSearch;
                }
        }
        return index;
    }

    @SuppressWarnings("unused")
    public String getItem(String item_Name, String item_Offset) {
        int index = 0;
        String[] split, objectNames, splitID;
        if (!dataWriter.isItemDatabaseEr()) {
            int objectID, objectOffset;
            try { // test for number entered for item name
                objectID = Integer.parseInt(item_Name);
                if (item_Offset.equals("-1")) {
                    if (findItemByID(item_Name + ",0") != -1)
                        return (item_Name + ",0");
                    else
                        return ("INVALID BLOCK TYPE");
                }
                try { // test for number entered for item offset
                    objectOffset = Integer.parseInt(item_Offset);
                    if (findItemByID(item_Name + "," + item_Offset) != -1)
                        return (item_Name + "," + item_Offset);
                    else
                        return ("INVALID BLOCK TYPE");
                }
                catch (Exception e) { // number was entered for item name but name for item offset
                    if (item_Offset.equals("-1"))
                        return (item_Name + ",0");
                    for (ItemCodes i : dataWriter.itemcodes) {
                        split = i.objectID.split(",");
                        if (split[0].equals(item_Name)) {
                            objectNames = i.objectName.split(",");
                            for (int x = 0; x < objectNames.length; x++) {
                                if (objectNames[x].equalsIgnoreCase(item_Offset)) {
                                    return (i.objectID);
                                }
                            }
                        }
                    }
                    return ("INVALID BLOCK TYPE");
                }
            } // name was entered for item name
            catch (Exception e) {
                index = findItemByName(item_Name);
                if (index == -1)
                    return ("INVALID BLOCK TYPE");
                if (item_Offset.equals("-1"))
                    return (dataWriter.itemcodes.get(index).objectID);
                try { // test for number in item offset
                    objectOffset = Integer.parseInt(item_Offset);
                    split = dataWriter.itemcodes.get(index).objectID.split(",");
                    return (split[0] + "," + item_Offset);
                }
                catch (Exception e1) { // name was entered for item offset
                    for (ItemCodes i : dataWriter.itemcodes) {
                        split = i.objectID.split(",");
                        splitID = dataWriter.itemcodes.get(index).objectID.split(",");
                        if (split[0].equals(splitID[0])) {
                            objectNames = i.objectName.split(",");
                            for (int x = 0; x < objectNames.length; x++) {
                                if (objectNames[x].equalsIgnoreCase(item_Offset)) {
                                    return (i.objectID);
                                }
                            }
                        }
                    }
                    return ("INVALID BLOCK TYPE");
                }
            }
        }
        else {
            //oops something is wrong fall back to internal database
            return (getItemIdAndData(item_Name, item_Offset));
        }
    }

    //Internal Database in case of external database error
    public String getItemIdAndData(String item_Name, String item_Offset) {
        if (item_Offset.equals("-1"))
            item_Offset = "0";
        if (item_Name.equalsIgnoreCase("air") || item_Name.equals("0") || item_Name.equals("00")) {
            return ("0,0");
        }
        if (item_Name.equalsIgnoreCase("stone") || item_Name.equals("1") || item_Name.equals("01")) {
            return ("1,0");
        }
        if (item_Name.equalsIgnoreCase("grass") || item_Name.equals("2") || item_Name.equals("02")) {
            return ("2,0");
        }
        if (item_Name.equalsIgnoreCase("dirt") || item_Name.equals("3") || item_Name.equals("03")) {
            return ("3,0");
        }
        if (item_Name.equalsIgnoreCase("cobblestone") || item_Name.equals("4") || item_Name.equals("04")) {
            return ("4,0");
        }
        if (item_Name.equalsIgnoreCase("wood") || item_Name.equalsIgnoreCase("woodplank") || item_Name.equalsIgnoreCase("woodenplank") || item_Name.equals("5") || item_Name.equals("05")) {
            return ("5,0");
        }
        if (item_Name.equalsIgnoreCase("bedrock") || item_Name.equalsIgnoreCase("adminium") || item_Name.equals("7") || item_Name.equals("07")) {
            return ("7,0");
        }
        if (item_Name.equalsIgnoreCase("water") || item_Name.equalsIgnoreCase("flowingwater") || item_Name.equals("8") || item_Name.equals("08")) {
            return ("8,0");
        }
        if (item_Name.equalsIgnoreCase("stillwater") || item_Name.equalsIgnoreCase("stationarywater") || item_Name.equals("9") || item_Name.equals("09")) {
            return ("9,0");
        }
        if (item_Name.equalsIgnoreCase("lava") || item_Name.equalsIgnoreCase("flowingwater") || item_Name.equals("10") || item_Name.equals("10")) {
            return ("10,0");
        }
        if (item_Name.equalsIgnoreCase("stilllava") || item_Name.equalsIgnoreCase("stationarylava") || item_Name.equals("11")) {
            return ("11,0");
        }
        if (item_Name.equalsIgnoreCase("sand") || item_Name.equals("12")) {
            return ("12,0");
        }
        if (item_Name.equalsIgnoreCase("gravel") || item_Name.equals("13")) {
            return ("13,0");
        }
        if (item_Name.equalsIgnoreCase("goldore") || item_Name.equals("14")) {
            return ("14,0");
        }
        if (item_Name.equalsIgnoreCase("ironore") || item_Name.equals("15")) {
            return ("15,0");
        }
        if (item_Name.equalsIgnoreCase("coalore") || item_Name.equals("16")) {
            return ("16,0");
        }
        if (item_Name.equalsIgnoreCase("log") || item_Name.equals("17")) {
            if (item_Offset.equalsIgnoreCase("usual") || item_Offset.equalsIgnoreCase("normal") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("17,0");
            if (item_Offset.equalsIgnoreCase("redwood") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("17,1");
            if (item_Offset.equalsIgnoreCase("birch") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("17,2");
            return ("INVALID LOG TYPE");
        }
        if (item_Name.equalsIgnoreCase("leaves") || item_Name.equals("18")) {
            if (item_Offset.equalsIgnoreCase("usual") || item_Offset.equalsIgnoreCase("normal") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("18,0");
            if (item_Offset.equalsIgnoreCase("redwood") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("18,1");
            if (item_Offset.equalsIgnoreCase("birch") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("18,2");
            return ("INVALID LEAF TYPE");
        }
        if (item_Name.equalsIgnoreCase("sponge") || item_Name.equals("19")) {
            return ("19,0");
        }
        if (item_Name.equalsIgnoreCase("glass") || item_Name.equals("20")) {
            return ("20,0");
        }
        if (item_Name.equalsIgnoreCase("lapislazuliore") || item_Name.equalsIgnoreCase("lapisore") || item_Name.equals("21")) {
            return ("21,0");
        }
        if (item_Name.equalsIgnoreCase("lapis") || item_Name.equalsIgnoreCase("lapisblock") || item_Name.equalsIgnoreCase("lapislazuliblock") || item_Name.equals("22")) {
            return ("22,0");
        }
        if (item_Name.equalsIgnoreCase("sandstone") || item_Name.equals("24")) {
            return ("24,0");
        }
        if (item_Name.equalsIgnoreCase("web") || item_Name.equalsIgnoreCase("spiderweb") || item_Name.equals("30")) {
            return ("30,0");
        }
        if (item_Name.equalsIgnoreCase("wool") || item_Name.equalsIgnoreCase("cloth") || item_Name.equals("35")) {
            if (item_Offset.equalsIgnoreCase("white") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("35,0");
            if (item_Offset.equalsIgnoreCase("orange") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("35,1");
            if (item_Offset.equalsIgnoreCase("magenta") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("35,2");
            if (item_Offset.equalsIgnoreCase("lightblue") || item_Offset.equals("3") || item_Offset.equals("03"))
                return ("35,3");
            if (item_Offset.equalsIgnoreCase("yellow") || item_Offset.equals("4") || item_Offset.equals("04"))
                return ("35,4");
            if (item_Offset.equalsIgnoreCase("lightgreen") || item_Offset.equals("5") || item_Offset.equals("05"))
                return ("35,5");
            if (item_Offset.equalsIgnoreCase("pink") || item_Offset.equals("6") || item_Offset.equals("06"))
                return ("35,6");
            if (item_Offset.equalsIgnoreCase("grey") || item_Offset.equalsIgnoreCase("gray") || item_Offset.equals("7") || item_Offset.equals("07"))
                return ("35,7");
            if (item_Offset.equalsIgnoreCase("lightgrey") || item_Offset.equals("lightgray") || item_Offset.equals("8") || item_Offset.equals("08"))
                return ("35,8");
            if (item_Offset.equalsIgnoreCase("cyan") || item_Offset.equals("9") || item_Offset.equals("09"))
                return ("35,9");
            if (item_Offset.equalsIgnoreCase("purple") || item_Offset.equals("10"))
                return ("35,10");
            if (item_Offset.equalsIgnoreCase("blue") || item_Offset.equals("11"))
                return ("35,11");
            if (item_Offset.equalsIgnoreCase("brown") || item_Offset.equals("12"))
                return ("35,12");
            if (item_Offset.equalsIgnoreCase("darkgreen") || item_Offset.equals("13"))
                return ("35,13");
            if (item_Offset.equalsIgnoreCase("red") || item_Offset.equals("14"))
                return ("35,14");
            if (item_Offset.equalsIgnoreCase("black") || item_Offset.equals("15"))
                return ("35,15");
            return ("INVALID COLOR");
        }
        if (item_Name.equalsIgnoreCase("gold") || item_Name.equalsIgnoreCase("goldblock") || item_Name.equals("41")) {
            return ("41,0");
        }
        if (item_Name.equalsIgnoreCase("iron") || item_Name.equalsIgnoreCase("ironblock") || item_Name.equals("42")) {
            return ("42,0");
        }
        if (item_Name.equalsIgnoreCase("doubleslab") || item_Name.equalsIgnoreCase("doublestep") || item_Name.equals("43")) {
            if (item_Offset.equalsIgnoreCase("stone") || item_Offset.equalsIgnoreCase("stoneslab") || item_Offset.equalsIgnoreCase("stonestep") || item_Offset.equals("0") || item_Offset.equals("00") || item_Offset.equals("6") || item_Offset.equals("06"))
                return ("43,0");
            if (item_Offset.equalsIgnoreCase("sandstone") || item_Offset.equalsIgnoreCase("sandstoneslab") || item_Offset.equalsIgnoreCase("sandstonestep") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("43,1");
            if (item_Offset.equalsIgnoreCase("wood") || item_Offset.equalsIgnoreCase("woodenslab") || item_Offset.equalsIgnoreCase("woodenstep") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("43,2");
            if (item_Offset.equalsIgnoreCase("cobblestone") || item_Offset.equalsIgnoreCase("cobblestoneslab") || item_Offset.equalsIgnoreCase("cobblestonestep") || item_Offset.equals("3") || item_Offset.equals("03"))
                return ("43,3");
            if (item_Offset.equalsIgnoreCase("brick") || item_Offset.equalsIgnoreCase("brickslab") || item_Offset.equals("4") || item_Offset.equals("04"))
                return ("43,4");
            if (item_Offset.equalsIgnoreCase("stonebrick") || item_Offset.equalsIgnoreCase("stonebrickslab") || item_Offset.equals("5") || item_Offset.equals("05"))
                return ("43,5");
        }
        if (item_Name.equalsIgnoreCase("slab") || item_Name.equalsIgnoreCase("singlestep") || item_Name.equalsIgnoreCase("step") || item_Name.equals("44")) {
            if (item_Offset.equalsIgnoreCase("stone") || item_Offset.equalsIgnoreCase("stoneslab") || item_Offset.equalsIgnoreCase("stonestep") || item_Offset.equals("0") || item_Offset.equals("00") || item_Offset.equals("6") || item_Offset.equals("06"))
                return ("44,0");
            if (item_Offset.equalsIgnoreCase("sandstone") || item_Offset.equalsIgnoreCase("sandstoneslab") || item_Offset.equalsIgnoreCase("sandstonestep") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("44,1");
            if (item_Offset.equalsIgnoreCase("wood") || item_Offset.equalsIgnoreCase("woodenslab") || item_Offset.equalsIgnoreCase("woodenstep") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("44,2");
            if (item_Offset.equalsIgnoreCase("cobblestone") || item_Offset.equalsIgnoreCase("cobblestoneslab") || item_Offset.equalsIgnoreCase("cobblestonestep") || item_Offset.equals("3") || item_Offset.equals("03"))
                return ("44,3");
            if (item_Offset.equalsIgnoreCase("brick") || item_Offset.equalsIgnoreCase("brickslab") || item_Offset.equals("4") || item_Offset.equals("04"))
                return ("44,4");
            if (item_Offset.equalsIgnoreCase("stonebrick") || item_Offset.equalsIgnoreCase("stonebrickslab") || item_Offset.equals("5") || item_Offset.equals("05"))
                return ("44,5");
        }
        if (item_Name.equalsIgnoreCase("brick") || item_Name.equalsIgnoreCase("brickblock") || item_Name.equals("45")) {
            return ("45,0");
        }
        if (item_Name.equalsIgnoreCase("bookshelf") || item_Name.equals("47")) {
            return ("47,0");
        }
        if (item_Name.equalsIgnoreCase("mossycobblestone") || item_Name.equalsIgnoreCase("mossystone") || item_Name.equalsIgnoreCase("mossstone") || item_Name.equals("48")) {
            return ("48,0");
        }
        if (item_Name.equalsIgnoreCase("obsidian") || item_Name.equals("49")) {
            return ("49,0");
        }
        if (item_Name.equalsIgnoreCase("fire") || item_Name.equals("51")) {
            return ("51,0");
        }
        if (item_Name.equalsIgnoreCase("diamondore") || item_Name.equals("56")) {
            return ("56,0");
        }
        if (item_Name.equalsIgnoreCase("diamondblock") || item_Name.equalsIgnoreCase("diamond") || item_Name.equals("57")) {
            return ("57,0");
        }
        if (item_Name.equalsIgnoreCase("redstoneore") || item_Name.equalsIgnoreCase("redstone") || item_Name.equals("73")) {
            return ("73,0");
        }
        if (item_Name.equalsIgnoreCase("glowingredstoneore") || item_Name.equals("74")) {
            return ("74,0");
        }
        if (item_Name.equalsIgnoreCase("redstonetorchoff") || item_Name.equals("75")) {
            return ("75,0");
        }
        if (item_Name.equalsIgnoreCase("redstonetorch") || item_Name.equals("76")) {
            return ("76,0");
        }
        if (item_Name.equalsIgnoreCase("ice") || item_Name.equals("79")) {
            return ("79,0");
        }
        if (item_Name.equalsIgnoreCase("snow") || item_Name.equalsIgnoreCase("snowblock") || item_Name.equals("80")) {
            return ("80,0");
        }
        if (item_Name.equalsIgnoreCase("cactus") || item_Name.equals("81")) {
            return ("81,0");
        }
        if (item_Name.equalsIgnoreCase("clay") || item_Name.equalsIgnoreCase("clayblock") || item_Name.equals("82")) {
            return ("82,0");
        }
        if (item_Name.equalsIgnoreCase("fence") || item_Name.equals("85")) {
            return ("85,0");
        }
        if (item_Name.equalsIgnoreCase("pumpkin") || item_Name.equals("86")) {
            return ("86,0");
        }
        if (item_Name.equalsIgnoreCase("netherrack") || item_Name.equalsIgnoreCase("netherrock") || item_Name.equalsIgnoreCase("bloodstone") || item_Name.equals("87")) {
            return ("87,0");
        }
        if (item_Name.equalsIgnoreCase("soulsand") || item_Name.equalsIgnoreCase("slowsand") || item_Name.equalsIgnoreCase("nethersand") || item_Name.equals("88")) {
            return ("88,0");
        }
        if (item_Name.equalsIgnoreCase("glowstoneblock") || item_Name.equalsIgnoreCase("glowstone") || item_Name.equalsIgnoreCase("brittlegold") || item_Name.equals("89")) {
            return ("89,0");
        }
        if (item_Name.equalsIgnoreCase("jackolantern") || item_Name.equals("91")) {
            return ("91,0");
        }
        if (item_Name.equalsIgnoreCase("stonebrick") || item_Name.equals("98")) {
            if (item_Offset.equalsIgnoreCase("normal") || item_Offset.equalsIgnoreCase("stonebrick") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("98,0");
            if (item_Offset.equalsIgnoreCase("mossy") || item_Offset.equalsIgnoreCase("mossystonebrick") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("98,1");
            if (item_Offset.equalsIgnoreCase("cracked") || item_Offset.equalsIgnoreCase("crackstonebrick") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("98,2");
        }
        if (item_Name.equalsIgnoreCase("redmushroom") || item_Name.equalsIgnoreCase("hugeredmushroom") || item_Name.equals("99")) {
            if (item_Offset.equalsIgnoreCase("fleshy") || item_Offset.equalsIgnoreCase("flesh") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("99,0");
            if (item_Offset.equals("1") || item_Offset.equals("01"))
                return ("99,1");
            if (item_Offset.equals("2") || item_Offset.equals("02"))
                return ("99,2");
            if (item_Offset.equals("3") || item_Offset.equals("03"))
                return ("99,3");
            if (item_Offset.equals("4") || item_Offset.equals("04"))
                return ("99,4");
            if (item_Offset.equals("5") || item_Offset.equals("05"))
                return ("99,5");
            if (item_Offset.equals("6") || item_Offset.equals("06"))
                return ("99,6");
            if (item_Offset.equals("7") || item_Offset.equals("07"))
                return ("99,7");
            if (item_Offset.equals("8") || item_Offset.equals("08"))
                return ("99,8");
            if (item_Offset.equals("9") || item_Offset.equals("09"))
                return ("99,9");
            if (item_Offset.equalsIgnoreCase("stem") || item_Offset.equals("10"))
                return ("99,10");
        }
        if (item_Name.equalsIgnoreCase("brownmushroom") || item_Name.equalsIgnoreCase("hugebrownmushroom") || item_Name.equals("100")) {
            if (item_Offset.equalsIgnoreCase("fleshy") || item_Offset.equalsIgnoreCase("flesh") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("100,0");
            if (item_Offset.equals("1") || item_Offset.equals("01"))
                return ("100,1");
            if (item_Offset.equals("2") || item_Offset.equals("02"))
                return ("100,2");
            if (item_Offset.equals("3") || item_Offset.equals("03"))
                return ("100,3");
            if (item_Offset.equals("4") || item_Offset.equals("04"))
                return ("100,4");
            if (item_Offset.equals("5") || item_Offset.equals("05"))
                return ("100,5");
            if (item_Offset.equals("6") || item_Offset.equals("06"))
                return ("100,6");
            if (item_Offset.equals("7") || item_Offset.equals("07"))
                return ("100,7");
            if (item_Offset.equals("8") || item_Offset.equals("08"))
                return ("100,8");
            if (item_Offset.equals("9") || item_Offset.equals("09"))
                return ("100,9");
            if (item_Offset.equalsIgnoreCase("stem") || item_Offset.equals("10"))
                return ("100,10");
        }
        if (item_Name.equalsIgnoreCase("ironbars") || item_Name.equals("101")) {
            return ("101,0");
        }
        if (item_Name.equalsIgnoreCase("glasspane") || item_Name.equals("102")) {
            return ("102,0");
        }
        if (item_Name.equalsIgnoreCase("melon") || item_Name.equals("103")) {
            return ("103,0");
        }       
        if (item_Name.equalsIgnoreCase("fencegate") || item_Name.equals("107")) {
            return ("107,0");
        }
        if (item_Name.equalsIgnoreCase("mycelium") || item_Name.equals("110")) {
            return ("110,0");
        }
        if (item_Name.equalsIgnoreCase("lilypad") || item_Name.equals("111")) {
            return ("111,0");
        }
        if (item_Name.equalsIgnoreCase("netherbrick") || item_Name.equals("112")) {
            return ("112,0");
        }
        if (item_Name.equalsIgnoreCase("netherbrickfence") || item_Name.equals("113")) {
            return ("113,0");
        }
        if (item_Name.equalsIgnoreCase("endstone") || item_Name.equalsIgnoreCase("whitestone") || item_Name.equals("121")) {
            return ("121,0");
        }
        if (item_Name.equalsIgnoreCase("redstonelamp") || item_Name.equals("123")) {
            return ("123,0");
        }
        if (item_Name.equalsIgnoreCase("oakwooddoubleslab") || item_Name.equalsIgnoreCase("oakdoubleslab") || (item_Name.equals("125")&&(item_Offset.equals("0")||item_Offset.equals("00")))) {
            return ("125,0");
        }
        if (item_Name.equalsIgnoreCase("sprucewooddoubleslab") || item_Name.equalsIgnoreCase("sprucedoubleslab") || (item_Name.equals("125")&&(item_Offset.equals("1")||item_Offset.equals("01")))) {
            return ("125,1");
        }
        if (item_Name.equalsIgnoreCase("birchwooddoubleslab") || item_Name.equalsIgnoreCase("birchdoubleslab") || (item_Name.equals("125")&&(item_Offset.equals("2")||item_Offset.equals("02")))) {
            return ("125,2");
        }
        if (item_Name.equalsIgnoreCase("junglewooddoubleslab") || item_Name.equalsIgnoreCase("jungledoubleslab") || (item_Name.equals("125")&&(item_Offset.equals("3")||item_Offset.equals("03")))) {
            return ("125,3");
        }
        if (item_Name.equalsIgnoreCase("oakwoodslab") || item_Name.equalsIgnoreCase("oakslab") || (item_Name.equals("126")&&(item_Offset.equals("0")||item_Offset.equals("00")))) {
            return ("126,0");
        }
        if (item_Name.equalsIgnoreCase("sprucewoodslab") || item_Name.equalsIgnoreCase("spruceslab") || (item_Name.equals("126")&&(item_Offset.equals("1")||item_Offset.equals("01")))) {
            return ("126,1");
        }
        if (item_Name.equalsIgnoreCase("birchwoodslab") || item_Name.equalsIgnoreCase("birchslab") || (item_Name.equals("126")&&(item_Offset.equals("2")||item_Offset.equals("02")))) {
            return ("126,2");
        }
        if (item_Name.equalsIgnoreCase("junglewoodslab") || item_Name.equalsIgnoreCase("jungleslab") || (item_Name.equals("126")&&(item_Offset.equals("3")||item_Offset.equals("03")))) {
            return ("126,3");
        }
        if (item_Name.equalsIgnoreCase("sandstonestairs") || item_Name.equals("128")) {
            return ("128,0");
        }
        if (item_Name.equalsIgnoreCase("emeraldore") || item_Name.equals("129")) {
            return ("129,0");
        }
        if (item_Name.equalsIgnoreCase("emeraldblock") || item_Name.equals("133")) {
            return ("133,0");
        }
        if (item_Name.equalsIgnoreCase("sprucewoodstairs") || item_Name.equals("134")) {
            return ("134,0");
        }
        if (item_Name.equalsIgnoreCase("birchwoodstairs") || item_Name.equals("135")) {
            return ("135,0");
        }
        if (item_Name.equalsIgnoreCase("junglewoodstairs") || item_Name.equals("136")) {
            return ("136,0");
        }
        if (item_Name.equalsIgnoreCase("cobblestonewall") || item_Name.equals("139")) {
            return ("139,0");
        }
        if (item_Name.equalsIgnoreCase("woodenbutton") || item_Name.equals("143")) {
            return ("143,0");
        }
        if (item_Name.equalsIgnoreCase("lightsensor") || item_Name.equalsIgnoreCase("daylightsensor") || item_Name.equals("151")) {
            return ("151,0");
        }
        if (item_Name.equalsIgnoreCase("blockofredstone") || item_Name.equalsIgnoreCase("redstoneblock") || item_Name.equals("152")) {
            return ("152,0");
        }
        if (item_Name.equalsIgnoreCase("netherquartzore") || item_Name.equals("153")) {
            return ("153,0");
        }
        if (item_Name.equalsIgnoreCase("netherquartzblock") || item_Name.equalsIgnoreCase("quartzblock") || item_Name.equals("155")) {
            return ("155,0");
        }
        if (item_Name.equalsIgnoreCase("stainedclay") || item_Name.equals("159")) {
            if (item_Offset.equalsIgnoreCase("white") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("159,0");
            if (item_Offset.equalsIgnoreCase("orange") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("159,1");
            if (item_Offset.equalsIgnoreCase("magenta") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("159,2");
            if (item_Offset.equalsIgnoreCase("lightblue") || item_Offset.equals("3") || item_Offset.equals("03"))
                return ("159,3");
            if (item_Offset.equalsIgnoreCase("yellow") || item_Offset.equals("4") || item_Offset.equals("04"))
                return ("159,4");
            if (item_Offset.equalsIgnoreCase("lightgreen") || item_Offset.equals("5") || item_Offset.equals("05"))
                return ("159,5");
            if (item_Offset.equalsIgnoreCase("pink") || item_Offset.equals("6") || item_Offset.equals("06"))
                return ("159,6");
            if (item_Offset.equalsIgnoreCase("grey") || item_Offset.equalsIgnoreCase("gray") || item_Offset.equals("7") || item_Offset.equals("07"))
                return ("159,7");
            if (item_Offset.equalsIgnoreCase("lightgrey") || item_Offset.equals("lightgray") || item_Offset.equals("8") || item_Offset.equals("08"))
                return ("159,8");
            if (item_Offset.equalsIgnoreCase("cyan") || item_Offset.equals("9") || item_Offset.equals("09"))
                return ("159,9");
            if (item_Offset.equalsIgnoreCase("purple") || item_Offset.equals("10"))
                return ("159,10");
            if (item_Offset.equalsIgnoreCase("blue") || item_Offset.equals("11"))
                return ("159,11");
            if (item_Offset.equalsIgnoreCase("brown") || item_Offset.equals("12"))
                return ("159,12");
            if (item_Offset.equalsIgnoreCase("darkgreen") || item_Offset.equals("13"))
                return ("159,13");
            if (item_Offset.equalsIgnoreCase("red") || item_Offset.equals("14"))
                return ("159,14");
            if (item_Offset.equalsIgnoreCase("black") || item_Offset.equals("15"))
                return ("159,15");
            return ("INVALID COLOR");
        }
        if (item_Name.equalsIgnoreCase("hayblock") || item_Name.equalsIgnoreCase("hay") || item_Name.equals("170")) {
            return ("170,0");
        }
        if (item_Name.equalsIgnoreCase("carpet") || item_Name.equals("171")) {
            if (item_Offset.equalsIgnoreCase("white") || item_Offset.equals("0") || item_Offset.equals("00"))
                return ("171,0");
            if (item_Offset.equalsIgnoreCase("orange") || item_Offset.equals("1") || item_Offset.equals("01"))
                return ("171,1");
            if (item_Offset.equalsIgnoreCase("magenta") || item_Offset.equals("2") || item_Offset.equals("02"))
                return ("171,2");
            if (item_Offset.equalsIgnoreCase("lightblue") || item_Offset.equals("3") || item_Offset.equals("03"))
                return ("171,3");
            if (item_Offset.equalsIgnoreCase("yellow") || item_Offset.equals("4") || item_Offset.equals("04"))
                return ("171,4");
            if (item_Offset.equalsIgnoreCase("lightgreen") || item_Offset.equals("5") || item_Offset.equals("05"))
                return ("171,5");
            if (item_Offset.equalsIgnoreCase("pink") || item_Offset.equals("6") || item_Offset.equals("06"))
                return ("171,6");
            if (item_Offset.equalsIgnoreCase("grey") || item_Offset.equalsIgnoreCase("gray") || item_Offset.equals("7") || item_Offset.equals("07"))
                return ("171,7");
            if (item_Offset.equalsIgnoreCase("lightgrey") || item_Offset.equals("lightgray") || item_Offset.equals("8") || item_Offset.equals("08"))
                return ("171,8");
            if (item_Offset.equalsIgnoreCase("cyan") || item_Offset.equals("9") || item_Offset.equals("09"))
                return ("171,9");
            if (item_Offset.equalsIgnoreCase("purple") || item_Offset.equals("10"))
                return ("171,10");
            if (item_Offset.equalsIgnoreCase("blue") || item_Offset.equals("11"))
                return ("171,11");
            if (item_Offset.equalsIgnoreCase("brown") || item_Offset.equals("12"))
                return ("171,12");
            if (item_Offset.equalsIgnoreCase("darkgreen") || item_Offset.equals("13"))
                return ("171,13");
            if (item_Offset.equalsIgnoreCase("red") || item_Offset.equals("14"))
                return ("171,14");
            if (item_Offset.equalsIgnoreCase("black") || item_Offset.equals("15"))
                return ("171,15");
            return ("INVALID COLOR");
        }
        if (item_Name.equalsIgnoreCase("hardenedclay") || item_Name.equalsIgnoreCase("hardclay") || item_Name.equals("172")) {
            return ("172,0");
        }
        if (item_Name.equalsIgnoreCase("coalblock") || item_Name.equals("173")) {
            return ("173,0");
        }
        return ("INVALID BLOCK TYPE");
    }
}