package plugin.arcwolf.blockdoor.Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;
import plugin.arcwolf.blockdoor.Link;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Doors.DoorOverlaps;
import plugin.arcwolf.blockdoor.Doors.DoorState;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.HDoorOverlaps;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Triggers.TrigOverlaps;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Zones.Zone;
import plugin.arcwolf.blockdoor.Zones.ZoneOverlaps;

public class DataWriter {

    private BlockDoor plugin;

    //All SingleState Doors Map
    public Map<SingleStateDoor, SingleStateDoor> allSSDoorsMap = new HashMap<SingleStateDoor, SingleStateDoor>();

    //All TwoState Doors Map
    public Map<TwoStateDoor, TwoStateDoor> allTSDoorsMap = new HashMap<TwoStateDoor, TwoStateDoor>();

    //All Hybrid Doors Map
    public Map<HDoor, HDoor> allHDoorsMap = new HashMap<HDoor, HDoor>();

    //All Triggers Map
    public Map<Trigger, Trigger> allTriggersMap = new HashMap<Trigger, Trigger>();

    //All Zones Map
    public Map<Zone, Zone> allZonesMap = new HashMap<Zone, Zone>();

    // Door and Trigger Block map
    public Map<Block, Object> blockMap = new HashMap<Block, Object>();

    // Zone Block Map
    public Map<Block, Zone> zoneBlockMap = new HashMap<Block, Zone>();

    public Map<String, List<String>> mobs = new HashMap<String, List<String>>();
    public Map<Door, Boolean> doorPhysics = new HashMap<Door, Boolean>();

    //Item database array
    public List<ItemCodes> itemcodes = new ArrayList<ItemCodes>();

    //zone occupancy tracker map
    public Map<Entity, List<Zone>> zoneOccupancyMap = new HashMap<Entity, List<Zone>>();

    //config variables
    private int max_DoorSize = 10;
    private int max_TwoStateSize = 20;
    private int max_hdoorSize = 50;
    private int max_ZoneSize = 20;

    private boolean overlapZones = false;
    private boolean overlapDoors = false;
    private boolean overlapTwoStateDoors = false;
    private boolean overlapHDoors = false;
    private boolean overlapTriggers = false;
    private boolean overlapRedstone = false;

    private boolean enableSpecialBlocks = false;
    private boolean enableConsoleCommands = false;

    //Item database error control variable
    private boolean itemDatabaseEr = false;

    //Uzone database error control variable
    private boolean uzoneDatabaseEr = false;

    //error control variable
    private boolean permissionsSet = false;
    private boolean error = false;

    //Admin plugin reload function
    private boolean reload = false;

    //For first run errors
    private boolean firstV2run = false;

    private String oldDoorsFile = "blockdoor.dat";
    private String doorsConfig = "config.txt";
    private String itemsDatabase = "items.txt";
    private String newDoorsFile = "blockdoorv2.dat";
    private String tmpDoorsFile = "blockdoorv2.tmp";
    private String uzoneDB = "mobs.txt";
    private File directory;
    private File oldDoorV1Database;
    private File newDoorV2Database;
    private File tmpDoorBinDatabase;
    private File doorConfig;
    private File itemDatabase;
    private File uzoneDatabase;

    public DataWriter() {
        plugin = BlockDoor.plugin;
        directory = plugin.getDataFolder();
        oldDoorV1Database = new File(directory, oldDoorsFile);
        newDoorV2Database = new File(directory, newDoorsFile);
        tmpDoorBinDatabase = new File(directory, tmpDoorsFile);
        doorConfig = new File(directory, doorsConfig);
        itemDatabase = new File(directory, itemsDatabase);
        uzoneDatabase = new File(directory, uzoneDB);
    }

    public void initFile() {
        if (!directory.exists()) {
            BlockDoor.LOGGER.info("BlockDoor folder does not exist - creating it... ");
            boolean chk = directory.mkdir();
            if (chk)
                BlockDoor.LOGGER.info("Successfully created folder.");
            else
                BlockDoor.LOGGER.info("Unable to create folder!");
        }
        if (!newDoorV2Database.exists()) {
            ObjectOutputStream outputStream;
            try {
                BlockDoor.LOGGER.info("BlockDoor Data File does not exist - creating it... ");
                outputStream = new ObjectOutputStream(new FileOutputStream(newDoorV2Database));
                outputStream.writeObject("");
                outputStream.close();
                firstV2run = true;
                BlockDoor.LOGGER.info("Successfully created New Data File.");
            } catch (FileNotFoundException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while creating " + newDoorV2Database.getAbsolutePath() + "\n", e);
            } catch (IOException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while creating " + newDoorV2Database.getAbsolutePath() + "\n", e);
            }
        }
        if (!doorConfig.exists()) {
            FileWriter fwriter = null;
            BufferedWriter writer = null;
            try {
                BlockDoor.LOGGER.info("BlockDoor Config File does not exist - creating it... ");
                fwriter = new FileWriter(doorConfig);
                writer = new BufferedWriter(fwriter);
                writer.write("#Max size for X Y Z\r\n");
                writer.write("# -1 to override\r\n");
                writer.write("max_zone_size=20\r\n");
                writer.write("max_door_size=10\r\n");
                writer.write("max_twostate_size=20\r\n");
                writer.write("max_hdoor_size=50\r\n");
                writer.write("#\r\n");
                writer.write("#Advanced Users\r\n");
                writer.write("#\r\n");
                writer.write("allow_overlap_zones=false\r\n");
                writer.write("allow_overlap_doors=false\r\n");
                writer.write("allow_overlap_twostatedoors=false\r\n");
                writer.write("allow_overlap_hdoors=false\r\n");
                writer.write("allow_overlap_triggers=false\r\n");
                writer.write("allow_overlap_redstone=false\r\n");
                writer.write("allow_special_blocks=false\r\n");
                writer.write("allow_console_commands=false\r\n");
            } catch (Exception e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while creating " + doorsConfig, e);
            } finally {
                if (writer != null) {
                    try {
                        writer.flush();
                        writer.close();
                        BlockDoor.LOGGER.info("Successfully created New Config File.");
                    } catch (IOException e) {
                        BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor error " + e);
                    }
                }
                if (fwriter != null) {
                    try {
                        fwriter.close();
                    } catch (IOException e) {
                        BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor error " + e);
                    }
                }
            }
        }
        if (!itemDatabase.exists()) {
            FileWriter fwriter = null;
            BufferedWriter writer = null;
            try {
                BlockDoor.LOGGER.info("BlockDoor Item Database does not exist - creating it...");
                fwriter = new FileWriter(itemDatabase);
                writer = new BufferedWriter(fwriter);
                writer.write("###########################################################################\r\n");
                writer.write("# All items listed must be listed in the following format\r\n");
                writer.write("# No spaces or tabs\r\n");
                writer.write("#   Ex: 1,0=stone\r\n");
                writer.write("#   or\r\n");
                writer.write("#   Ex: 5,0=wood,woodplank\r\n");
                writer.write("#\r\n");
                writer.write("# All items listed must be listed in ascending numerical order\r\n");
                writer.write("#\r\n");
                writer.write("# Database faults will cause blockdoor to revert to its internal database\r\n");
                writer.write("###########################################################################\r\n");
                writer.write("0,0=air\r\n");
                writer.write("1,0=stone\r\n");
                writer.write("2,0=grass\r\n");
                writer.write("3,0=dirt\r\n");
                writer.write("4,0=cobblestone\r\n");
                writer.write("5,0=wood,woodplank,woodenplank,woodenplanks\r\n");
                writer.write("7,0=bedrock,adminium\r\n");
                writer.write("8,0=water,flowingwater\r\n");
                writer.write("9,0=stillwater,stationarywater\r\n");
                writer.write("10,0=lava,flowinglava\r\n");
                writer.write("11,0=stilllava,stationarylava\r\n");
                writer.write("12,0=sand\r\n");
                writer.write("13,0=gravel\r\n");
                writer.write("14,0=goldore\r\n");
                writer.write("15,0=ironore\r\n");
                writer.write("16,0=coalore\r\n");
                writer.write("17,0=log\r\n");
                writer.write("17,1=redwood,darklog,redwoodlog\r\n");
                writer.write("17,2=birch,birchlog\r\n");
                writer.write("18,0=leaves\r\n");
                writer.write("18,1=redwood\r\n");
                writer.write("18,2=birch\r\n");
                writer.write("19,0=sponge\r\n");
                writer.write("20,0=glass\r\n");
                writer.write("21,0=lapisore,lapislazuliore\r\n");
                writer.write("22,0=lapisblock,lapislazuliblock,lapis\r\n");
                writer.write("24,0=sandstone\r\n");
                writer.write("30,0=web,spiderweb,webbing\r\n");
                writer.write("35,0=wool,whitewool,whitecloth,cloth,wool,white\r\n");
                writer.write("35,1=orangewool,orangecloth,orange\r\n");
                writer.write("35,2=magentawool,magentacloth,magenta\r\n");
                writer.write("35,3=lightbluecloth,lightbluewool,lightblue\r\n");
                writer.write("35,4=yellowwool,yellowcloth,yellow\r\n");
                writer.write("35,5=lightgreenwool,lightgreencloth,limecloth,limewool,lightgreen,lime\r\n");
                writer.write("35,6=pinkwool,pinkcloth,pink\r\n");
                writer.write("35,7=greycloth,greywool,graywool,graycloth,grey,gray\r\n");
                writer.write("35,8=lightgreycloth,lightgreywool,lightgraywool,lightgraycloth,lightgrey,lightgray\r\n");
                writer.write("35,9=cyancloth,cyanwool,cyan\r\n");
                writer.write("35,10=purplecloth,purplewool,purple\r\n");
                writer.write("35,11=bluecloth,bluewool,blue\r\n");
                writer.write("35,12=brownwool,browncloth,brown\r\n");
                writer.write("35,13=darkgreenwool,darkgreencloth,darkgreen\r\n");
                writer.write("35,14=redcloth,redwool,red\r\n");
                writer.write("35,15=blackcloth,blackwool,black\r\n");
                writer.write("41,0=goldblock\r\n");
                writer.write("42,0=ironblock\r\n");
                writer.write("43,0=doublestoneslab,doublestep,doubleslab\r\n");
                writer.write("43,1=doublesandstoneslab,doublesandslab,sandstone,sandstoneslab,sandstonestep\r\n");
                writer.write("43,2=doublewoodenslab,doublewoodslab,wood,doublewoodstep,doublewoodenstep\r\n");
                writer.write("43,3=doublecobblestoneslab,doublecobbleslab,cobblestone,cobblestoneslab,cobblestonestep\r\n");
                writer.write("44,4=doublebrickslab\r\n");
                writer.write("44,5=doublestonebrickslab\r\n");
                writer.write("44,0=stoneslab,step,slab,stoneslab,stonestep\r\n");
                writer.write("44,1=sandstoneslab,sandslab,sandstone,sandstonestep\r\n");
                writer.write("44,2=woodenslab,woodslab,wood,woodenstep\r\n");
                writer.write("44,3=cobblestoneslab,cobbleslab,cobblestone,cobblestonestep\r\n");
                writer.write("44,4=brickslab\r\n");
                writer.write("44,5=stonebrickslab\r\n");
                writer.write("45,0=brick\r\n");
                writer.write("47,0=bookcase,bookshelf\r\n");
                writer.write("48,0=mossycobblestone,mossycobble,mossstone,mossystone\r\n");
                writer.write("49,0=obsidian\r\n");
                writer.write("51,0=fire\r\n");
                writer.write("56,0=diamondore\r\n");
                writer.write("57,0=diamondblock\r\n");
                writer.write("73,0=redstoneore\r\n");
                writer.write("74,0=glowingredstoneore\r\n");
                writer.write("75,0=redstonetorchoff\r\n");
                writer.write("76,0=redstonetorchon,redstonetorch\r\n");
                writer.write("79,0=ice\r\n");
                writer.write("80,0=snowblock\r\n");
                writer.write("81,0=cactus\r\n");
                writer.write("82,0=clay\r\n");
                writer.write("85,0=fence\r\n");
                writer.write("86,0=pumpkin\r\n");
                writer.write("87,0=redmossycobblestone,netherrack,netherrock,redcobble,bloodstone\r\n");
                writer.write("88,0=mud,soulsand,slowsand,nethersand\r\n");
                writer.write("89,0=glowstone,brittlegold,glowstoneblock\r\n");
                writer.write("91,0=jack-o-lantern,jackolantern\r\n");
                writer.write("98,0=stonebrick\r\n");
                writer.write("98,1=mossystonebrick\r\n");
                writer.write("98,2=crackedstonebrick\r\n");
                writer.write("99,0=redmushroomflesh\r\n");
                writer.write("99,1=hugeredmushroomcorner1\r\n");
                writer.write("99,2=hugeredmushroomside2\r\n");
                writer.write("99,3=hugeredmushroomcorner3\r\n");
                writer.write("99,4=hugeredmushroomside4\r\n");
                writer.write("99,5=hugeredmushroomtop5\r\n");
                writer.write("99,6=hugeredmushroomside6\r\n");
                writer.write("99,7=hugeredmushroomcorner7\r\n");
                writer.write("99,8=hugeredmushroomside8\r\n");
                writer.write("99,9=hugeredmushroomcorner9\r\n");
                writer.write("99,10=hugeredmushroomstem\r\n");
                writer.write("100,0=redmushroomflesh\r\n");
                writer.write("100,1=hugebrownmushroomcorner1\r\n");
                writer.write("100,2=hugebrownmushroomside2\r\n");
                writer.write("100,3=hugebrownmushroomcorner3\r\n");
                writer.write("100,4=hugebrownmushroomside4\r\n");
                writer.write("100,5=hugebrownmushroomtop5\r\n");
                writer.write("100,6=hugebrownmushroomside6\r\n");
                writer.write("100,7=hugebrownmushroomcorner7\r\n");
                writer.write("100,8=hugebrownmushroomside8\r\n");
                writer.write("100,9=hugebrownmushroomcorner9\r\n");
                writer.write("100,10=hugebrownmushroomstem\r\n");
                writer.write("101,0=ironbars\r\n");
                writer.write("102,0=glasspane,glasspanel\r\n");
                writer.write("103,0=melon,watermelon\r\n");
                writer.write("107,0=fencegate\r\n");
                writer.write("110,0=mycelium\r\n");
                writer.write("111,0=lilypad\r\n");
                writer.write("112,0=netherbrick\r\n");
                writer.write("113,0=netherbrickfence\r\n");
                writer.write("121,0=endstone,whitestone\r\n");
                writer.write("123,0=redstonelamp\r\n");
                writer.write("125,0=oakwooddoubleslab, oakdoubleslab\r\n");
                writer.write("125,1=sprucewooddoubleslab,sprucedoubleslab\r\n");
                writer.write("125,2=birchwooddoubleslab,birchdoubleslab\r\n");
                writer.write("125,3=junglewooddoubleslab,jungledoubleslab\r\n");
                writer.write("126,0=oakwoodslab, oakslab\r\n");
                writer.write("126,1=sprucewoodslab,spruceslab\r\n");
                writer.write("126,2=birchwoodslab,birchslab\r\n");
                writer.write("126,3=junglewoodslab,jungleslab\r\n");
                writer.write("128,0=sandstonestairs\r\n");
                writer.write("129,0=emeraldore\r\n");
                writer.write("133,0=emeraldblock\r\n");
                writer.write("134,0=sprucewoodstairs\r\n");
                writer.write("135,0=birchwoodstairs\r\n");
                writer.write("136,0=junglewoodstairs\r\n");
                writer.write("139,0=cobblestonewall\r\n");
                writer.write("143,0=woodenbutton\r\n");
                writer.write("151,0=lightsensor,daylightsensor\r\n");
                writer.write("152,0=blockofredstone,redstoneblock\r\n");
                writer.write("153,0=netherquartzore\r\n");
                writer.write("155,0=netherquartzblock,quartzblock\r\n");
            } catch (Exception e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while creating " + itemsDatabase, e);
                BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                itemDatabaseEr = true;
            } finally {
                if (writer != null) {
                    try {
                        writer.flush();
                        writer.close();
                        BlockDoor.LOGGER.info("Successfully created New Item Database.");
                    } catch (IOException e) {
                        BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor error " + e);
                        BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                        itemDatabaseEr = true;
                    }
                }
                if (fwriter != null) {
                    try {
                        fwriter.close();
                    } catch (IOException e) {
                        BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor error " + e);
                        BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                        itemDatabaseEr = true;
                    }
                }
            }
        }
        if (!uzoneDatabase.exists()) {
            FileWriter fwriter = null;
            BufferedWriter writer = null;
            try {
                BlockDoor.LOGGER.info("BlockDoor Mob Database does not exist - creating it... ");
                fwriter = new FileWriter(uzoneDatabase);
                writer = new BufferedWriter(fwriter);
                writer.write("#Case Sensitive CraftBukkit Mob Class file name = names to use in game are comma seperated\r\n");
                writer.write("CraftPig = pig,craftpig\r\n");
                writer.write("CraftCow = cow,craftcow\r\n");
                writer.write("CraftChicken = chicken,craftchicken\r\n");
                writer.write("CraftSheep = sheep,craftsheep\r\n");
                writer.write("CraftSquid = squid,craftsquid\r\n");
                writer.write("CraftWolf = wolf,craftwolf\r\n");
                writer.write("CraftZombie = zombie,craftzombie\r\n");
                writer.write("CraftSpider = spider,craftspider\r\n");
                writer.write("CraftSlime = slime,craftslime\r\n");
                writer.write("CraftCreeper = creeper,craftcreeper\r\n");
                writer.write("CraftGiant = giant,craftgiant\r\n");
                writer.write("CraftPigZombie = pigzombie,craftpigzombie\r\n");
                writer.write("CraftGhast = ghast,craftghast\r\n");
                writer.write("CraftSkeleton = skeleton,craftskeleton\r\n");
                writer.write("CraftEnderman = enderman,craftenderman\r\n");
                writer.write("CraftCaveSpider = cavespider,craftcavespider\r\n");
                writer.write("CraftSilverfish = silverfish,craftsilverfish\r\n");
                writer.write("CraftEnderDragon = enderdragon,craftenderdragon\r\n");
                writer.write("CraftMushroomCow = mushroomcow,mooshroomcow,craftmushroomcow\r\n");
                writer.write("CraftSnowman = snowman,craftsnowman\r\n");
                writer.write("CraftBlaze = blaze,craftblaze\r\n");
                writer.write("CraftVillager = villager,testificate\r\n");
                writer.write("CraftMagmaCube = magmacube,craftmagmacube\r\n");
                writer.write("CraftBat = bat,craftbat\r\n");
                writer.write("CraftWitch = witch,craftwitch\r\n");
                writer.write("CraftWither = wither,craftwither\r\n");
                writer.write("CraftWitherSkull = witherskull,craftwitherskull\r\n");
                writer.write("CraftHorse = horse,crafthorse\r\n");
            } catch (Exception e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while creating " + uzoneDatabase, e);
            } finally {
                if (writer != null) {
                    try {
                        writer.flush();
                        writer.close();
                        BlockDoor.LOGGER.info("Successfully created Mob Database.");
                    } catch (IOException e) {
                        BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor error " + e);
                    }
                }
                if (fwriter != null) {
                    try {
                        fwriter.close();
                    } catch (IOException e) {
                        BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor error " + e);
                    }
                }
            }
        }
        reloadFile();
    }

    @SuppressWarnings("unused")
    public void reloadFile() {
        if (reload == true) {
            BlockDoor.LOGGER.info("Datafile reload initiated...");
            reInit();
        }
        if (oldDoorV1Database.exists() || newDoorV2Database.exists()) {
            int doorCount = 0;
            int twostateCount = 0;
            int hybridDoorCount = 0;
            int triggerCount = 0;
            int zoneCount = 0;
            int redTrigCount = 0;

            boolean updateOverlaps = false;

            SingleStateDoor d;
            HDoor hd;
            TwoStateDoor tsd;
            Zone z;
            Trigger t;

            try {
                Scanner scanner;
                if (oldDoorV1Database.exists() && firstV2run) {
                    String binDatabase = loadBinDatabase(oldDoorV1Database);
                    scanner = new Scanner(binDatabase);
                    updateOverlaps = true;
                }
                else {
                    String binDatabase = loadBinDatabase(newDoorV2Database);
                    scanner = new Scanner(binDatabase);
                }
                if (BlockDoor.LOADFAIL) return;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (line.startsWith("#") || line.length() == 0)
                        continue;
                    if (line.startsWith("DOOR:")) {
                        d = new SingleStateDoor(line);
                        if (d != null && d.door_creator != "FAILED") {
                            allSSDoorsMap.put(d, d);
                            addSingleStateToBlockMap(d);
                            doorCount++;
                        }
                    }
                    else if (line.startsWith("HDOOR:")) {
                        hd = new HDoor(line);
                        if (hd != null && hd.door_creator != "FAILED") {
                            allHDoorsMap.put(hd, hd);
                            addHybridDoorToBlockMap(hd);
                            hybridDoorCount++;
                        }
                    }
                    else if (line.startsWith("TWOSTATEDOOR:")) {
                        tsd = new TwoStateDoor(line);
                        if (tsd != null && tsd.door_creator != "FAILED") {
                            allTSDoorsMap.put(tsd, tsd);
                            addTwoStateToBlockMap(tsd);
                            twostateCount++;
                        }
                    }
                    else if (line.startsWith("TRIGGER:") || line.startsWith("MYTRIGGER:") || line.startsWith("REDTRIG:")) {
                        t = new Trigger(line);
                        if (t != null && t.trigger_creator != "FAILED") {
                            allTriggersMap.put(t, t);
                            World world = plugin.getWorld(t.trigger_world);
                            if (world != null) {
                                t.world = world;
                                addToBlockMap(world.getBlockAt(t.trigger_x, t.trigger_y, t.trigger_z), t);
                            }
                            if (t.trigger_Type.equals("REDTRIG"))
                                redTrigCount++;
                            else
                                triggerCount++;

                        }
                    }
                    else if (line.startsWith("ZONE:") || line.startsWith("MYZONE:") || line.startsWith("MZONE:") || line.startsWith("AZONE:") ||
                            line.startsWith("PZONE:") || line.startsWith("EZONE:") || line.startsWith("UZONE:")) {
                        if (!firstV2run)
                            z = new Zone(line);
                        else {
                            z = new Zone(convertZone(line));
                        }
                        if (z != null && z.zone_creator != "FAILED") {
                            allZonesMap.put(z, z);
                            addZoneToZoneBlockMap(z);
                            zoneCount++;
                        }
                    }
                }
                scanner.close();
                buildDoorLinks();
            } catch (Exception e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while reading " + newDoorsFile, e);
            }

            plugin.twostatedoorhelper.lock(); // lock all doors on load
            updateTwoStateDoorOverlapRefs(); // Update Overlap References
            updateHDoorOverlapRefs();
            if (updateOverlaps) { // Convert V1 database to V2
                BlockDoor.LOGGER.log(Level.INFO, "BlockDoor updating old datastore. This may take a few moments.");
                BlockDoor.LOGGER.log(Level.INFO, "Please wait...");
                updateOverlapsForV1toV2();
                BlockDoor.LOGGER.log(Level.INFO, "BlockDoor finished updating old datastore.");
            }
            else { // Update Overlap References
                updateSingleStateDoorOverlapRefs();
                updateTriggerOverlapRefs();
                updateZoneOverlapsRefs();
            }

            if (oldDoorV1Database.exists()) {
                if (oldDoorV1Database.renameTo(new File(directory, "blockdoor.bak"))) {
                    BlockDoor.LOGGER.log(Level.INFO, "Old BlockDoor database converted and renamed to: blockdoor.bak");
                    saveDatabase(false);
                }
                else {
                    BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor was unable to convert old V1 database.");
                    BlockDoor.LOGGER.log(Level.SEVERE, "Manually rename or deleted old database. (blockdoor.dat)");
                    saveDatabase(false);
                }

            }
            BlockDoor.LOGGER.log(Level.INFO, "BlockDoor loaded: ");
            BlockDoor.LOGGER.log(Level.INFO, doorCount + " Classic Door(s) " + hybridDoorCount + " Hybrid Door(s) " + twostateCount + " TwoState Door(s)");
            BlockDoor.LOGGER.log(Level.INFO, zoneCount + " Zone(s) " + triggerCount + " Trigger(s) " + redTrigCount + " Redstone Trigger(s)");
        }

        if (doorConfig.exists()) {
            max_ZoneSize = 20;
            max_DoorSize = 10;
            max_hdoorSize = 50;
            max_TwoStateSize = 20;
            overlapTwoStateDoors = false;
            overlapZones = false;
            overlapDoors = false;
            overlapHDoors = false;
            overlapTriggers = false;
            overlapRedstone = false;
            enableSpecialBlocks = false;
            enableConsoleCommands = false;
            Scanner scanner;
            try {
                scanner = new Scanner(doorConfig);
                String line;
                String[] split;
                while (scanner.hasNextLine()) {
                    line = scanner.nextLine();
                    if (line.startsWith("#") || line.length() == 0)
                        continue;
                    if (line.startsWith("max_zone_size=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                max_ZoneSize = Integer.parseInt(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Max Zone Size in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Max Zone Size in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("max_door_size=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                max_DoorSize = Integer.parseInt(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Max Door Size in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Max Door Size in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("max_twostate_size=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                max_TwoStateSize = Integer.parseInt(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Max Two State Door Size in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Max Two State Door Size in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("max_hdoor_size=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                max_hdoorSize = Integer.parseInt(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Max Hybrid State Door Size in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Max Two State Door Size in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_overlap_hdoors=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                overlapHDoors = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Hybrid Door overlap in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Max Two State Door Size in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_overlap_zones=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                overlapZones = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Zone overlap in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Zone overlap in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_overlap_doors=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                overlapDoors = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Door overlap in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Door overlap in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_overlap_twostatedoors=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                overlapTwoStateDoors = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Two State Door overlap in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Two State Door overlap in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_overlap_triggers=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                overlapTriggers = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Trigger overlap in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Trigger overlap in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_overlap_redstone=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                overlapRedstone = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Redstone overlap in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Redstone overlap in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_special_blocks=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                enableSpecialBlocks = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Special Blocks in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Special Blocks in config wrong. Default value used");
                        }
                    }
                    else if (line.startsWith("allow_console_commands=")) {
                        split = line.split("=");
                        if (split.length == 2) {
                            try {
                                enableConsoleCommands = Boolean.parseBoolean(split[1]);
                            } catch (Exception e) {
                                BlockDoor.LOGGER.log(Level.SEVERE, "Console command value in config wrong. Default value used");
                            }
                        }
                        else {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Console command value in config wrong. Default value used");
                        }
                    }
                }
                BlockDoor.LOGGER.info("BlockDoor config file finished loading...");
            } catch (FileNotFoundException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while reading " + doorsConfig, e);
            }
        }
        if (itemDatabase.exists()) {
            Scanner scanner;
            int counter = 0;
            try {
                scanner = new Scanner(itemDatabase);
                String line, objectCode, objectNames;
                String[] split;
                int objectID, objectOffset;
                while (scanner.hasNextLine()) {
                    counter++;
                    line = scanner.nextLine();
                    if (line.startsWith("#") || line.length() == 0)
                        continue;
                    split = line.split("=");
                    objectCode = split[0];
                    objectNames = split[1];
                    split = objectCode.split(",");
                    //test for correct number values. Error out if wrong.
                    objectID = Integer.parseInt(split[0]);
                    objectOffset = Integer.parseInt(split[1]);
                    //***

                    //Make sure Primary type exists before adding subtypes, error if it doesnt
                    if (objectOffset != 0) {
                        //check for primary item type id. Not used right now. Maybe in the future
                        /*
                         * if(ItemsCodes.findItemByID(objectID+",0")==-1){
                         * BlockDoor.logger.log(Level.SEVERE,
                         * "No Primary type for item subtype");
                         * throw new Exception();
                         * }
                         */
                        if (plugin.itemcodeshelper.fillDatabase(objectNames, objectCode) != -1) {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Duplicate database entry");
                            throw new Exception();
                        }
                    }
                    else {
                        if (plugin.itemcodeshelper.findItemByID(objectCode) != -1) {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Duplicate database entry");
                            throw new Exception();
                        }
                        if (plugin.itemcodeshelper.fillDatabase(objectNames, objectCode) != -1) {
                            BlockDoor.LOGGER.log(Level.SEVERE, "Duplicate database entry");
                            throw new Exception();
                        }
                    }
                }
                BlockDoor.LOGGER.info("BlockDoor Item Database finished loading...");
            } catch (FileNotFoundException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while reading " + itemsDatabase, e);
                BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                itemDatabaseEr = true;
            } catch (Exception e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Error on line " + counter + " of item database");
                BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                itemDatabaseEr = true;
            }
        }
        else {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor item database Not Found");
            BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
            itemDatabaseEr = true;
        }
        if (uzoneDatabase.exists()) {
            Scanner scanner;
            int counter = 0;
            try {
                scanner = new Scanner(uzoneDatabase);
                String line, mobClassString, mobNames;
                String[] split;
                while (scanner.hasNextLine()) {
                    counter++;
                    line = scanner.nextLine();
                    if (line.startsWith("#") || line.length() == 0)
                        continue;
                    split = line.split("=");
                    if (split.length != 2)
                        throw new Exception();
                    mobClassString = split[0].trim();
                    mobNames = split[1];
                    split = mobNames.split(",");
                    String mobClassSimpleName = mobClassString;
                    if (mobs.get(mobClassSimpleName) == null)
                        mobs.put(mobClassSimpleName, new ArrayList<String>());
                    for(String name : split) {
                        mobs.get(mobClassSimpleName).add(name.trim().toLowerCase());
                    }
                }
                BlockDoor.LOGGER.info("BlockDoor Mob Database finished loading...");
            } catch (ClassNotFoundException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Invalid Mob database entry");
                BlockDoor.LOGGER.log(Level.SEVERE, "Error on line " + counter + " of Mob database");
                BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                uzoneDatabaseEr = true;
            } catch (FileNotFoundException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while reading " + uzoneDatabase, e);
                BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                uzoneDatabaseEr = true;
            } catch (Exception e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "Exception while reading " + uzoneDatabase, e);
                BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
                uzoneDatabaseEr = true;
            }
            System.out.println("Blockdoor Finished loading " + counter + " mobs...");
        }
        else {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor Mob database Not Found");
            BlockDoor.LOGGER.log(Level.SEVERE, "Using internal database...");
            uzoneDatabaseEr = true;
        }

        if (reload) {
            BlockDoor.LOGGER.info("Datafile Reload completed...");
            reload = false;
        }
    }

    private Zone convertZone(String in_string) {
        String[] split = in_string.split(":");
        List<Link> links = new ArrayList<Link>();
        Zone z = new Zone("", "", "", "", "");
        z.zone_Type = split[0];
        if (split.length == 12) {

            z.zone_creator = split[1];
            z.zone_name = split[2];

            z.zone_start_x = Integer.parseInt(split[3]);
            z.zone_start_y = Integer.parseInt(split[4]);
            z.zone_start_z = Integer.parseInt(split[5]);

            z.zone_end_x = Integer.parseInt(split[6]);
            z.zone_end_y = Integer.parseInt(split[7]);
            z.zone_end_z = Integer.parseInt(split[8]);
            z.coordsSet = Boolean.parseBoolean(split[9]);
            z.occupants = Integer.parseInt(split[10]);

            z.zone_world = split[11];
            z.world = BlockDoor.plugin.getWorld(z.zone_world);
        }
        else if (split.length == 13) {
            if (!split[0].equals("UZONE")) {
                z.zone_creator = split[1];
                z.zone_name = split[2];

                z.zone_start_x = Integer.parseInt(split[3]);
                z.zone_start_y = Integer.parseInt(split[4]);
                z.zone_start_z = Integer.parseInt(split[5]);

                z.zone_end_x = Integer.parseInt(split[6]);
                z.zone_end_y = Integer.parseInt(split[7]);
                z.zone_end_z = Integer.parseInt(split[8]);
                z.coordsSet = Boolean.parseBoolean(split[9]);
                z.occupants = Integer.parseInt(split[10]);

                z.zone_world = split[11];
                z.world = BlockDoor.plugin.getWorld(z.zone_world);
                String[] linksplit = split[12].split("\\|");
                Link l;
                for(int i = 0; i < linksplit.length; i++) {
                    l = new Link(linksplit[i], z.zone_world);
                    if (l.link_creator != "FAILED")
                        links.add(l);
                }
                z.links = links;
            }
            else {
                z.zone_creator = split[1];
                z.zone_name = split[2];

                z.uzone_trigger = split[3];

                z.zone_start_x = Integer.parseInt(split[4]);
                z.zone_start_y = Integer.parseInt(split[5]);
                z.zone_start_z = Integer.parseInt(split[6]);

                z.zone_end_x = Integer.parseInt(split[7]);
                z.zone_end_y = Integer.parseInt(split[8]);
                z.zone_end_z = Integer.parseInt(split[9]);
                z.coordsSet = Boolean.parseBoolean(split[10]);
                z.occupants = Integer.parseInt(split[11]);

                z.zone_world = split[12];
                z.world = BlockDoor.plugin.getWorld(z.zone_world);
            }
        }
        else if (split.length == 14) {

            z.zone_creator = split[1];
            z.zone_name = split[2];

            z.uzone_trigger = split[3];

            z.zone_start_x = Integer.parseInt(split[4]);
            z.zone_start_y = Integer.parseInt(split[5]);
            z.zone_start_z = Integer.parseInt(split[6]);

            z.zone_end_x = Integer.parseInt(split[7]);
            z.zone_end_y = Integer.parseInt(split[8]);
            z.zone_end_z = Integer.parseInt(split[9]);
            z.coordsSet = Boolean.parseBoolean(split[10]);
            z.occupants = Integer.parseInt(split[11]);

            z.zone_world = split[12];
            z.world = BlockDoor.plugin.getWorld(z.zone_world);

            String[] linksplit = split[13].split("\\|");
            Link l;
            for(int i = 0; i < linksplit.length; i++) {
                l = new Link(linksplit[i], z.zone_world);
                if (l.link_creator != "FAILED")
                    links.add(l);
            }
            z.links = links;
        }
        else {
            z.zone_creator = "FAILED";
            z.coordsSet = false;
        }
        return z;
    }

    //Overlap updates from v1 to v2
    private void updateOverlapsForV1toV2() {
        for(SingleStateDoor startDoor : allSSDoorsMap.values()) {
            for(SingleStateDoor testDoor : allSSDoorsMap.values()) {
                if (startDoor != testDoor) {
                    doorCoordSearch(startDoor, testDoor);
                }
            }
        }

        for(Trigger startTrig : allTriggersMap.values()) {
            for(Trigger testTrig : allTriggersMap.values()) {
                if (startTrig != testTrig) {
                    if (startTrig.isOverlaping(testTrig)) {
                        TrigOverlaps to = new TrigOverlaps(startTrig);
                        testTrig.overlaps.put(to, to);
                    }
                }
            }
        }

        for(Zone startZone : allZonesMap.values()) {
            for(Zone testZone : allZonesMap.values()) {
                if (startZone != testZone) {
                    zoneCoordSearch(startZone, testZone);
                }
            }
        }
    }

    private void zoneCoordSearch(Zone startZone, Zone testZone) {
        for(int x = testZone.zone_start_x; x <= testZone.zone_end_x; x++)
            for(int y = testZone.zone_start_y; y <= testZone.zone_end_y; y++)
                for(int z = testZone.zone_start_z; z <= testZone.zone_end_z; z++) {
                    if (startZone.zoneOverlap(x, y, z)) {
                        ZoneOverlaps zo = new ZoneOverlaps(testZone);
                        startZone.overlaps.put(zo, zo);
                        return;
                    }
                }
    }

    private void doorCoordSearch(SingleStateDoor startDoor, SingleStateDoor testDoor) {
        for(int x = testDoor.door_start_x; x <= testDoor.door_end_x; x++)
            for(int y = testDoor.door_start_y; y <= testDoor.door_end_y; y++)
                for(int z = testDoor.door_start_z; z <= testDoor.door_end_z; z++) {
                    if (startDoor.doorOverlap(x, y, z)) {
                        DoorOverlaps ol = new DoorOverlaps(testDoor);
                        startDoor.doorOverlaps.put(ol, ol);
                        startDoor.overlapAmount = startDoor.doorOverlaps.size();
                        return;
                    }
                }
    }

    private void updateSingleStateDoorOverlapRefs() {
        for(SingleStateDoor ssd : allSSDoorsMap.values()) {
            for(DoorOverlaps ol : ssd.doorOverlaps.values()) {
                SingleStateDoor overlap = plugin.singlestatedoorhelper.findDoor(ol);
                ol.d = overlap;
            }
        }
    }

    private void updateTwoStateDoorOverlapRefs() {
        for(TwoStateDoor tsd : allTSDoorsMap.values()) {
            for(DoorOverlaps ol : tsd.doorOverlaps.values()) {
                TwoStateDoor overlap = plugin.twostatedoorhelper.findDoor(ol);
                ol.d = overlap;
            }
        }
    }

    private void updateHDoorOverlapRefs() {
        for(HDoor hd : allHDoorsMap.values()) {
            for(HDoorOverlaps ol : hd.hdoorOverlaps.values()) {
                HDoor overlap = plugin.hdoorhelper.findDoor(ol);
                ol.d = overlap;
            }
        }
    }

    private void updateTriggerOverlapRefs() {
        for(Trigger t : allTriggersMap.values()) {
            for(TrigOverlaps to : t.overlaps.values()) {
                to.trig = plugin.triggerhelper.findTrigger(to, t.trigger_Type);
            }
        }
    }

    private void updateZoneOverlapsRefs() {
        for(Zone z : allZonesMap.values()) {
            for(ZoneOverlaps zo : z.overlaps.values()) {
                zo.zone = plugin.zonehelper.findZone(zo);
            }
        }
    }

    private void buildDoorLinks() {
        for(Trigger t : allTriggersMap.values()) {
            for(Link l : t.links) {
                setLinkDoor(l);
            }
        }
        for(Zone z : allZonesMap.values()) {
            for(Link l : z.links) {
                setLinkDoor(l);
            }
        }
    }

    private void setLinkDoor(Link l) {
        if (l.doorType.equals(DoorTypes.ONESTATE)) {
            l.door = plugin.singlestatedoorhelper.findDoor(l.link_name, l.link_creator, l.link_world);
        }
        else if (l.doorType.equals(DoorTypes.TWOSTATE)) {
            l.door = plugin.twostatedoorhelper.findDoor(l.link_name, l.link_creator, l.link_world);
        }
        else if (l.doorType.equals(DoorTypes.HYBRIDSTATE)) {
            l.door = plugin.hdoorhelper.findDoor(l.link_name, l.link_creator, l.link_world);
        }
    }

    private String loadBinDatabase(File doorDatabase) {
        File oldFile = new File(directory, "blockdoor.old");
        if (tmpDoorBinDatabase.exists()) {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor found possable database corruption!");
            BlockDoor.LOGGER.log(Level.SEVERE, "TMP blockdoor database is present in blockdoor data directory!");
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor will not load! Delete or renamed TMP file to blockdoor.dat and try again!");
            BlockDoor.LOADFAIL = true;
            plugin.getServer().getPluginManager().disablePlugin(plugin);
        }
        else if (oldFile.exists()) {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor found possable database corruption!");
            BlockDoor.LOGGER.log(Level.SEVERE, "Old blockdoor database is present in blockdoor data directory!");
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor will not load! Delete or renamed Old file to blockdoor.dat and try again!");
            BlockDoor.LOADFAIL = true;
            plugin.getServer().getPluginManager().disablePlugin(plugin);
        }
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(doorDatabase));
            String input = (String) inputStream.readObject();
            inputStream.close();
            return input;
        } catch (FileNotFoundException e) {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor Cant Find " + doorDatabase.getAbsolutePath());
        } catch (IOException e) {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor Access " + doorDatabase.getAbsolutePath());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void deleteLinks(Door d) {
        //Delete links for all Triggers
        for(Trigger trigger : allTriggersMap.values()) {
            for(Link link : trigger.links) {
                if (link.door.equals(d)) {
                    trigger.links.remove(link);
                    break;
                }
            }
        }
        //Delete links for all zone types
        for(Zone z : allZonesMap.values()) {
            for(Link link : z.links) {
                if (link.door.equals(d)) {
                    z.links.remove(link);
                    break;
                }
            }
        }
        saveDatabase(false);
    }

    public int saveDatabase(boolean exitSave) {
        StringBuffer databin = new StringBuffer();
        if (newDoorV2Database.exists()) { // Check if the database exists, if it does save all blockdoor objects to file.
            int count = 0;
            for(Door d : allSSDoorsMap.values()) {
                databin.append(d.toString() + "\n");
                count++;
            }
            for(Door d : allTSDoorsMap.values()) {
                databin.append(d.toString() + "\n");
                count++;
            }
            for(Door d : allHDoorsMap.values()) {
                databin.append(d.toString() + "\n");
                count++;
            }
            for(Trigger t : allTriggersMap.values()) {
                databin.append(t.toString() + "\n");
                count++;
            }
            for(Zone z : allZonesMap.values()) {
                databin.append(z.toString() + "\n");
                count++;
            }
            if (exitSave)
                safeSaveDatabase(databin);
            else
                quickSaveDatabase(databin);
            return count;
        }
        else
            return -1;
    }

    public void quickSaveDatabase(StringBuffer databin) {
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(newDoorV2Database));
            outputStream.writeObject(databin.toString());
        } catch (IOException e) {
            BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor can not Access " + newDoorV2Database.getAbsolutePath(), e);
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException ex) {
                BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor can not Access " + newDoorV2Database.getAbsolutePath(), ex);
            }
        }
    }

    public void safeSaveDatabase(StringBuffer databin) {
        if (databin.length() == 0) return;
        if (newDoorV2Database.exists()) { // Check if the database exists, if it does save all blockdoor objects to file.      
            ObjectOutputStream outputStream = null;
            try {
                outputStream = new ObjectOutputStream(new FileOutputStream(tmpDoorBinDatabase));
                outputStream.writeObject(databin.toString());
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(tmpDoorBinDatabase));
                String tmpDatabin = (String) inputStream.readObject();
                inputStream.close();
                File oldFile = new File(directory, "blockdoor.old");
                if (oldFile.exists()) oldFile.delete();
                if (tmpDatabin.equals(databin.toString())) {
                    if (newDoorV2Database.exists()) {
                        newDoorV2Database.renameTo(oldFile);
                    }
                    tmpDoorBinDatabase.renameTo(newDoorV2Database);
                    if (oldFile.exists()) oldFile.delete();
                }
                else {
                    BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor detected database corruption. Database was not saved correctly!\n ");
                }
            } catch (IOException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor can not Access door database\n", e);
            } catch (ClassNotFoundException e) {
                BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor can not read temp File. Database was not saved!\n ", e);
                e.printStackTrace();
            } finally {
                try {
                    if (outputStream != null) {
                        outputStream.flush();
                        outputStream.close();
                    }
                } catch (IOException ex) {
                    BlockDoor.LOGGER.log(Level.SEVERE, "BlockDoor can not Access " + newDoorV2Database.getAbsolutePath(), ex);
                }
            }
        }
    }

    private void reInit() {

        //All Single State Doors Map
        allSSDoorsMap.clear();

        //All Two State Doors Map
        allTSDoorsMap.clear();

        //All Hybrid Doors Map
        allHDoorsMap.clear();

        //All Trigger Map
        allTriggersMap.clear();

        //Item database array
        itemcodes.clear();

        //zone occupancy tracker Map
        zoneOccupancyMap.clear();

        //Block Map
        blockMap.clear();

        //All Zones Map
        allZonesMap.clear();

        //All Zones Block Map
        zoneBlockMap.clear();

        //Clear playerSettings Map
        BlockDoorSettings.playerSettings.clear();

        //config variables
        max_DoorSize = 10;
        max_ZoneSize = 20;
        max_TwoStateSize = 20;
        max_hdoorSize = 50;
        overlapZones = false;
        overlapDoors = false;
        overlapTwoStateDoors = false;
        overlapHDoors = false;
        overlapTriggers = false;
        overlapRedstone = false;
        enableConsoleCommands = false;

        //Item database error control variable
        itemDatabaseEr = false;

    }

    public void addToBlockMap(Block block, Object obj) {
        blockMap.put(block, obj);
    }

    public void removeFromBlockMap(Block block) {
        blockMap.remove(block);
    }

    private void addToZoneBlockMap(Block block, Zone zone) {
        zoneBlockMap.put(block, zone);
    }

    private void removeFromZoneBlockMap(Block block) {
        zoneBlockMap.remove(block);
    }

    public void addSingleStateToBlockMap(Door d) {
        for(int x = d.door_start_x; x <= d.door_end_x; x++) {
            for(int y = d.door_start_y; y <= d.door_end_y; y++) {
                for(int z = d.door_start_z; z <= d.door_end_z; z++) {
                    World world = plugin.getWorld(d.door_world);
                    if (world != null) {
                        Block block = world.getBlockAt(x, y, z);
                        addToBlockMap(block, d);
                    }
                }
            }
        }
    }

    public void addZoneToZoneBlockMap(Zone zone) {
        for(int x = zone.zone_start_x; x <= zone.zone_end_x; x++)
            for(int y = zone.zone_start_y; y <= zone.zone_end_y; y++)
                for(int z = zone.zone_start_z; z <= zone.zone_end_z; z++) {
                    World world = plugin.getWorld(zone.zone_world);
                    if (world != null) {
                        Block block = world.getBlockAt(x, y, z);
                        addToZoneBlockMap(block, zone);
                    }
                }
    }

    public void removeZoneFromZoneBlockMap(Zone zone) {
        for(int x = zone.zone_start_x; x <= zone.zone_end_x; x++)
            for(int y = zone.zone_start_y; y <= zone.zone_end_y; y++)
                for(int z = zone.zone_start_z; z <= zone.zone_end_z; z++) {
                    World world = plugin.getWorld(zone.zone_world);
                    if (world != null) {
                        Block block = world.getBlockAt(x, y, z);
                        removeFromZoneBlockMap(block);
                    }
                }
    }

    public void updateOldZoneBlockMap(Zone z) {
        removeZoneFromZoneBlockMap(z);
        for(ZoneOverlaps zo : z.overlaps.values()) {
            addZoneToZoneBlockMap(zo.zone);
        }
    }

    public void removeSingleStateFromBlockMap(SingleStateDoor d) {
        for(int x = d.door_start_x; x <= d.door_end_x; x++) {
            for(int y = d.door_start_y; y <= d.door_end_y; y++) {
                for(int z = d.door_start_z; z <= d.door_end_z; z++) {
                    Block block = plugin.getWorld(d.door_world).getBlockAt(x, y, z);
                    removeFromBlockMap(block);
                }
            }
        }
    }

    public void updateOldSSDBlockMap(SingleStateDoor d) {
        removeSingleStateFromBlockMap(d);
        for(DoorOverlaps ssdo : d.doorOverlaps.values()) {
            addSingleStateToBlockMap(ssdo.d);
        }
    }

    public void addTwoStateToBlockMap(Door d) {
        for(int x = d.door_start_x; x <= d.door_end_x; x++) {
            for(int y = d.door_start_y; y <= d.door_end_y; y++) {
                for(int z = d.door_start_z; z <= d.door_end_z; z++) {
                    World world = plugin.getWorld(d.door_world);
                    if (world != null) {
                        Block block = world.getBlockAt(x, y, z);
                        addToBlockMap(block, d);
                    }
                }
            }
        }
    }

    public void removeTwoStateFromBlockMap(TwoStateDoor tsd) {
        for(int x = tsd.door_start_x; x <= tsd.door_end_x; x++) {
            for(int y = tsd.door_start_y; y <= tsd.door_end_y; y++) {
                for(int z = tsd.door_start_z; z <= tsd.door_end_z; z++) {
                    Block block = plugin.getWorld(tsd.door_world).getBlockAt(x, y, z);
                    removeFromBlockMap(block);
                }
            }
        }
    }

    // Always makes sure that if a door is unlocked in the overlap group it is last to be re-added to the blockmap.
    public void updateOldTSDBlockMap(TwoStateDoor tsd) {
        removeTwoStateFromBlockMap(tsd);
        TwoStateDoor unlocked = null;
        for(DoorOverlaps ol : tsd.doorOverlaps.values()) {
            if (!((TwoStateDoor) ol.d).isLocked()) {
                unlocked = (TwoStateDoor) ol.d;
                continue;
            }
            addTwoStateToBlockMap(ol.d);
        }
        if (unlocked != null) addTwoStateToBlockMap(unlocked);
    }

    public void addHybridDoorToBlockMap(Door d) {
        if (d == null) return;
        for(DoorState block : ((HDoor) d).hDoorContents.values()) {
            World world = plugin.getWorld(d.door_world);
            if (world != null) {
                addToBlockMap(world.getBlockAt(block.x, block.y, block.z), d);
            }
        }
    }

    public void removeHybridDoorFromBlockMap(HDoor hd) {
        for(DoorState block : hd.hDoorContents.values()) {
            World world = plugin.getWorld(hd.door_world);
            if (world != null) {
                removeFromBlockMap(world.getBlockAt(block.x, block.y, block.z));
            }
        }
    }

    public void updateOldHDBlockMap(HDoor hd) {
        removeHybridDoorFromBlockMap(hd);
        for(HDoorOverlaps hdol : hd.hdoorOverlaps.values()) {
            addHybridDoorToBlockMap(hdol.d);
        }
    }

    /**
     * @return the max_DoorSize
     */
    public int getMax_DoorSize() {
        return max_DoorSize;
    }

    /**
     * @return the max_hdoorSize
     */
    public int getMax_hdoorSize() {
        return max_hdoorSize;
    }

    /**
     * @return the overlapHDoors
     */
    public boolean isOverlapHDoors() {
        return overlapHDoors;
    }

    /**
     * @return the max_TwoStateSize
     */
    public int getMax_TwoStateSize() {
        return max_TwoStateSize;
    }

    /**
     * @return the max_ZoneSize
     */
    public int getMax_ZoneSize() {
        return max_ZoneSize;
    }

    /**
     * @return the overlapZones
     */
    public boolean isOverlapZones() {
        return overlapZones;
    }

    /**
     * @return the overlapDoors
     */
    public boolean isOverlapDoors() {
        return overlapDoors;
    }

    /**
     * @return the overlapTwoStateDoors
     */
    public boolean isOverlapTwoStateDoors() {
        return overlapTwoStateDoors;
    }

    /**
     * @return the overlapTriggers
     */
    public boolean isOverlapTriggers() {
        return overlapTriggers;
    }

    /**
     * @return the overlapRedstone
     */
    public boolean isOverlapRedstone() {
        return overlapRedstone;
    }

    /**
     * @return the enableSpecialBlocks
     */
    public boolean isEnableSpecialBlocks() {
        return enableSpecialBlocks;
    }

    /**
     * @return the enableConsoleCommands
     */
    public boolean isEnableConsoleCommands() {
        return enableConsoleCommands;
    }

    /**
     * @return the itemDatabaseEr
     */
    public boolean isItemDatabaseEr() {
        return itemDatabaseEr;
    }

    /**
     * @return the uzoneDatabaseEr
     */
    public boolean isUzoneDatabaseEr() {
        return uzoneDatabaseEr;
    }

    /**
     * @param itemDatabaseEr
     *            the itemDatabaseEr to set
     */
    public void setItemDatabaseEr(boolean itemDatabaseEr) {
        this.itemDatabaseEr = itemDatabaseEr;
    }

    /**
     * @return the permissionsSet
     */
    public boolean isPermissionsSet() {
        return permissionsSet;
    }

    /**
     * @param permissionsSet
     *            the permissionsSet to set
     */
    public void setPermissionsSet(boolean permissionsSet) {
        this.permissionsSet = permissionsSet;
    }

    /**
     * @return the error
     */
    public boolean isError() {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * @param reload
     *            the reload to set
     */
    public void setReload(boolean reload) {
        this.reload = reload;
    }

    /**
     * @return the newDoorsFile
     */
    public String getDoorsLoc() {
        return newDoorsFile;
    }
}