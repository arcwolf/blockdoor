package plugin.arcwolf.blockdoor.Utils;

public class ChunkCoords {

    private int x, z;
    private String world;

    public ChunkCoords(String world, int x, int z) {
        this.world = world;
        this.x = x;
        this.z = z;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getZ() {
        return z;
    }

    /**
     * @return the world
     */
    public String getWorld() {
        return world;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ChunkCoords [x=" + x + ", z=" + z + ", world=" + world + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((world == null) ? 0 : world.hashCode());
        result = prime * result + x;
        result = prime * result + z;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ChunkCoords other = (ChunkCoords) obj;
        if (world == null) {
            if (other.world != null) return false;
        }
        else if (!world.equals(other.world)) return false;
        if (x != other.x) return false;
        if (z != other.z) return false;
        return true;
    }
}
