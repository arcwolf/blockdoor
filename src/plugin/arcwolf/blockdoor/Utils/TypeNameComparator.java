package plugin.arcwolf.blockdoor.Utils;

import java.util.Comparator;

import plugin.arcwolf.blockdoor.DLister;

public class TypeNameComparator implements Comparator<Object> {
    @Override
    public int compare(Object o1, Object o2) {
        DLister TypeN1 = (DLister) o1;
        DLister TypeN2 = (DLister) o2;
        return TypeN1.getTypeName().compareToIgnoreCase(TypeN2.getTypeName());
    }
}