package plugin.arcwolf.blockdoor.Utils;

import org.bukkit.block.Block;

/*
 * Contains all the little special case checks for objects that like to POP into items if the server thinks they are
 * placed wrongly
 */

public class SpecialCaseCheck {

    public static boolean isDoor(int blockId) {
        switch (blockId) {
            case 64: // Wood Door
            case 71: // Iron Door
                return true;
            default: // not a door
                return false;
        }
    }

    public static boolean isGrassOrDirt(int blockId) {
        switch (blockId) {
            case 2: // Grass
            case 3: // Dirt
                return true;
            default: // Not grass or dirt
                return false;
        }
    }

    public static boolean isWater(int blockId) {
        switch (blockId) {
            case 8: // Flowing Water
            case 9: // Water source
                return true;
            default: // not water
                return false;
        }
    }

    public static boolean isLava(int blockId) {
        switch (blockId) {
            case 10: // Flowing Lava
            case 11: // Lava source
                return true;
            default: // not Lava
                return false;
        }
    }

    public static boolean isJungleLog(Block block) {
        return block.getTypeId() == 17 && block.getData() == 3;
    }

    public static boolean isFence(Block block) {
        return block.getTypeId() == 85 || block.getTypeId() == 113;
    }

    public static boolean isValidSolid(Block block) {
        int blockId = block.getTypeId();
        if ((blockId == 44 || blockId == 126) && block.getData() > 7) return true; // Stone and Wood Slab Special Case Check
        switch (blockId) {
            case 1: // Stone
            case 2: // Grass
            case 3: // Dirt
            case 4: // Cobblestone
            case 5: // Wood Planks
            case 7: // Bedrock
            case 12: // Sand
            case 13: // Gravel
            case 14: // Gold Ore
            case 15: // Iron Ore
            case 16: // Coal ore
            case 17: // Log
            case 18: // Leaves
            case 19: // Sponge
            case 21: // Lapis Lazuli Ore
            case 22: // Lapis Lazuli Block
            case 23: // Dispenser
            case 24: // Sandstone
            case 35: // Wool
            case 41: // Gold Block
            case 42: // Iron Block
            case 43: // Double Slab
            case 45: // Bricks
            case 47: // Bookshelf
            case 48: // Moss Stone
            case 49: // Obsidian
            case 56: // Diamond Ore
            case 57: // Diamond Block
            case 61: // Furnace off
            case 62: // Furnace on
            case 73: // Redstone Ore
            case 74: // Glowing Redstone Ore
            case 80: // Snow Block
            case 82: // Clay
            case 86: // Pumpkin
            case 87: // Netherrack
            case 88: // Soul Sand
            case 89: // Glowstone
            case 91: // Jack o lantern
            case 97: // Monster Egg
            case 98: // Stone Bricks
            case 103: // Melon Block
            case 110: // Mycelium
            case 112: // Nether Brick
            case 120: // Ender Portal Block
            case 121: // End Stone
            case 123: // Redstone Lamp
            case 124: // Redstone Lamp on
            case 125: // Wood Double Slab
            case 133: // Emerald Block
            case 137: // Command Block
            case 152: // Redstone Block
            case 153: // Quartz Ore
            case 154: // Hopper
            case 155: // Quartz Block
            case 158: // Dropper
                return true;
            default: // Not valid solid
                return false;
        }
    }

    public static boolean isValidMushroomBlock(Block block) {
        int blockId = block.getTypeId();
        if ((blockId == 44 || blockId == 126) && block.getData() > 7) return true; // Stone and Wood Slab Special Case Check
        switch (blockId) {
            case 1: // Stone
            case 2: // Grass
            case 3: // Dirt
            case 4: // Cobblestone
            case 5: // Wood Planks
            case 7: // Bedrock
            case 12: // Sand
            case 13: // Gravel
            case 14: // Gold Ore
            case 15: // Iron Ore
            case 16: // Coal ore
            case 17: // Log
            case 18: // Leaves
            case 19: // Sponge
            case 21: // Lapis Lazuli Ore
            case 22: // Lapis Lazuli Block
            case 23: // Dispenser
            case 24: // Sandstone
            case 35: // Wool
            case 41: // Gold Block
            case 42: // Iron Block
            case 43: // Double Slab
            case 45: // Bricks
            case 47: // Bookshelf
            case 48: // Moss Stone
            case 49: // Obsidian
            case 56: // Diamond Ore
            case 57: // Diamond Block
            case 61: // Furnace off
            case 62: // Furnace on
            case 73: // Redstone Ore
            case 74: // Glowing Redstone Ore
            case 80: // Snow Block
            case 82: // Clay
            case 86: // Pumpkin
            case 87: // Netherrack
            case 88: // Soul Sand
            case 91: // Jack o lantern
            case 97: // Monster Egg
            case 98: // Stone Bricks
            case 103: // Melon Block
            case 110: // Mycelium
            case 112: // Nether Brick
            case 121: // End Stone
            case 123: // Redstone Lamp
            case 125: // Wood Double Slab
            case 133: // Emerald Block
            case 137: // Command Block
            case 152: // Redstone Block
            case 153: // Quartz Ore
            case 155: // Quartz Block
            case 158: // Dropper
                return true;
            default: // Not Valid block
                return false;
        }
    }

    public static boolean isValidNetherWartBlock(int blockId) {
        return blockId == 88; // Soul Sand
    }

    public static boolean isValidCactusBlock(int blockId) {
        return blockId == 12; // Sand
    }

    public static boolean isValidFarmland(int blockId) {
        return blockId == 60; // Farmland
    }

    public static boolean isRedstonePowerSource(Block block) {
        return isRedstonePowerSource(block.getTypeId());
    }

    public static boolean isRedstonePowerSource(int blockId) {
        switch (blockId) {
            case 28: // Detector Rail
            case 55: // Redstone Wire
            case 64: // Wooden Door
            case 69: // Lever
            case 70: // Stone Pressure Plate
            case 71: // Iron Door
            case 72: // Wood Pressure Plate
            case 75: // Redstone Torch inactive
            case 76: // Redstone Torch active
            case 77: // Stone Button
            case 96: // TrapDoor
            case 131: // Tripwire Hook
            case 143: // Wooden Button
            case 146: // Trapped Chest
            case 147: // Light Weighted Pressure Plate
            case 148: // Heavy Weighted Pressure Plate
            case 151: // Daylight Sensor
                return true;
            default: // Not redstone source
                return false;
        }
    }

    public static boolean isInvalidBlock(int blockId) {
        switch (blockId) {
            case 26: // Bed
            case 29: // Sticky Piston
            case 33: // Piston
            case 34: // Piston head
            case 36: // Move by Piston Block
            case 46: // TNT
            case 52: // Monster Spawner
            case 90: // Nether Portal
            case 92: // Cake
            case 119: // End Portal
            case 122: // Dragon egg
            case 137: // Command Block
            case 138: // Beacon
            case 144: // Mob Heads
            case 145: // Anvil
                return true;
            default: // Valid Block
                return false;
        }
    }

    public static boolean isSpecialBlock(int blockId) {
        switch (blockId) {
            case 6: // Sapling
            case 8: // Flowing Water
            case 9: // Water Source
            case 10: // Flowing Lava
            case 11: // Lava Source
            case 12: // Sand
            case 13: // Gravel
            case 27: // Powered Rail
            case 28: // Detector Rail
            case 31: // Grass
            case 32: // Dead Bush
            case 37: // Dandelion
            case 38: // Rose
            case 39: // Brown Mushroom
            case 40: // Red Mushroom
            case 50: // Torch
            case 51: // Fire
            case 55: // Redstone Wire
            case 59: // Wheat
            case 64: // Wood Door
            case 65: // Ladder
            case 66: // Rail
            case 69: // Lever
            case 70: // Stone Pressure Plate
            case 71: // Iron Door
            case 72: // Wooden Pressure Plate
            case 75: // Redstone Torch inactive
            case 76: // Redstone Torch active
            case 77: // Stone Button
            case 81: // Cactus
            case 83: // Sugar Cane
            case 93: // Redstone Repeater inactive
            case 94: // Redstone Repeater active
            case 96: // Trapdoor
            case 104: // Pumpkin Stem
            case 105: // Melon Stem
            case 106: // Vine
            case 111: // Lily Pad
            case 115: // Nether Wart
            case 127: // Cocoa Plant
            case 131: // Tripwire hook
            case 132: // tripwire
            case 140: // flower pot
            case 141: // Carrot Plant
            case 142: // Potatoe Plant
            case 143: // Wood Button
            case 147: // Light Weight Pressure Plate
            case 148: // Heavy Weight Pressure Plate
            case 149: // Redstone Comparator inactive
            case 150: // Redstone Comparator active
            case 157: // Activator Rail
                return true;
            default: // Not special
                return false;
        }
    }

    public static boolean isSpecialBlock(Block block) {
        return isSpecialBlock(block.getTypeId());
    }
}