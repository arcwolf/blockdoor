package plugin.arcwolf.blockdoor.Triggers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.World;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Link;

public class Trigger {

    public String trigger_Type = "";
    public String trigger_name = "";
    public String trigger_creator = "";
    public String trigger_world = "";

    public World world;

    public int trigger_x, trigger_y, trigger_z;
    public boolean coordsSet;

    public Map<TrigOverlaps, TrigOverlaps> overlaps = new HashMap<TrigOverlaps, TrigOverlaps>();

    public List<Link> links = new ArrayList<Link>();

    public Trigger(String trigType, String in_name, String in_creator, String in_world) {
        trigger_Type = trigType;
        trigger_name = in_name;
        trigger_creator = in_creator;
        coordsSet = false;
        trigger_world = in_world;
        world = BlockDoor.plugin.getWorld(trigger_world);
    }

    public Trigger(String in_string) {
        String[] split = in_string.split(":");
        trigger_Type = split[0];
        trigger_creator = split[1];
        trigger_name = split[2];

        trigger_x = Integer.parseInt(split[3]);
        trigger_y = Integer.parseInt(split[4]);
        trigger_z = Integer.parseInt(split[5]);
        coordsSet = Boolean.parseBoolean(split[6]);

        trigger_world = split[7];
        world = BlockDoor.plugin.getWorld(trigger_world);

        if (split.length == 9) { // V1 database
            String[] linksplit = split[8].split("\\|");
            Link l;
            for(int i = 0; i < linksplit.length; i++) {
                l = new Link(linksplit[i], trigger_world);
                if (l.link_creator != "FAILED") {
                    links.add(l);
                }
            }
        }
        else if (split.length == 10) { // V2 database
            String[] linksplit = split[8].split("\\|");
            Link l;
            for(int i = 0; i < linksplit.length; i++) {
                l = new Link(linksplit[i], trigger_world);
                if (l.link_creator != "FAILED") {
                    links.add(l);
                }
            }
            String[] overlapSplit = split[9].split("\\|");
            TrigOverlaps to;
            for(int i = 0; i < overlapSplit.length; i++) {
                to = new TrigOverlaps(overlapSplit[i]);
                overlaps.put(to, to);
            }
        }
        else if (split.length < 8 || split.length > 10) {
            trigger_creator = "FAILED";
            coordsSet = false;
        }
    }

    public final boolean isOverlaping(Trigger t) {
        return trigger_x == t.trigger_x && trigger_y == t.trigger_y && trigger_z == t.trigger_z && trigger_world.equals(t.trigger_world);
    }

    public final void processLinks() {
        for(Link l : links) {
            l.process();
        }
    }

    public final String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(trigger_Type);
        builder.append(":");
        builder.append(trigger_creator);
        builder.append(":");
        builder.append(trigger_name);
        builder.append(":");

        builder.append(Integer.toString(trigger_x));
        builder.append(":");
        builder.append(Integer.toString(trigger_y));
        builder.append(":");
        builder.append(Integer.toString(trigger_z));
        builder.append(":");

        builder.append(Boolean.toString(coordsSet));
        builder.append(":");
        builder.append(trigger_world);
        builder.append(":");

        for(Link l : links) {
            if (l.door == null) {
                BlockDoor.LOGGER.info(trigger_Type + " named: " + trigger_name + " linked to " + l.doorType.name() + " door called " + l.link_name);
                BlockDoor.LOGGER.info("Location: " + trigger_world + ": " + trigger_x + "," + trigger_y + "," + trigger_z + " and was set to " + l.linkType.name());
                BlockDoor.LOGGER.info("The door doesnt exist! The link was not saved!");
                BlockDoor.LOGGER.info("*************************");
                continue;
            }
            builder.append(l.link_name);
            builder.append(" ");
            builder.append(l.link_creator);
            builder.append(" ");
            builder.append(l.linkType.name());
            builder.append(" ");
            builder.append(l.doorType.name());
            builder.append("|");
        }
        builder.append(":");
        for(TrigOverlaps to : overlaps.values()) {
            builder.append(to.creator);
            builder.append(",");
            builder.append(to.name);
            builder.append(",");
            builder.append(to.world);
            builder.append("|");
        }
        return builder.toString();
    }

    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((trigger_creator == null) ? 0 : trigger_creator.hashCode());
        result = prime * result + ((trigger_Type == null) ? 0 : trigger_Type.hashCode());
        result = prime * result + ((trigger_name == null) ? 0 : trigger_name.hashCode());
        result = prime * result + ((trigger_world == null) ? 0 : trigger_world.hashCode());
        return result;
    }

    public final boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Trigger other = (Trigger) obj;
        if (trigger_creator == null) {
            if (other.trigger_creator != null) return false;
        }
        else if (!trigger_creator.equals(other.trigger_creator)) return false;
        if (trigger_Type == null) {
            if (other.trigger_Type != null) return false;
        }
        else if (!trigger_Type.equals(other.trigger_Type)) return false;
        if (trigger_name == null) {
            if (other.trigger_name != null) return false;
        }
        else if (!trigger_name.equals(other.trigger_name)) return false;
        if (trigger_world == null) {
            if (other.trigger_world != null) return false;
        }
        else if (!trigger_world.equals(other.trigger_world)) return false;
        return true;
    }
}
