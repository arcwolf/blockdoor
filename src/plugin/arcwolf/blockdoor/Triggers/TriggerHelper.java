package plugin.arcwolf.blockdoor.Triggers;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Utils.SpecialCaseCheck;

public class TriggerHelper {

    public BlockDoor plugin;
    public DataWriter dataWriter;

    public TriggerHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public Trigger addTrigger(String in_name, String in_creator, String in_world, String in_type, Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        Trigger t = new Trigger(in_type, in_name, in_creator, in_world);
        if (dataWriter.allTriggersMap.containsKey(t))
            return dataWriter.allTriggersMap.get(t);
        else {
            settings.notFound = 1;
            return t;
        }
    }

    public void createRedTrig(BlockDoorSettings settings, Player player, Block blockClicked) {
        if (settings.command == "redstonetrigger") {
            if (SpecialCaseCheck.isRedstonePowerSource(blockClicked)) {
                plugin.triggerhelper.createTrigger(settings, player, blockClicked);
            }
            else {
                player.sendMessage("Must click on a valid object. Command canceled!");
                BlockDoorSettings.clearSettings(player);
            }
        }
    }

    public Trigger findTrigger(TrigOverlaps to, String trigger_Type) {
        return findTrigger(trigger_Type, to.name, to.creator, to.world);
    }

    public Trigger findTrigger(String in_type, String in_name, String in_creator, String in_world) {
        return dataWriter.allTriggersMap.get(new Trigger(in_type, in_name, in_creator, in_world));
    }

    public Trigger findCoordinates(int in_x, int in_y, int in_z, String in_world) {
        for(Trigger t : dataWriter.allTriggersMap.values())
            if (t.trigger_x == in_x && t.trigger_y == in_y && t.trigger_z == in_z && t.trigger_world.equals(in_world)) {
                t.world = plugin.getWorld(in_world);
                return t;
            }
        return null;
    }

    public int findLink(Trigger t, Door d) {
        int index = -1;
        for(int j = 0; j < t.links.size(); j++)
            if (t.links.get(j).door.equals(d))
                return j;
        return index;
    }

    public void createTrigger(BlockDoorSettings settings, Player player, Block blockClicked) {
        int blockX = (int) blockClicked.getLocation().getX();
        int blockY = (int) blockClicked.getLocation().getY();
        int blockZ = (int) blockClicked.getLocation().getZ();
        Object obj = dataWriter.blockMap.get(blockClicked);
        if (obj == null) {
            Trigger newTrigger = saveTrigger(player, settings, blockX, blockY, blockZ);
            dataWriter.allTriggersMap.put(newTrigger, newTrigger);
            dataWriter.blockMap.put(blockClicked, newTrigger);
            dataWriter.saveDatabase(false);
            player.sendMessage(getProperTriggerName(newTrigger) + " '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created.");
        }
        else if (obj instanceof SingleStateDoor || obj instanceof TwoStateDoor || obj instanceof HDoor) {
            player.sendMessage(ChatColor.RED + getType(settings.friendlyCommand) + " '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' REJECTED, OVERLAP OF " + ((Door) obj).door_type.name() + " DOOR!");
            BlockDoorSettings.clearSettings(player);
            return;
        }
        else {
            Trigger t = null;

            if (obj instanceof Trigger && settings.friendlyCommand.equals("dtrig") && ((Trigger) obj).trigger_Type.equals("TRIGGER") && dataWriter.isOverlapTriggers()) {
                t = (Trigger) obj;
                Trigger newTrigger = saveTrigger(player, settings, blockX, blockY, blockZ);
                addOverlapsUpdateMap(newTrigger, t);
                dataWriter.blockMap.put(blockClicked, newTrigger);
                dataWriter.saveDatabase(false);
                player.sendMessage(getProperTriggerName(newTrigger) + " '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created.");
            }
            else if (obj instanceof Trigger && settings.friendlyCommand.equals("dmytrig") && ((Trigger) obj).trigger_Type.equals("MYTRIGGER") && dataWriter.isOverlapTriggers()) {
                t = (Trigger) obj;
                Trigger newTrigger = saveTrigger(player, settings, blockX, blockY, blockZ);
                addOverlapsUpdateMap(newTrigger, t);
                dataWriter.blockMap.put(blockClicked, newTrigger);
                dataWriter.saveDatabase(false);
                player.sendMessage(getProperTriggerName(newTrigger) + " '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created.");
            }
            else if (obj instanceof Trigger && settings.friendlyCommand.equals("dredtrig") && ((Trigger) obj).trigger_Type.equals("REDTRIG") && dataWriter.isOverlapRedstone()) {
                t = (Trigger) obj;
                Trigger newTrigger = saveTrigger(player, settings, blockX, blockY, blockZ);
                addOverlapsUpdateMap(newTrigger, t);
                dataWriter.blockMap.put(blockClicked, newTrigger);
                dataWriter.saveDatabase(false);
                player.sendMessage(getProperTriggerName(newTrigger) + " '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created.");
            }
            else {
                if (obj != null) {
                    t = (Trigger) obj;
                    player.sendMessage(ChatColor.RED + getProperTriggerName(t) + " '" + ChatColor.GOLD + settings.name + ChatColor.RED + "' REJECTED");
                    showOverlapMessage(t, player);
                }
                BlockDoorSettings.clearSettings(player);
                return;
            }
        }
    }

    private void addOverlapsUpdateMap(Trigger newTrigger, Trigger overlap) {
        TrigOverlaps t1 = new TrigOverlaps(newTrigger);
        for(TrigOverlaps ol : overlap.overlaps.values()) {
            ol.trig.overlaps.put(t1, t1);
            newTrigger.overlaps.put(ol, ol);
        }
        overlap.overlaps.put(t1, t1);
        TrigOverlaps t2 = new TrigOverlaps(overlap);
        newTrigger.overlaps.put(t2, t2);
        newTrigger.overlaps.remove(t1); // Clear self overlap.
        dataWriter.allTriggersMap.put(newTrigger, newTrigger);
    }

    private Trigger saveTrigger(Player player, BlockDoorSettings settings, int blockX, int blockY, int blockZ) {
        Trigger newTrigger = addTrigger(settings.name, player.getName(), player.getWorld().getName(), getType(settings.friendlyCommand), player);
        newTrigger.trigger_x = blockX;
        newTrigger.trigger_y = blockY;
        newTrigger.trigger_z = blockZ;
        newTrigger.trigger_world = player.getWorld().getName();
        newTrigger.coordsSet = true;
        BlockDoorSettings.clearSettings(player);
        return newTrigger;
    }

    public void showOverlapMessage(Trigger foundTrigger, Player player) {
        player.sendMessage("Overlap of " + getProperTriggerName(foundTrigger) + " [" + ChatColor.GREEN + foundTrigger.trigger_name + ChatColor.WHITE + "] Created by [" +
                ChatColor.GREEN + foundTrigger.trigger_creator + ChatColor.WHITE + "]");
        player.sendMessage("Located at [X= " + ChatColor.AQUA + foundTrigger.trigger_x + ChatColor.WHITE + " Y= " +
                ChatColor.AQUA + foundTrigger.trigger_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                foundTrigger.trigger_z + ChatColor.WHITE + "]");
    }

    private String getType(String friendlyName) {
        if (friendlyName.equals("dredtrig")) {
            return "REDTRIG";
        }
        else if (friendlyName.equals("dmytrig")) {
            return "MYTRIGGER";
        }
        else {
            return "TRIGGER";
        }
    }

    public String getProperTriggerName(Trigger foundTrigger) {
        if (foundTrigger.trigger_Type.equals("TRIGGER"))
            return "Block Trigger";
        if (foundTrigger.trigger_Type.equals("MYTRIGGER"))
            return "My Trigger";
        if (foundTrigger.trigger_Type.equals("REDTRIG"))
            return "Redstone Trigger";
        return "";
    }

    private boolean canBreak(Trigger trig, Player player) {
        return (plugin.playerHasPermission(player, "blockdoor.player.breaktriggers") && trig.trigger_creator.equals(player.getName())) || plugin.playerHasPermission(player, "blockdoor.admin.breaktriggers");
    }

    private void removeOverlapsUpdateBlockMap(Trigger trigger, Block block) {
        for(TrigOverlaps ol : trigger.overlaps.values()) {
            dataWriter.allTriggersMap.remove(ol.trig);
        }
        dataWriter.blockMap.remove(block);
        dataWriter.allTriggersMap.remove(trigger);
        dataWriter.saveDatabase(false);
    }

    public void updateOverlapsUpdateBlockMap(Trigger trigger, Block block) {
        Trigger t = null;
        for(TrigOverlaps ol1 : trigger.overlaps.values()) {
            for(TrigOverlaps ol2 : ol1.trig.overlaps.values()) {
                if (ol2.trig.equals(trigger)) {
                    ol1.trig.overlaps.remove(ol2);
                    break;
                }
            }
            t = ol1.trig;
        }
        if (t != null) {
            dataWriter.blockMap.put(block, t);
        }
        else {
            dataWriter.blockMap.remove(block);
        }
        dataWriter.allTriggersMap.remove(trigger);
        dataWriter.saveDatabase(false);
    }

    public void removeBlock(BlockBreakEvent event, BlockDoorSettings settings) {
        Block block = event.getBlock();
        Object obj = dataWriter.blockMap.get(block);
        if (obj != null && obj instanceof Trigger) {
            if (canBreak((Trigger) obj, event.getPlayer())) {
                Trigger trig = (Trigger) obj;
                removeOverlapsUpdateBlockMap(trig, block);
                event.getPlayer().sendMessage(getProperTriggerName(trig) + " " + ChatColor.GREEN + trig.trigger_name + ChatColor.WHITE + " by " + ChatColor.GREEN + trig.trigger_creator + ChatColor.WHITE + " removed.");
            }
            else {
                event.getPlayer().sendMessage(ChatColor.RED + "You dont have permissions to break this " + getProperTriggerName((Trigger) obj));
                event.setCancelled(true);
            }
        }
    }
}
