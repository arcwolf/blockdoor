package plugin.arcwolf.blockdoor.Triggers;


public class TrigOverlaps {

    public String creator, name, world;
    public Trigger trig;

    public TrigOverlaps(Trigger in_trig) {
        trig = in_trig;
        creator = trig.trigger_creator;
        name = trig.trigger_name;
        world = trig.trigger_world;
    }

    public TrigOverlaps(String in_string) {
        String[] split = in_string.split(",");
        if (split.length == 3) {
            creator = split[0];
            name = split[1];
            world = split[2];
        }
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((world == null) ? 0 : world.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        TrigOverlaps other = (TrigOverlaps) obj;
        if (creator == null) {
            if (other.creator != null) return false;
        }
        else if (!creator.equals(other.creator)) return false;
        if (name == null) {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (world == null) {
            if (other.world != null) return false;
        }
        else if (!world.equals(other.world)) return false;
        return true;
    }
}
