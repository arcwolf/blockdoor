package plugin.arcwolf.blockdoor;

import java.util.logging.Level;

import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;
import plugin.arcwolf.blockdoor.BlockDoor.Types;
import plugin.arcwolf.blockdoor.Doors.Door;

public class Link {

    public String link_name;
    public String link_creator;
    public String link_world;
    public Types linkType;
    public DoorTypes doorType;
    public Door door;

    public Link(Door door, String in_type, DoorTypes doorType) {
        link_name = door.door_name;
        link_creator = door.door_creator;
        link_world = door.door_world;

        if (in_type.equalsIgnoreCase("TOGGLE"))
            linkType = Types.TOGGLE;
        else if (in_type.equalsIgnoreCase("OPEN"))
            linkType = Types.OPEN;
        else if (in_type.equalsIgnoreCase("CLOSE"))
            linkType = Types.CLOSE;
        else
            linkType = Types.DISABLED;

        this.door = door;
        this.doorType = doorType;
    }

    public Link(String in_string, String in_world) {
        String[] split = in_string.split("\\s");
        if (split.length == 3) {
            link_name = split[0];
            link_creator = split[1];
            link_world = in_world;

            if (split[2].equalsIgnoreCase("TOGGLE"))
                linkType = Types.TOGGLE;
            else if (split[2].equalsIgnoreCase("OPEN"))
                linkType = Types.OPEN;
            else if (split[2].equalsIgnoreCase("CLOSE"))
                linkType = Types.CLOSE;
            else
                linkType = Types.DISABLED;

            doorType = DoorTypes.ONESTATE;
        }
        else if (split.length == 4) {
            link_name = split[0];
            link_creator = split[1];
            link_world = in_world;
            if (split[2].equalsIgnoreCase("TOGGLE"))
                linkType = Types.TOGGLE;
            else if (split[2].equalsIgnoreCase("OPEN"))
                linkType = Types.OPEN;
            else if (split[2].equalsIgnoreCase("CLOSE"))
                linkType = Types.CLOSE;
            else
                linkType = Types.DISABLED;

            if (split[3].equalsIgnoreCase("ONESTATE"))
                doorType = DoorTypes.ONESTATE;
            else if (split[3].equalsIgnoreCase("TWOSTATE"))
                doorType = DoorTypes.TWOSTATE;
            else if (split[3].equalsIgnoreCase("HYBRIDSTATE"))
                doorType = DoorTypes.HYBRIDSTATE;
            else
                doorType = DoorTypes.ONESTATE;
        }
        else
            link_creator = "FAILED";
    }

    public void process() {
        if (door != null) {
            switch (linkType) {
                case TOGGLE:
                    door.toggle();
                    break;
                case OPEN:
                    door.open();
                    break;
                case CLOSE:
                    door.close();
                    break;
                default:
                    BlockDoor.LOGGER.log(Level.INFO, "ERROR PARSING LINK!");
                    break;
            }
        }
        else {
            BlockDoor.LOGGER.info("BlockDoor: The " + doorType.name() + " ' " + link_name + " ' created by ' " + link_creator + " ' in world ' " + link_world + " ' attempted to toggle but has no record that it exists.");
        }
    }
}