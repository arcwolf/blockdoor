package plugin.arcwolf.blockdoor;

// Based on concepts from Ho0ber's blockdoor for Hey0

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import plugin.arcwolf.blockdoor.Doors.HDoorHelper;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoorHelper;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoorHelper;
import plugin.arcwolf.blockdoor.Triggers.TriggerHelper;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Utils.ItemCodesHelper;
import plugin.arcwolf.blockdoor.Zones.ZoneHelper;
import plugin.arcwolf.blockdoor.commands.CommandHandler;
import plugin.arcwolf.blockdoor.listeners.BlockDoorBlockListener;
import plugin.arcwolf.blockdoor.listeners.BlockDoorEntityListener;
import plugin.arcwolf.blockdoor.listeners.BlockDoorPlayerListener;
import plugin.arcwolf.blockdoor.listeners.ZoneMobWatcher;

// Permissions Imports
import ru.tehkode.permissions.bukkit.PermissionsEx;
import org.anjocaido.groupmanager.GroupManager;
import com.nijikokun.bukkit.Permissions.Permissions;
import net.milkbowl.vault.permission.Permission;

public class BlockDoor extends JavaPlugin {

    public static final Logger LOGGER = Logger.getLogger("Minecraft.BlockDoor");
    public static boolean LOADFAIL = false;
    private boolean dataFilesLoaded = false;
    private net.milkbowl.vault.permission.Permission vaultPerms;

    public static enum Locks {
        LOCKED, UNLOCKED
    }

    public static enum Types {
        TOGGLE, OPEN, CLOSE, DISABLED
    }

    public static enum DoorTypes {
        ONESTATE, TWOSTATE, HYBRIDSTATE
    }

    public Server server;
    private BukkitScheduler scheduler;
    public static BlockDoor plugin;

    private GroupManager groupManager;
    private Permissions permissionsPlugin;
    private PermissionsEx permissionsExPlugin;
    private de.bananaco.bpermissions.imp.Permissions bPermissions;

    public DataWriter datawriter;
    public DListerHelper dlisterhelper;
    public ItemCodesHelper itemcodeshelper;

    public CommandHandler commandhandler;

    public ZoneHelper zonehelper;

    public TriggerHelper triggerhelper;

    public HDoorHelper hdoorhelper;
    public TwoStateDoorHelper twostatedoorhelper;
    public SingleStateDoorHelper singlestatedoorhelper;
    private String pluginName;
    private String pluginVersion;

    @Override
    public void onEnable() {

        plugin = this;
        server = this.getServer();
        scheduler = server.getScheduler();

        datawriter = new DataWriter();
        dlisterhelper = new DListerHelper();
        itemcodeshelper = new ItemCodesHelper();

        commandhandler = new CommandHandler();

        zonehelper = new ZoneHelper();

        triggerhelper = new TriggerHelper();

        hdoorhelper = new HDoorHelper();
        twostatedoorhelper = new TwoStateDoorHelper();
        singlestatedoorhelper = new SingleStateDoorHelper();

        BlockDoorBlockListener blockListener = new BlockDoorBlockListener();
        BlockDoorPlayerListener playerListener = new BlockDoorPlayerListener();
        BlockDoorEntityListener entityListener = new BlockDoorEntityListener();

        PluginDescriptionFile pdfFile = getDescription();
        PluginManager pm = server.getPluginManager();

        pluginName = pdfFile.getName();
        pluginVersion = pdfFile.getVersion();

        // Used with regular triggers, zones and dinfo
        // Also, protection from commands being abused with teleportation 
        pm.registerEvents(playerListener, this);

        // For use with redstone triggers and twostate doors
        pm.registerEvents(blockListener, this);

        // Prevent endermen pickup/place, explosions and leaves decaying with twostate doors
        pm.registerEvents(entityListener, this);

        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

            @Override
            public void run() {
                datawriter.initFile();
                dataFilesLoaded = true;
            }
        }, 1);

        if (LOADFAIL) return;
        getPermissionsPlugin(); //Test for permissions        
        LOGGER.info(pluginName + " version " + pluginVersion + " is enabled!");

        // set up the thread for zone detection
        scheduler.scheduleSyncRepeatingTask(this, new ZoneMobWatcher(), 10, 10);
    }

    @Override
    public void onDisable() {
        scheduler.cancelTasks(this);
        if (!LOADFAIL) {
            LOGGER.info(pluginName + " saving database...");
            twostatedoorhelper.lock(); // Lock all doors in database.
            if (dataFilesLoaded) {
                int count = datawriter.saveDatabase(true); // Resave the database before quit.
                if (count != -1)
                    LOGGER.info(pluginName + " Save Completed. " + count + " object(s) saved.");
                else
                    LOGGER.log(Level.SEVERE, datawriter.getDoorsLoc() + " Does not exist! Save failed!");
            }
        }
        LOGGER.info(pluginName + " version " + pluginVersion + " is disabled!");
    }

    //return a world object from a world name string
    public World getWorld(String worldname) {
        return server.getWorld(worldname);
    }

    //test for a players permissions
    public boolean playerHasPermission(Player player, String command) {
        getPermissionsPlugin();
        if (vaultPerms != null) { return vaultPerms.has(player, command); }
        if (groupManager != null) {
            return groupManager.getWorldsHolder().getWorldPermissions(player).has(player, command);
        }
        else if (permissionsPlugin != null) {
            return (Permissions.Security.permission(player, command));
        }
        else if (permissionsExPlugin != null) {
            return (PermissionsEx.getPermissionManager().has(player, command));
        }
        else if (bPermissions != null) {
            return bPermissions.has(player, command);
        }
        else if (player.hasPermission(command)) {
            return true;
        }
        else if (datawriter.isError() && player.isOp()) {
            return true;
        }
        else {
            return false;
        }
    }

    // permissions plugin enabled test
    private void getPermissionsPlugin() {
        if (server.getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
            if (!datawriter.isPermissionsSet()) {
                LOGGER.info("Vault detected, " + pluginName + " permissions enabled...");
                datawriter.setPermissionsSet(true);
            }
            vaultPerms = rsp.getProvider();
        }
        else if (server.getPluginManager().getPlugin("GroupManager") != null) {
            Plugin p = server.getPluginManager().getPlugin("GroupManager");
            if (!datawriter.isPermissionsSet()) {
                LOGGER.info("GroupManager detected, " + pluginName + " permissions enabled...");
                datawriter.setPermissionsSet(true);
            }
            groupManager = (GroupManager) p;
        }
        else if (server.getPluginManager().getPlugin("Permissions") != null) {
            Plugin p = server.getPluginManager().getPlugin("Permissions");
            if (!datawriter.isPermissionsSet()) {
                LOGGER.info("Permissions detected, " + pluginName + " permissions enabled...");
                datawriter.setPermissionsSet(true);
            }
            permissionsPlugin = (Permissions) p;
        }
        else if (server.getPluginManager().getPlugin("PermissionsEx") != null) {
            Plugin p = server.getPluginManager().getPlugin("PermissionsEx");
            if (!datawriter.isPermissionsSet()) {
                LOGGER.info("PermissionsEx detected, " + pluginName + " permissions enabled...");
                datawriter.setPermissionsSet(true);
            }
            permissionsExPlugin = (PermissionsEx) p;
        }
        else if (server.getPluginManager().getPlugin("bPermissions") != null) {
            Plugin p = server.getPluginManager().getPlugin("bPermissions");
            if (!datawriter.isPermissionsSet()) {
                LOGGER.info("bPermissions detected, " + pluginName + " permissions enabled...");
                datawriter.setPermissionsSet(true);
            }
            bPermissions = (de.bananaco.bpermissions.imp.Permissions) p;
        }
        else if (server.getPluginManager().getPlugin("PermissionsBukkit") != null) {
            if (!datawriter.isPermissionsSet()) {
                LOGGER.info("Bukkit permissions detected, " + pluginName + " permissions enabled...");
                datawriter.setPermissionsSet(true);
            }
        }
        else {
            if (!datawriter.isError()) {
                LOGGER.info("Unknown Permissions detected, " + pluginName + " in Generic or OPs mode...");
                datawriter.setError(true);
            }
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] split) {
        Player player = null;
        if ((sender instanceof Player)) {
            player = (Player) sender;
            // inGame is the command handler for players sending commands from within the game
            if (!hasInvalidChar(cmd, split))
                return commandhandler.inGame(cmd, commandLabel, split, player);
            else {
                sender.sendMessage(ChatColor.RED + "Command Canceled");
                sender.sendMessage(ChatColor.GOLD + "You can not use the following characters in that command:");
                sender.sendMessage(ChatColor.RED + "  ,   :   $   |");
                return false;
            }
        }
        else {
            // inConsole is the command handler for users not in game. IE Console
            if (datawriter.isEnableConsoleCommands()) {
                if (!hasInvalidChar(cmd, split))
                    return commandhandler.inConsole(sender, cmd, split);
                else {
                    sender.sendMessage(ChatColor.RED + "Command Canceled");
                    sender.sendMessage(ChatColor.GOLD + "You can not use the following characters in that command:");
                    sender.sendMessage(ChatColor.RED + "  ,   :   $   |");
                    return false;
                }
            }
            else {
                sender.sendMessage("Console commands are disabled. Enable them in the config file");
            }
            return true;
        }
    }

    private boolean hasInvalidChar(Command cmd, String[] split) {
        String cmdName = cmd.getName().toLowerCase();
        if (!cmdName.equals("dfill") && !cmdName.equals("dempty") && !cmdName.equals("ddel")) {
            for(String test : split) {
                if (test.contains(",") || test.contains(":") || test.contains("$") || test.contains("|") || test.contains(" ")) return true;
            }
        }
        return false;
    }
}
