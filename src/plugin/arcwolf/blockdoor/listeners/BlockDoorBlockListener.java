package plugin.arcwolf.blockdoor.listeners;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.world.StructureGrowEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Triggers.TrigOverlaps;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Utils.SpecialCaseCheck;
import org.bukkit.event.block.BlockSpreadEvent;

public class BlockDoorBlockListener implements Listener {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public BlockDoorBlockListener() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
        if (event.isCancelled())
            return;
        if (event.isSticky()) {
            Object obj = dataWriter.blockMap.get(event.getRetractLocation().getBlock());
            if (obj != null && !(obj instanceof SingleStateDoor)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
        if (event.isCancelled())
            return;

        for(Block b : event.getBlocks()) {
            Object obj = dataWriter.blockMap.get(b);
            if (obj != null && !(obj instanceof SingleStateDoor)) {
                event.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockFade(BlockFadeEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj != null && !(obj instanceof SingleStateDoor))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onStructureGrow(StructureGrowEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getLocation().getBlock());
        if (obj != null && !(obj instanceof SingleStateDoor))
            event.setCancelled(true);
    }

    //Listens for redstone change events and toggles any redtrig's that are at the event site.
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockRedstoneChange(BlockRedstoneEvent event) {
        Block block = event.getBlock();
        Object obj = dataWriter.blockMap.get(block);
        if (obj != null && obj instanceof Trigger && ((Trigger) obj).trigger_Type.equals("REDTRIG")) {
            Trigger trig = (Trigger) obj;
            trig.world = block.getWorld();
            if (trig.world.getPlayers().size() != 0) {
                trig.processLinks();
                for(TrigOverlaps to : trig.overlaps.values()) {
                    to.trig.processLinks();
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockSpread(BlockSpreadEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj != null && !(obj instanceof SingleStateDoor))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockGrow(BlockGrowEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj != null && !(obj instanceof SingleStateDoor))
            event.setCancelled(true);
    }

    //Listens for physics events relating to twostate doors and hdoors. Prevents popping while door is being created.
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPhysics(BlockPhysicsEvent event) {
        if (event.isCancelled()) return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj == null) return;
        if (SpecialCaseCheck.isSpecialBlock(event.getBlock().getTypeId())) {
            if (obj instanceof TwoStateDoor) {
                TwoStateDoor tsd = (TwoStateDoor) dataWriter.blockMap.get(event.getBlock());
                if (plugin.datawriter.doorPhysics.containsKey(tsd)) {
                    event.setCancelled(true);
                }
            }
            else if (obj instanceof HDoor) {
                HDoor hd = (HDoor) obj;
                if (plugin.datawriter.doorPhysics.containsKey(hd)) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // temporary fix until I get around to incorporating snow forming in a twostate door.
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockForm(BlockFormEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj != null && !(obj instanceof SingleStateDoor))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBurn(BlockBurnEvent event) {
        if (event.isCancelled())
            return;
        Block block = event.getBlock();
        Object obj = dataWriter.blockMap.get(block);
        if (obj != null && !(obj instanceof SingleStateDoor)) {

            /*TODO Experimental.
             * ISSUE: When fire burns out naturally, that update is not saved. No Event Fired for Fire extinguish events.
             
            if (obj instanceof TwoStateDoor && !((TwoStateDoor) obj).isLocked()) {
                TwoStateDoor tsd = (TwoStateDoor) obj;
                CustomLocation cloc = new CustomLocation(block.getLocation());
                if (tsd.isOpen) {
                    DoorState ds = tsd.openState.get(cloc);
                    ds.blockID = 0;
                    ds.offset = 0;
                }
                else {
                    DoorState ds = tsd.closedState.get(cloc);
                    ds.blockID = 0;
                    ds.offset = 0;
                }
            }
            else {
                event.setCancelled(true);
            }*/
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockIgnite(BlockIgniteEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        Player player = event.getPlayer();
        if (obj != null && obj instanceof TwoStateDoor) {
            TwoStateDoor tsd = (TwoStateDoor) obj;
            if (tsd.isLocked()) {
                if (player != null) {
                    player.sendMessage("BlockDoor:" + ChatColor.RED + " Canceled, All doors in this area are locked.");
                }
                event.setCancelled(true);
            }
            else if (player != null && !player.equals(tsd.door_creator) && !plugin.playerHasPermission(player, "blockdoor.admin.twostate")) {
                player.sendMessage("Only " + tsd.door_creator + " may edit this door.");
                event.setCancelled(true);
            }
            else if (!dataWriter.isEnableSpecialBlocks()) {
                if (player != null) {
                    event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
                }
                event.setCancelled(true);
            }
            if (event.getPlayer() == null) {
                event.setCancelled(true);
            }
            if (event.isCancelled())
                return;
            else {
                if (!plugin.twostatedoorhelper.addBlock(event.getBlock(), event.getPlayer()))
                    return;
                else {
                    event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLeavesDecay(LeavesDecayEvent event) {
        if (event.isCancelled())
            return;
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj != null && !(obj instanceof SingleStateDoor))
            event.setCancelled(true);
    }

    // Check for water flow in a twostate door and save its state
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockFromTo(BlockFromToEvent event) {
        if (event.isCancelled())
            return;
        Object fromObj = dataWriter.blockMap.get(event.getBlock());
        Object toObj = dataWriter.blockMap.get(event.getToBlock());
        if ((fromObj != null || toObj != null) && (fromObj instanceof TwoStateDoor || toObj instanceof TwoStateDoor)) {
            event.setCancelled(true);
        }
    }

    //Listens for block place events and checks for Door states to update
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockPlace(final BlockPlaceEvent event) {
        if (event.isCancelled()) return;
        BlockDoorSettings settings = BlockDoorSettings.getSettings(event.getPlayer());
        Player player = event.getPlayer();
        Block block = event.getBlock();
        if (settings.command.equalsIgnoreCase("hdoor")) {
            Object obj = dataWriter.blockMap.get(block);
            TwoStateDoor tsd = null;
            SingleStateDoor ssd = null;
            if (obj != null) {
                if (obj instanceof TwoStateDoor)
                    tsd = (TwoStateDoor) obj;
                else if (obj instanceof SingleStateDoor)
                    ssd = (SingleStateDoor) obj;
                if (tsd != null) {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "Placement canceled, Overlap with " + ChatColor.WHITE + tsd.door_creator + "'s Two State Door (" + tsd.door_name + ChatColor.RED + ") not allowed!");
                    event.setCancelled(true);
                    return;
                }
                else if (ssd != null) {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "Placement canceled, Overlap with " + ChatColor.WHITE + ssd.door_creator + "'s Single State Door (" + ssd.door_name + ChatColor.RED + ") not allowed!");
                    event.setCancelled(true);
                    return;
                }
            }
            // new workaround for bukkit's flawed getData() method which I've been informed will never be fixed
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                public void run() {
                    if (event.isCancelled())
                                return;
                    if (!plugin.hdoorhelper.addBlock(event.getBlock(), event.getPlayer()))
                        return;
                    else {
                        event.setCancelled(true);
                    }
                }
            }, 1);
            return;
        }

        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj == null) return;
        if (obj instanceof TwoStateDoor) {
            TwoStateDoor tsd = (TwoStateDoor) obj;
            if (tsd.isLocked()) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " Canceled, All doors in this area are locked.");
                event.setCancelled(true);
                return;
            }
            else if (SpecialCaseCheck.isInvalidBlock(event.getBlock().getTypeId())) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
                event.setCancelled(true);
                return;
            }
            else if (!player.equals(tsd.door_creator) && !plugin.playerHasPermission(player, "blockdoor.admin.twostate")) {
                player.sendMessage("Only " + tsd.door_creator + " may edit this door.");
                event.setCancelled(true);
                return;
            }
            else if (!dataWriter.isEnableSpecialBlocks() && SpecialCaseCheck.isSpecialBlock(block)) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
                event.setCancelled(true);
                return;
            }

            // new workaround for bukkit's flawed getData() method which I've been informed will never be fixed
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                public void run() {
                    if (event.isCancelled()) return;
                    plugin.twostatedoorhelper.addBlock(event.getBlock(), event.getPlayer());
                }
            }, 1);
        }
    }

    //listens for block break events and removes record of blockdoor object that is at the event site.
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.isCancelled())
            return;
        BlockDoorSettings settings = BlockDoorSettings.getSettings(event.getPlayer());
        plugin.hdoorhelper.removeBlock(event, settings);
        plugin.twostatedoorhelper.removeBlock(event, settings);
        plugin.triggerhelper.removeBlock(event, settings);
    }
}