package plugin.arcwolf.blockdoor.listeners;

// A kind of custom listener for LivingEntities teleporting, dying or walking in and out of zones.

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Utils.ChunkCoords;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Zones.*;

public class ZoneMobWatcher implements Runnable {

    private BlockDoor plugin;
    private DataWriter dataWriter;
    private Map<Entity, List<Zone>> zoneOccupancyMap;

    //Map of all zoned entities in the server
    private Map<Integer, LivingEntity> eList = new HashMap<Integer, LivingEntity>();

    private volatile boolean shouldToggle = false;

    public ZoneMobWatcher() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
        zoneOccupancyMap = dataWriter.zoneOccupancyMap;
    }

    // Cycles through all the zones and tests for entity occupancy.
    public void run() {
        shouldToggle = false;
        boolean hasPermission;
        clearDespawns();
        LivingEntity e;
        for(Zone az : dataWriter.allZonesMap.values()) {
            //int chunkCount = 0;
            buildZoneChunkMap(az);
            for(Entry<ChunkCoords, ChunkCoords> chunkCoords : az.zoneChunk.entrySet()) {
                World world = plugin.getWorld(chunkCoords.getKey().getWorld());
                if (world == null) continue;
                int x = chunkCoords.getKey().getX();
                int z = chunkCoords.getKey().getZ();
                Chunk chunk = world.getChunkAt(x, z);

                // Fixes stale entity list in chunk map
                /* Not needed now, herre for Reference.
                 * if (!chunk.isLoaded()) {
                    az.setChunkUnloaded(true);
                    chunkCount++;
                    continue;
                }
                if (chunkCount != 0) {
                    continue;
                }
                else if (az.isChunkUnloaded()) {
                    az.setInitialized(false);
                    az.setChunkUnloaded(false);
                }
                */

                // ***********
                for(Entity ent : chunk.getEntities()) {
                    e = null;
                    if (ent instanceof LivingEntity)
                        e = (LivingEntity) ent;
                    else {
                        continue;
                    }
                    hasPermission = true;
                    // Checks for a players usezones permissions and sets hasPermissions to false if the player does not have permission to use zones.
                    if (e instanceof Player && !(plugin.playerHasPermission((Player) e, "blockdoor.usezones") || plugin.playerHasPermission((Player) e, "*")))
                        hasPermission = false;

                    // Check for player and permission
                    if (hasPermission && e instanceof Player) {
                        //Player detection zone
                        if (az.zone_Type.equals("ZONE")) {
                            testZone(az, e);
                        }

                        // Player detection for My Zones 
                        if (((Player) e).getName().equals(az.zone_creator) || (plugin.playerHasPermission((Player) e, "blockdoor.admin.myzone"))) {
                            if (az.zone_Type.equals("MYZONE")) {
                                testZone(az, e);
                            }
                        }
                    }
                    // Zone detection for selected entities
                    if (!plugin.datawriter.isUzoneDatabaseEr() && az.zone_Type.equals("UZONE") && uzoneDetect(e, az)) {
                        testZone(az, e);
                    }
                    else if (plugin.datawriter.isUzoneDatabaseEr() && az.zone_Type.equals("UZONE") && uzoneInteralDetect(e, az)) {
                        testZone(az, e);
                    }
                    // Zone detection for Passive mobs
                    if (!(e instanceof Player) && az.zone_Type.equals("PZONE") && (isPassiveMob(e))) {
                        testZone(az, e);
                    }

                    // Zone detection for Aggressive mobs
                    if (!(e instanceof Player) && az.zone_Type.equals("AZONE") && isAggressiveMob(e)) {
                        testZone(az, e);
                    }

                    // Zone detection for Aggressive and Passive mobs combined
                    if (!(e instanceof Player) && az.zone_Type.equals("MZONE")) {
                        testZone(az, e);
                    }

                    // Zone detection for Aggressive mobs, passive mobs and players
                    if (hasPermission && az.zone_Type.equals("EZONE")) {
                        testZone(az, e);
                    }
                }
            }
        }
    }

    //Gets a list of all chunks that a zone occupies and sets the zone's Chunk map to contain those chunks
    private void buildZoneChunkMap(Zone zone) {
        try {
            World world = plugin.getWorld(zone.zone_world);
            if (world == null) return;
            // Not needed now. Left here JIC reference
            /*if (world != null && world.getPlayers().size() == 0) { // Dumps chunk map if no players are in this world
                if (!zone.zoneChunk.isEmpty()) zone.zoneChunk.clear();
                zone.setInitialized(false);
                return;
            }*/

            Map<ChunkCoords, ChunkCoords> surroundingChunks;
            if (zone.world == null)
                zone.world = world;
            if (!zone.isInitialized()) {
                zone.zoneChunk.clear();
                for(int x = zone.zone_start_x; x <= zone.zone_end_x; x++) {
                    for(int y = zone.zone_start_y; y <= zone.zone_end_y; y++) {
                        for(int z = zone.zone_start_z; z <= zone.zone_end_z; z++) {
                            surroundingChunks = getSurroundingChunks(world.getChunkAt(new Location(world, x, y, z)));
                            surroundingChunks.keySet().removeAll(zone.zoneChunk.keySet());
                            zone.zoneChunk.putAll(surroundingChunks);
                        }
                    }
                }
                zone.setInitialized(true);
            }
        } catch (Exception e) {} // Do Nothing
        //************************
    }

    // surrounding 
    private Map<ChunkCoords, ChunkCoords> getSurroundingChunks(Chunk startChunk) {
        int chunkX = startChunk.getX();
        int chunkZ = startChunk.getZ();
        World world = startChunk.getWorld();
        Map<ChunkCoords, ChunkCoords> tmp = new HashMap<ChunkCoords, ChunkCoords>();
        Chunk tempChunk;
        for(int x = -1; x < 2; x++) {
            for(int z = -1; z < 2; z++) {
                tempChunk = world.getChunkAt(chunkX + x, chunkZ + z);
                ChunkCoords cc = new ChunkCoords(world.getName(), tempChunk.getX(), tempChunk.getZ());
                tmp.put(cc, cc);
            }
        }
        return tmp;
    }

    // Tests zones for entry, exit and deaths/despawns
    private void testZone(Zone z, LivingEntity e) {
        Location l = e.getLocation();
        World eWorld = l.getWorld();
        double eHealth = e.getHealth();
        boolean foundLiving = zoneOccupancyMap.containsKey(e) && zoneOccupancyMap.get(e).contains(z);
        z.world = plugin.getWorld(z.zone_world);
        if (z.coordsSet && z.isInZone(l) && eHealth != 0 && z.world == eWorld) {
            //ENTERING ZONE
            if (!eList.containsKey(e.getEntityId()))
                eList.put(e.getEntityId(), e);
            if (z.occupants == 0) {
                updateZoneMap(z, e);
                z.processLinks();
            }
            else if (z.occupants > 0) {
                if (!foundLiving) {
                    updateZoneMap(z, e);
                }
                shouldToggle = false;
            }
        }
        else if (z.coordsSet && z.isInZone(l) && (eHealth == 0 && foundLiving && z.world == eWorld)) {
            //DEATH WHILE IN ZONE
            eList.remove(e.getEntityId());
            if (z.occupants > 0) {
                if (zoneOccupancyMap.containsKey(e)) zoneOccupancyMap.get(e).remove(z);
                updateOccupants(z, e);
                shouldToggle = z.occupants == 0;
            }
            if (z.occupants == 0 && shouldToggle && z.world == eWorld) {
                updateOccupants(z, e);
                z.processLinks();
                shouldToggle = false;
            }
        }
        else if (z.coordsSet && !z.isInZone(l) && (foundLiving) && z.world == eWorld) {
            //LEAVING ZONE
            eList.remove(e.getEntityId());
            if (z.occupants > 0) {
                if (zoneOccupancyMap.containsKey(e)) zoneOccupancyMap.get(e).remove(z);
                updateOccupants(z, e);
                shouldToggle = z.occupants == 0;
            }
            if (z.occupants == 0 && shouldToggle) {
                updateOccupants(z, e);
                z.processLinks();
                shouldToggle = false;
            }
        }
    }

    private void updateZones(List<Zone> list) {
        for(Zone z : list) {
            if (z.occupants > 0) {
                z.occupants--;
            }
            else {
                z.occupants = 0;
            }
            if (z.occupants == 0) z.processLinks();
        }
    }

    private void updateOccupants(Zone z, Entity e) {
        List<Zone> zoneList = zoneOccupancyMap.get(e);
        if (z.occupants > 0)
            z.occupants--;
        else
            z.occupants = 0;
        if (zoneList == null || zoneList.size() == 0) {
            zoneOccupancyMap.remove(e);
        }
    }

    private void updateZoneMap(Zone z, LivingEntity e) {
        List<Zone> zoneList = zoneOccupancyMap.get(e);
        if (zoneList != null && !zoneList.contains(z)) {
            zoneList.add(z);
            z.occupants++;
        }
        else {
            zoneList = new ArrayList<Zone>();
            zoneList.add(z);
            z.occupants++;
            zoneOccupancyMap.put(e, zoneList);
        }
    }

    private void clearDespawns() {
        Iterator<Map.Entry<Integer, LivingEntity>> eListIter = eList.entrySet().iterator();
        while (eListIter.hasNext()) {
            Map.Entry<Integer, LivingEntity> entry = eListIter.next();
            if (entry.getValue().isDead()) {
                updateZones(zoneOccupancyMap.get(entry.getValue()));
                zoneOccupancyMap.remove(entry.getValue());
                eListIter.remove();
            }
        }
    }

    private boolean isAggressiveMob(LivingEntity e) {
        return e instanceof Monster || e instanceof Ghast || e instanceof Slime;
    }

    private boolean isPassiveMob(LivingEntity e) {
        return e instanceof Animals || e instanceof Snowman || e instanceof WaterMob || e instanceof NPC || e instanceof Bat;
    }

    private boolean uzoneDetect(LivingEntity e, Zone uz) {
        Map<String, List<String>> mobMap = plugin.datawriter.mobs;
        if (e instanceof Player) {
            Player p = (Player) e;
            if (p.getName().equals(uz.uzone_trigger))
                return true;
        }
        if (mobMap.get(e.getClass().getSimpleName()) != null && mobMap.get(e.getClass().getSimpleName()).contains(uz.uzone_trigger.trim().toLowerCase())) { return true; }
        return false;
    }

    private boolean uzoneInteralDetect(LivingEntity e, Zone uz) {
        if ((uz.uzone_trigger.equalsIgnoreCase("CraftPig") || uz.uzone_trigger.equalsIgnoreCase("Pig")) && e instanceof Pig)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftCow") || uz.uzone_trigger.equalsIgnoreCase("Cow")) && e instanceof Cow)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftChicken") || uz.uzone_trigger.equalsIgnoreCase("Chicken")) && e instanceof Chicken)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSheep") || uz.uzone_trigger.equalsIgnoreCase("Sheep")) && e instanceof Sheep)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSquid") || uz.uzone_trigger.equalsIgnoreCase("Squid")) && e instanceof Squid)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftWolf") || uz.uzone_trigger.equalsIgnoreCase("Wolf")) && e instanceof Wolf)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftZombie") || uz.uzone_trigger.equalsIgnoreCase("Zombie")) && e instanceof Zombie)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSpider") || uz.uzone_trigger.equalsIgnoreCase("Spider")) && e instanceof Spider)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSlime") || uz.uzone_trigger.equalsIgnoreCase("Slime")) && e instanceof Slime)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftCreeper") || uz.uzone_trigger.equalsIgnoreCase("Creeper")) && e instanceof Creeper)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftGiant") || uz.uzone_trigger.equalsIgnoreCase("Giant")) && e instanceof Giant)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftPigZombie") || uz.uzone_trigger.equalsIgnoreCase("PigZombie")) && e instanceof PigZombie)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftGhast") || uz.uzone_trigger.equalsIgnoreCase("Ghast")) && e instanceof Ghast)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSkeleton") || uz.uzone_trigger.equalsIgnoreCase("Skeleton")) && e instanceof Skeleton)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftEnderman") || uz.uzone_trigger.equalsIgnoreCase("Enderman")) && e instanceof Enderman)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftCaveSpider") || uz.uzone_trigger.equalsIgnoreCase("CaveSpider")) && e instanceof CaveSpider)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSilverfish") || uz.uzone_trigger.equalsIgnoreCase("Silverfish")) && e instanceof Silverfish)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftEnderDragon") || uz.uzone_trigger.equalsIgnoreCase("EnderDragon")) && e instanceof EnderDragon)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftMushroomCow") || uz.uzone_trigger.equalsIgnoreCase("Mooshroom") || uz.uzone_trigger.equalsIgnoreCase("MushroomCow")) && e instanceof MushroomCow)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftSnowman") || uz.uzone_trigger.equalsIgnoreCase("Snowman")) && e instanceof Snowman)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftBlaze") || uz.uzone_trigger.equalsIgnoreCase("Blaze")) && e instanceof Blaze)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftVillager") || uz.uzone_trigger.equalsIgnoreCase("Villager") || uz.uzone_trigger.equalsIgnoreCase("Testificate")) && e instanceof Villager)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftMagmaCube") || uz.uzone_trigger.equalsIgnoreCase("MagmaCube")) && e instanceof MagmaCube)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftOcelot") || uz.uzone_trigger.equalsIgnoreCase("Ocelot")) && e instanceof Ocelot)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftIronGolem") || uz.uzone_trigger.equalsIgnoreCase("IronGolem")) && e instanceof IronGolem)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftBat") || uz.uzone_trigger.equalsIgnoreCase("Bat")) && e instanceof Bat)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftWitch") || uz.uzone_trigger.equalsIgnoreCase("Witch")) && e instanceof Witch)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftWither") || uz.uzone_trigger.equalsIgnoreCase("Wither")) && e instanceof Wither)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftWitherSkull") || uz.uzone_trigger.equalsIgnoreCase("WitherSkull")) && e instanceof WitherSkull)
            return true;
        else if ((uz.uzone_trigger.equalsIgnoreCase("CraftHorse") || uz.uzone_trigger.equalsIgnoreCase("Horse")) && e instanceof Horse)
            return true;
        else if (e instanceof Player) {
            Player p = (Player) e;
            if (p.getName().equals(uz.uzone_trigger))
                return true;
        }
        return false;
    }
}
