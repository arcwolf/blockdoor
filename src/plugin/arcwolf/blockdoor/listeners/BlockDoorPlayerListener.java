package plugin.arcwolf.blockdoor.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.Locks;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class BlockDoorPlayerListener implements Listener {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public BlockDoorPlayerListener() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMove(PlayerMoveEvent event) {
        if (event.isCancelled())
            return;
        Player player = event.getPlayer();
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);

        if (settings.command == "info") {
            Block block = player.getLocation().getBlock();
            Zone zone = dataWriter.zoneBlockMap.get(block);
            TwoStateDoor tsd = null;
            SingleStateDoor ssd = null;
            Object obj = dataWriter.blockMap.get(block);
            if (zone == null && obj == null && settings.select == 1) {
                settings.select = 0;
            }
            else if (zone != null && settings.select == 0) {
                settings.select = 1;
                player.sendMessage("Found " + plugin.zonehelper.getFriendlyZoneName(zone) + " [" + ChatColor.GREEN + zone.zone_name + ChatColor.WHITE + "] Created by [" +
                            ChatColor.GREEN + zone.zone_creator + (zone.zone_Type.equals("UZONE") ? ChatColor.WHITE + "] Triggered by [" +
                                    ChatColor.GREEN + zone.uzone_trigger + ChatColor.WHITE + "]" : ChatColor.WHITE + "]"));
                player.sendMessage("Starting at [X= " + ChatColor.AQUA + zone.zone_start_x + ChatColor.WHITE + " Y= " +
                            ChatColor.AQUA + zone.zone_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                            zone.zone_start_z + ChatColor.WHITE + "]");
                player.sendMessage("Ending at [X= " + ChatColor.AQUA + zone.zone_end_x + ChatColor.WHITE + " Y= " +
                            ChatColor.AQUA + zone.zone_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                            zone.zone_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                            zone.zone_world + ChatColor.WHITE + "]");
            }
            else if (obj instanceof TwoStateDoor && settings.select == 0) {
                tsd = (TwoStateDoor) obj;
                settings.select = 1;
                player.sendMessage("Found TwoState Door [" + ChatColor.GREEN + tsd.door_name + ChatColor.WHITE + "] Created by [" +
                        ChatColor.GREEN + tsd.door_creator + ChatColor.WHITE + "]");
                player.sendMessage("Starting at [X= " + ChatColor.AQUA + tsd.door_start_x + ChatColor.WHITE + " Y= " +
                        ChatColor.AQUA + tsd.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                        tsd.door_start_z + ChatColor.WHITE + "]");
                player.sendMessage("Ending at [X= " + ChatColor.AQUA + tsd.door_end_x + ChatColor.WHITE + " Y= " +
                        ChatColor.AQUA + tsd.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                        tsd.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                        tsd.door_world + ChatColor.WHITE + "]");
                player.sendMessage(ChatColor.GREEN + tsd.door_name + ChatColor.WHITE + " is " + (tsd.locks.equals(Locks.UNLOCKED) ? ChatColor.GREEN : ChatColor.RED) + tsd.locks.name());
            }
            else if (obj instanceof SingleStateDoor && settings.select == 0){
                ssd = (SingleStateDoor) obj;
                settings.select = 1;
                player.sendMessage("Found SingleState Door [" + ChatColor.GREEN + ssd.door_name + ChatColor.WHITE + "] Created by [" +
                        ChatColor.GREEN + ssd.door_creator + ChatColor.WHITE + "]");
                player.sendMessage("Starting at [X= " + ChatColor.AQUA + ssd.door_start_x + ChatColor.WHITE + " Y= " +
                        ChatColor.AQUA + ssd.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                        ssd.door_start_z + ChatColor.WHITE + "]");
                player.sendMessage("Ending at [X= " + ChatColor.AQUA + ssd.door_end_x + ChatColor.WHITE + " Y= " +
                        ChatColor.AQUA + ssd.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                        ssd.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                        ssd.door_world + ChatColor.WHITE + "]");
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.isCancelled())
            return;
        Block blockClicked = event.getClickedBlock();
        Action playerClicked = event.getAction();
        Player player = event.getPlayer();

        if (blockClicked != null) {
            if (playerClicked == Action.LEFT_CLICK_BLOCK) {
                blockClick(player, blockClicked);
            }
            else if (playerClicked == Action.RIGHT_CLICK_BLOCK) {
                blockRightClicked(player, blockClicked);
                blockClick(player, blockClicked);
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (event.isCancelled())
            return;
        Player player = event.getPlayer();
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        if (settings.command != "") {
            plugin.commandhandler.cancelCommands(player);
            player.sendMessage(ChatColor.RED + "Teleported while BlockDoor command active. Command canceled!");
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.commandhandler.cancelCommands(player);
        BlockDoorSettings.clearSettings(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketFill(final PlayerBucketFillEvent event) {
        if (event.isCancelled())
            return;
        if (!dataWriter.blockMap.containsKey(event.getBlockClicked())) return;
        Player player = event.getPlayer();
        Object obj = dataWriter.blockMap.get(event.getBlockClicked());
        if (obj != null && obj instanceof TwoStateDoor) {
            TwoStateDoor tsd = (TwoStateDoor) obj;
            if (tsd.isLocked()) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " Canceled, All doors in this area are locked.");
                event.setCancelled(true);
                return;
            }
            else if (!player.equals(tsd.door_creator) && !plugin.playerHasPermission(player, "blockdoor.admin.twostate")) {
                player.sendMessage("Only " + tsd.door_creator + " may edit this door.");
                event.setCancelled(true);
                return;
            }
            else if (!dataWriter.isEnableSpecialBlocks()) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
                event.setCancelled(true);
                return;
            }
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                public void run() {
                    if (event.isCancelled()) return;
                    plugin.twostatedoorhelper.addBlock(event.getBlockClicked(), event.getPlayer());
                }
            }, 1);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerBucketEmpty(final PlayerBucketEmptyEvent event) {
        if (event.isCancelled())
            return;
        World world = event.getPlayer().getWorld();
        BlockFace face = event.getBlockFace();
        int modX = face.getModX();
        int modY = face.getModY();
        int modZ = face.getModZ();
        int x = event.getBlockClicked().getX() + modX;
        int y = event.getBlockClicked().getY() + modY;
        int z = event.getBlockClicked().getZ() + modZ;
        final Location loc = new Location(world, x, y, z);
        Player player = event.getPlayer();
        Object obj = dataWriter.blockMap.get(loc.getBlock());
        if (obj == null) return;
        if (obj instanceof TwoStateDoor) {
            TwoStateDoor tsd = (TwoStateDoor) obj;
            if (tsd.isLocked()) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " Canceled, All doors in this area are locked.");
                event.setCancelled(true);
                return;
            }
            else if (!player.equals(tsd.door_creator) && !plugin.playerHasPermission(player, "blockdoor.admin.twostate")) {
                player.sendMessage("Only " + tsd.door_creator + " may edit this door.");
                event.setCancelled(true);
                return;
            }
            else if (!dataWriter.isEnableSpecialBlocks()) {
                event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
                event.setCancelled(true);
                return;
            }
            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                public void run() {
                    if (event.isCancelled()) return;
                    plugin.twostatedoorhelper.addBlock(loc.getBlock(), event.getPlayer());
                }
            }, 1);
        }
        else if (obj instanceof Trigger && ((Trigger) obj).trigger_Type.equals("REDTRIG")) {
            event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " You cant place that here.");
            event.setCancelled(true);
        }
    }

    /*
     * Controls the different commands a player can enter when creating or
     * toggling blockdoor objects.
     */
    private void blockClick(Player player, Block blockClicked) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        if (settings.command == "") {
            Object obj = dataWriter.blockMap.get(blockClicked);
            if (obj != null) {
                if (obj instanceof Trigger && ((Trigger) obj).trigger_Type.equals("TRIGGER") && plugin.playerHasPermission(player, "blockdoor.usetriggers")) {
                    Trigger t = (Trigger) obj;
                    t.world = player.getWorld();
                    t.processLinks();
                }
                else if (obj instanceof Trigger && ((Trigger) obj).trigger_Type.equals("MYTRIGGER") && plugin.playerHasPermission(player, "blockdoor.usetriggers")) {
                    Trigger mt = (Trigger) obj;
                    if (mt.trigger_creator.equals(player.getName()) || plugin.playerHasPermission(player, "blockdoor.admin.mytrigger")) {
                        mt.world = player.getWorld();
                        mt.processLinks();
                    }
                    else {
                        player.sendMessage(ChatColor.YELLOW + "You toggle...but nothing happens");
                    }
                }
            }

        }
        else if (settings.command == "info") {
            infoSearch(player, blockClicked);
        }
        else if (settings.command == "savestate") {
            Object obj = dataWriter.blockMap.get(blockClicked);
            TwoStateDoor tsd = null;
            if (obj != null && obj instanceof TwoStateDoor) {
                tsd = (TwoStateDoor) obj;
            }
            if (tsd != null && tsd.locks.equals(Locks.UNLOCKED)) {
                if (tsd.door_creator.equals(player.getName()) || (plugin.playerHasPermission(player, "blockdoor.admin.save"))) {
                    plugin.twostatedoorhelper.saveTwoState(tsd, player);
                    infoSearch(player, blockClicked);
                    if (tsd.isOpen)
                        player.sendMessage(ChatColor.GREEN + "Open State has been saved");
                    else
                        player.sendMessage(ChatColor.GREEN + "Close State has been saved");
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "No unlocked door detected");
                settings.command = "";
            }
        }
        else if (settings.command == "door") {
            plugin.singlestatedoorhelper.createDoor(settings, player, blockClicked);
        }
        else if (settings.command == "twostate") {
            plugin.twostatedoorhelper.createDoor(settings, player, blockClicked);
        }
        else if (settings.command == "trigger" || settings.command == "mytrigger") {
            plugin.triggerhelper.createTrigger(settings, player, blockClicked);
        }
        else if (settings.command == "zone" || settings.command == "myzone" || settings.command == "mzone" || settings.command == "ezone" ||
                settings.command == "azone" || settings.command == "pzone" || settings.command == "uzone") {
            plugin.zonehelper.createZone(settings, player, blockClicked);
        }
    }

    /*
     * Handles the redstone right click for redstone trigger creations.
     */
    private void blockRightClicked(Player player, Block blockClicked) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        plugin.triggerhelper.createRedTrig(settings, player, blockClicked);
    }

    /*
     * Used as part of the dinfo command.
     * Searches for blockdoor objects that were clicked on and displays the
     * correct information about that object.
     */
    private void infoSearch(Player player, Block blockClicked) {
        int blockX = (int) blockClicked.getLocation().getX();
        int blockY = (int) blockClicked.getLocation().getY();
        int blockZ = (int) blockClicked.getLocation().getZ();
        Trigger t = null;
        TwoStateDoor tsd = null;
        HDoor hd = null;
        SingleStateDoor ss = null;
        Object obj = dataWriter.blockMap.get(blockClicked);
        if (obj == null) return;

        if (obj instanceof TwoStateDoor)
            tsd = (TwoStateDoor) obj;
        else if (obj instanceof HDoor)
            hd = (HDoor) obj;
        else if (obj instanceof SingleStateDoor)
            ss = (SingleStateDoor) obj;
        else if (obj instanceof Trigger)
            t = (Trigger) obj;

        if (t != null) {
            player.sendMessage("Found " + t.trigger_Type + " [" + ChatColor.GREEN + t.trigger_name + ChatColor.WHITE + "] Created by [" +
                    ChatColor.GREEN + t.trigger_creator + ChatColor.WHITE + "]");
            player.sendMessage("Located at [X= " + ChatColor.AQUA + t.trigger_x + ChatColor.WHITE + " Y= " +
                    ChatColor.AQUA + t.trigger_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                    t.trigger_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                    t.trigger_world + ChatColor.WHITE + "]");
        }
        else if (ss != null) {
            player.sendMessage("Found Door [" + ChatColor.GREEN + ss.door_name + ChatColor.WHITE + "] Created by [" +
                    ChatColor.GREEN + ss.door_creator + ChatColor.WHITE + "]");
            player.sendMessage("Starting at [X= " + ChatColor.AQUA + ss.door_start_x + ChatColor.WHITE + " Y= " +
                    ChatColor.AQUA + ss.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                    ss.door_start_z + ChatColor.WHITE + "]");
            player.sendMessage("Ending at [X= " + ChatColor.AQUA + ss.door_end_x + ChatColor.WHITE + " Y= " +
                    ChatColor.AQUA + ss.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                    ss.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                    ss.door_world + ChatColor.WHITE + "]");
        }
        else if (hd != null) {
            player.sendMessage("Found Hybrid Door [" + ChatColor.GREEN + hd.door_name + ChatColor.WHITE + "] Created by [" +
                    ChatColor.GREEN + hd.door_creator + ChatColor.WHITE + "]");
            player.sendMessage("Located at [X= " + ChatColor.AQUA + blockX + ChatColor.WHITE + " Y= " +
                    ChatColor.AQUA + blockY + ChatColor.WHITE + " Z= " + ChatColor.AQUA + blockZ + ChatColor.WHITE + "] in world ["
                    + ChatColor.AQUA + hd.door_world + ChatColor.WHITE + "]");
        }
        else if (tsd != null) {
            player.sendMessage("Found TwoState Door [" + ChatColor.GREEN + tsd.door_name + ChatColor.WHITE + "] Created by [" +
                    ChatColor.GREEN + tsd.door_creator + ChatColor.WHITE + "]");
            player.sendMessage("Starting at [X= " + ChatColor.AQUA + tsd.door_start_x + ChatColor.WHITE + " Y= " +
                    ChatColor.AQUA + tsd.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                    tsd.door_start_z + ChatColor.WHITE + "]");
            player.sendMessage("Ending at [X= " + ChatColor.AQUA + tsd.door_end_x + ChatColor.WHITE + " Y= " +
                    ChatColor.AQUA + tsd.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                    tsd.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                    tsd.door_world + ChatColor.WHITE + "]");
            player.sendMessage(ChatColor.GREEN + tsd.door_name + ChatColor.WHITE + " is " + (tsd.locks.equals(Locks.UNLOCKED) ? ChatColor.GREEN : ChatColor.RED) + tsd.locks.name());
        }
        else {
            player.sendMessage("No blockdoor object found at [x=" + ChatColor.GREEN + blockX + ChatColor.WHITE + " y=" +
                    ChatColor.GREEN + blockY + ChatColor.WHITE + " z=" + ChatColor.GREEN + blockZ + ChatColor.WHITE + "]");
        }
    }
}