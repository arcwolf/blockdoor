package plugin.arcwolf.blockdoor.listeners;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.Locks;
import plugin.arcwolf.blockdoor.Doors.DoorState;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Triggers.TrigOverlaps;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.CustomLocation;
import plugin.arcwolf.blockdoor.Utils.SpecialCaseCheck;

public class BlockDoorEntityListener implements Listener {

    private BlockDoor plugin;

    public BlockDoorEntityListener() {
        plugin = BlockDoor.plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityExplode(EntityExplodeEvent event) {
        if (event.isCancelled())
            return;
        boolean cancel = false;
        for(Block block : event.blockList()) {
            cancel = updateBlock(plugin.datawriter.blockMap.get(block), block.getBlockPower(), block.getData(), block.getLocation());
            if (cancel) {
                event.setCancelled(true);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        if (event.isCancelled())
            return;
        boolean cancel = false;
        Object obj = plugin.datawriter.blockMap.get(event.getBlock());
        if (event.getEntity() instanceof Sheep || event.getEntity() instanceof Monster ||
                SpecialCaseCheck.isSpecialBlock(event.getBlock().getTypeId()) || SpecialCaseCheck.isSpecialBlock(event.getTo().getId())) {
            cancel = updateBlock(obj, event.getTo().getId(), event.getData(), event.getBlock().getLocation());
        }
        else if (SpecialCaseCheck.isInvalidBlock(event.getBlock().getTypeId()) || SpecialCaseCheck.isInvalidBlock(event.getTo().getId())) {
            if (obj != null && obj instanceof TwoStateDoor)
                cancel = true;
        }
        if (cancel) event.setCancelled(true);
    }

    private boolean updateBlock(Object obj, int blockId, byte data, Location loc) {
        if (obj != null) {
            if (obj instanceof TwoStateDoor) {
                TwoStateDoor tsd = (TwoStateDoor) obj;
                CustomLocation cloc = new CustomLocation(loc);
                if (tsd.isLocked()) {
                    return true;
                }
                else {
                    if (tsd.isOpen) {
                        DoorState ds = tsd.openState.get(cloc);
                        if (ds == null) return false;
                        ds.blockID = blockId;
                        ds.offset = data;
                    }
                    else {
                        DoorState ds = tsd.closedState.get(cloc);
                        if (ds == null) return false;
                        ds.blockID = blockId;
                        ds.offset = data;
                    }
                }
            }
            else if (!(obj instanceof SingleStateDoor))
                return true; // Red Trigger, HDoor, etc.
        }
        return false;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onItemSpawn(final ItemSpawnEvent event) { // for item popping & twostate doors (JIC method) "Just in Case"
        if (event.isCancelled())
            return;

        final Block block = event.getLocation().getBlock();
        int blockId = block.getTypeId();
        int blockId2 = event.getEntity().getItemStack().getTypeId();
        if (blockId2 == 324)
            blockId = 64;
        else if (blockId2 == 330) blockId = 71;

        final Object obj = plugin.datawriter.blockMap.get(block);
        if (obj == null) return;

        if (SpecialCaseCheck.isSpecialBlock(blockId)) {
            int x = event.getLocation().getBlockX();
            int y = event.getLocation().getBlockY();
            int z = event.getLocation().getBlockZ();
            TwoStateDoor tsd = null;
            HDoor hd = null;
            if (obj instanceof TwoStateDoor)
                tsd = (TwoStateDoor) obj;
            else if (obj instanceof HDoor) hd = (HDoor) obj;
            if (tsd != null) {
                if (tsd.locks.equals(Locks.UNLOCKED)) {
                    CustomLocation cloc = new CustomLocation(tsd.door_world, x, y, z);
                    plugin.twostatedoorhelper.updateDoor(cloc, tsd, 0, (byte) 0);
                }
                else {
                    if (blockId2 == 324 || blockId2 == 330) {
                        CustomLocation cloc = new CustomLocation(tsd.door_world, x, y, z);
                        plugin.twostatedoorhelper.updateDoor(cloc, tsd, 0, (byte) 0);
                    }
                    event.setCancelled(true);
                }
            }
            if (hd != null) {
                if (hd.hDoorContents.get(new CustomLocation(block.getLocation())).blockID == blockId) {
                    event.setCancelled(true);
                }
            }
            if (obj instanceof Trigger && ((Trigger) obj).trigger_Type.equals("REDTRIG") && SpecialCaseCheck.isRedstonePowerSource(blockId)) {
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

                    public void run() {
                        if (event.isCancelled()) return;
                        if (!SpecialCaseCheck.isRedstonePowerSource(event.getLocation().getBlock())) {
                            Trigger trigger = (Trigger) obj;
                            Player player = plugin.getServer().getPlayer(trigger.trigger_creator);
                            if (player != null && player.isOnline()) {
                                player.sendMessage("BlockDoor: " + ChatColor.RED + " RedTrig " + ChatColor.AQUA + trigger.trigger_name + ChatColor.RED + " deleted because it popped.");
                                player.sendMessage("Location - " + ChatColor.GOLD + trigger.trigger_world + ": " + trigger.trigger_x + "," + trigger.trigger_y + "," + trigger.trigger_z);
                            }
                            for(TrigOverlaps ol : trigger.overlaps.values()) {
                                plugin.datawriter.allTriggersMap.remove(ol.trig);
                            }
                            plugin.datawriter.blockMap.remove(block);
                            plugin.datawriter.allTriggersMap.remove(trigger);
                            plugin.datawriter.saveDatabase(false);
                        }
                    }
                }, 1);
            }
        }
    }
}