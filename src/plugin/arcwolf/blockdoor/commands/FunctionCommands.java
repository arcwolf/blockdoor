package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class FunctionCommands {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public FunctionCommands() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public boolean commands(String[] split, Player player, BlockDoorSettings settings) {
        if (settings.friendlyCommand.equals("dinfo")) {
            if (split.length == 0) {
                player.sendMessage(ChatColor.AQUA + "Click a block for BlockDoor info ");
                player.sendMessage(ChatColor.AQUA + "Walk into a zone for BlockDoor info ");
                player.sendMessage("[/dcancel]" + ChatColor.AQUA + " to stop.");
                settings.command = "info";
                return true;
            }
            else {
                player.sendMessage(ChatColor.BLUE + "Usage: /dinfo");
                settings.command = "";
                return false;
            }
        }
        else if (settings.friendlyCommand.equals("dreload")) {
            player.sendMessage(ChatColor.BLUE + "One moment, Reloading BlockDoor datafiles...");
            dataWriter.setReload(true);
            dataWriter.reloadFile();
            player.sendMessage(ChatColor.BLUE + "Finished Reloading BlockDoor datafiles...");
            player.sendMessage(ChatColor.BLUE + "Check server log for errors if necessary.");
            return true;
        }
        else if (settings.friendlyCommand.equals("dsave")) {
            if (split.length > 0) {
                player.sendMessage(ChatColor.BLUE + "Usage: /dsave");
                player.sendMessage(ChatColor.BLUE + "Click on a twostate door");
                return false;
            }
            else if (split.length == 0) {
                player.sendMessage(ChatColor.GREEN + "Click on a twostate door to save.");
                settings.command = "savestate";
                return true;
            }
            /*
             * Disabled unless demand warrants its inclusion
             * else if(split.length == 1)
             * {
             * int index = TwoStateDoor.findDoor(split[1], player.getName(),
             * player.getWorld().getName());
             * if(index>=0)
             * {
             * BlockDoorSettings settings =
             * BlockDoorSettings.getSettings(player);
             * if(BlockDoor.twostate.get(index).creator.equals(player.getName())
             * || (BlockDoor.playerCanUseCommand(player,
             * "blockdoor.admin.save")))
             * {
             * TwoStateDoor.saveTwoState(index, player);
             * if(BlockDoor.twostate.get(index).isOpen)
             * player.sendMessage(ChatColor.GREEN+"Closed State has been saved");
             * else
             * player.sendMessage(ChatColor.GREEN+"Open State has been saved");
             * }
             * }
             * else
             * {
             * player.sendMessage(ChatColor.RED + "That door is not unlocked");
             * }
             * }
             */
        }
        else if (settings.friendlyCommand.equals("admin-dlist")) {
            boolean admin = true;
            plugin.dlisterhelper.commandList(player, split, admin);
            return true;
        }
        else if (settings.friendlyCommand.equals("player-dlist")) {
            boolean admin = false;
            plugin.dlisterhelper.commandList(player, split, admin);
            return true;
        }
        else if (settings.friendlyCommand.equals("uzone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), player.getName(), split[2], player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing uzone: " + ChatColor.GREEN + split[1] + " " + split[2]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The uzone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " triggered by: " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("uzone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), split[2], split[3], player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing uzone: " + ChatColor.GREEN + split[1] + " " + split[2]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The uzone - " + ChatColor.WHITE + split[1] + " owned by: " + split[2] + " triggered by: " + split[3] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("uzone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), split[2], split[3], split[4]);
            if (z != null) {
                player.sendMessage("Removing uzone: " + ChatColor.GREEN + split[1] + " by " + split[2] + " triggerd by " + split[3] + " in " + split[4]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The uzone - " + ChatColor.WHITE + split[1] + " owned by: " + split[2] + " triggered by: " + split[3] + " in " + split[4] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("link-player-ddel")) {
            split[1] = split[1].toLowerCase();
            String uzoneLink[] = split[1].split(":");
            String playerName = player.getName();
            String linkWorld = player.getWorld().getName();
            int linkIndex = -1;
            if (uzoneLink[0].equalsIgnoreCase("uzone") && uzoneLink.length > 1) {
                Zone uzone = plugin.zonehelper.findZone("UZONE", split[2], playerName, uzoneLink[1], linkWorld);
                linkIndex = plugin.zonehelper.findLink(uzone, getDoor(split[4], player, getDoorType(split[3])));
                if (linkIndex != -1) {
                    player.sendMessage("Removing link on Uzone: " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                    uzone.links.remove(linkIndex);
                    dataWriter.saveDatabase(false);
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "Could not find Uzone: " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                    return true;
                }
            }
            else if (uzoneLink[0].equalsIgnoreCase("uzone")) {
                player.sendMessage("Usage: /ddel link <triggerType(:uzoneTrigger)><triggerName><doorType><doorName>");
                return true;
            }
            else if (split[1].contains("zone")) {
                String zoneType = parseZoneCommand(split[1]);
                Zone z = plugin.zonehelper.findZone(zoneType, split[2], playerName, "", linkWorld);
                if (z != null) {
                    linkIndex = plugin.zonehelper.findLink(z, getDoor(split[4], player, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        player.sendMessage("Removing link on " + zoneType + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        z.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "Could not find " + zoneType + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                return true;
            }
            else if (split[1].contains("trig")) {
                Trigger t = getTrigger(split[1], split[2], playerName, linkWorld);
                if (t != null) {
                    linkIndex = plugin.triggerhelper.findLink(t, getDoor(split[4], player, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        player.sendMessage("Removing link on " + t.trigger_Type + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        t.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "Could not find " + t.trigger_Type + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "Could not find " + split[1] + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                    return true;
                }
            }
        }
        else if (settings.friendlyCommand.equals("link-admin-ddel")) {
            split[1] = split[1].toLowerCase();
            String uzoneLink[] = split[1].split(":");
            String playerName = split[5];
            String linkWorld = player.getWorld().getName();
            int linkIndex = -1;
            if (uzoneLink[0].equalsIgnoreCase("uzone") && uzoneLink.length > 1) {
                Zone uzone = plugin.zonehelper.findZone("UZONE", split[2], playerName, uzoneLink[1], linkWorld);
                linkIndex = plugin.zonehelper.findLink(uzone, getDoor(split[4], player, getDoorType(split[3])));
                if (linkIndex != -1) {
                    player.sendMessage("Removing link on Uzone: " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                    uzone.links.remove(linkIndex);
                    dataWriter.saveDatabase(false);
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "Could not find Uzone: " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                    return true;
                }
            }
            else if (uzoneLink[0].equalsIgnoreCase("uzone")) {
                player.sendMessage("Usage: /ddel link <triggerType(:uzoneTrigger)> <triggerName> <doorType> <doorName> <player>");
                return true;
            }
            else if (split[1].contains("zone")) {
                String zoneType = parseZoneCommand(split[1]);
                Zone z = plugin.zonehelper.findZone(zoneType, split[2], playerName, "", linkWorld);
                if (z != null) {
                    linkIndex = plugin.zonehelper.findLink(z, getDoor(split[4], player, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        player.sendMessage("Removing link on " + zoneType + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        z.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "Could not find " + zoneType + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                return true;
            }
            else if (split[1].contains("trig")) {
                Trigger t = getTrigger(split[1], split[2], playerName, linkWorld);
                if (t != null) {
                    linkIndex = plugin.triggerhelper.findLink(t, getDoor(split[4], player, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        player.sendMessage("Removing link on " + t.trigger_Type + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        t.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "Could not find " + t.trigger_Type + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "Could not find " + split[1] + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                    return true;
                }
            }
        }
        else if (settings.friendlyCommand.equals("link-world-ddel")) {
            split[1] = split[1].toLowerCase();
            String uzoneLink[] = split[1].split(":");
            String playerName = split[5];
            String linkWorld = split[6];
            int linkIndex = -1;
            if (uzoneLink[0].equalsIgnoreCase("uzone") && uzoneLink.length > 1) {
                Zone uzone = plugin.zonehelper.findZone("UZONE", split[2], playerName, uzoneLink[1], linkWorld);
                linkIndex = plugin.zonehelper.findLink(uzone, getDoor(split[4], player, getDoorType(split[3])));
                if (linkIndex != -1) {
                    player.sendMessage("Removing link on Uzone: " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                    uzone.links.remove(linkIndex);
                    dataWriter.saveDatabase(false);
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "Could not find Uzone: " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                    return true;
                }
            }
            else if (uzoneLink[0].equalsIgnoreCase("uzone")) {
                player.sendMessage("Usage: /ddel link <triggerType(:uzoneTrigger)> <triggerName> <doorType> <doorName> <player> <world>");
                return true;
            }
            else if (split[1].contains("zone")) {
                String zoneType = parseZoneCommand(split[1]);
                Zone z = plugin.zonehelper.findZone(zoneType, split[2], playerName, "", linkWorld);
                if (z != null) {
                    linkIndex = plugin.zonehelper.findLink(z, getDoor(split[4], player, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        player.sendMessage("Removing link on " + zoneType + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        z.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "Could not find " + zoneType + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                return true;
            }
            else if (split[1].contains("trig")) {
                Trigger t = getTrigger(split[1], split[2], playerName, linkWorld);
                if (t != null) {
                    linkIndex = plugin.triggerhelper.findLink(t, getDoor(split[4], player, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        player.sendMessage("Removing link on " + t.trigger_Type + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        t.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "Could not find " + t.trigger_Type + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "Could not find " + split[1] + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                    return true;
                }
            }
        }
        else if (settings.friendlyCommand.equals("door-player-ddel")) {
            Door d = plugin.singlestatedoorhelper.findDoor(split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (d != null) {
                player.sendMessage("Removing door: " + ChatColor.GREEN + split[1]);
                dataWriter.deleteLinks(d);
                plugin.singlestatedoorhelper.deleteOverlaps((SingleStateDoor) d);
                dataWriter.updateOldSSDBlockMap((SingleStateDoor) d);
                dataWriter.allSSDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("door-admin-ddel")) {
            Door d = plugin.singlestatedoorhelper.findDoor(split[1].toLowerCase(), split[2], player.getWorld().getName());
            if (d != null) {
                player.sendMessage("Removing door: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                dataWriter.deleteLinks(d);
                plugin.singlestatedoorhelper.deleteOverlaps((SingleStateDoor) d);
                dataWriter.updateOldSSDBlockMap((SingleStateDoor) d);
                dataWriter.allSSDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("door-world-ddel")) {
            Door d = plugin.singlestatedoorhelper.findDoor(split[1].toLowerCase(), split[2], split[3]);
            if (d != null) {
                player.sendMessage("Removing door: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                dataWriter.deleteLinks(d);
                plugin.singlestatedoorhelper.deleteOverlaps((SingleStateDoor) d);
                dataWriter.updateOldSSDBlockMap((SingleStateDoor) d);
                dataWriter.allSSDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("hdoor-player-ddel")) {
            Door d = plugin.hdoorhelper.findDoor(split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (d != null) {
                player.sendMessage("Removing hybrid door: " + ChatColor.GREEN + split[1]);
                dataWriter.updateOldHDBlockMap((HDoor) d);
                dataWriter.deleteLinks(d);
                plugin.hdoorhelper.deleteOverlaps((HDoor) d);
                dataWriter.allHDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The hybrid door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("hdoor-admin-ddel")) {
            Door d = plugin.hdoorhelper.findDoor(split[1].toLowerCase(), split[2], player.getWorld().getName());
            if (d != null) {
                player.sendMessage("Removing hybrid door: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                dataWriter.updateOldHDBlockMap((HDoor) d);
                dataWriter.deleteLinks(d);
                plugin.hdoorhelper.deleteOverlaps((HDoor) d);
                dataWriter.allHDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The hybrid door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("hdoor-world-ddel")) {
            Door d = plugin.hdoorhelper.findDoor(split[1].toLowerCase(), split[2], split[3]);
            if (d != null) {
                player.sendMessage("Removing hybrid door: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                dataWriter.updateOldHDBlockMap((HDoor) d);
                dataWriter.deleteLinks(d);
                plugin.hdoorhelper.deleteOverlaps((HDoor) d);
                dataWriter.allHDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The hybrid door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("twostate-player-ddel")) {
            Door d = plugin.twostatedoorhelper.findDoor(split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (d != null) {
                player.sendMessage("Removing door: " + ChatColor.GREEN + split[1]);
                dataWriter.deleteLinks(d);
                //plugin.twostatedoorhelper.deleteOverlaps(d.creator, d.doorName, d.door_world);
                plugin.twostatedoorhelper.deleteOverlaps((TwoStateDoor) d);
                dataWriter.updateOldTSDBlockMap((TwoStateDoor) d);
                dataWriter.allTSDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("twostate-admin-ddel")) {
            Door d = plugin.twostatedoorhelper.findDoor(split[1].toLowerCase(), split[2], player.getWorld().getName());
            if (d != null) {
                player.sendMessage("Removing door: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                dataWriter.deleteLinks(d);
                //plugin.twostatedoorhelper.deleteOverlaps(d.creator, d.doorName, d.door_world);
                plugin.twostatedoorhelper.deleteOverlaps((TwoStateDoor) d);
                dataWriter.updateOldTSDBlockMap((TwoStateDoor) d);
                dataWriter.allTSDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("twostate-world-ddel")) {
            Door d = plugin.twostatedoorhelper.findDoor(split[1].toLowerCase(), split[2], split[3]);
            if (d != null) {
                player.sendMessage("Removing door: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                dataWriter.deleteLinks(d);
                //plugin.twostatedoorhelper.deleteOverlaps(d.creator, d.doorName, d.door_world);
                plugin.twostatedoorhelper.deleteOverlaps((TwoStateDoor) d);
                dataWriter.updateOldTSDBlockMap((TwoStateDoor) d);
                dataWriter.allTSDoorsMap.remove(d);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("trigger-player-ddel")) {
            Trigger trigger = plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (trigger != null) {
                player.sendMessage("Removing trigger: " + ChatColor.GREEN + split[1]);
                World world = plugin.getWorld(trigger.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(trigger, world.getBlockAt(trigger.trigger_x, trigger.trigger_y, trigger.trigger_z));
                }
                dataWriter.allTriggersMap.remove(trigger);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("trigger-admin-ddel")) {
            Trigger trigger = plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), split[2], player.getWorld().getName());
            if (trigger != null) {
                player.sendMessage("Removing trigger: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                World world = plugin.getWorld(trigger.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(trigger, world.getBlockAt(trigger.trigger_x, trigger.trigger_y, trigger.trigger_z));
                }
                dataWriter.allTriggersMap.remove(trigger);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("trigger-world-ddel")) {
            Trigger trigger = plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), split[2], split[3]);
            if (trigger != null) {
                player.sendMessage("Removing trigger: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                World world = plugin.getWorld(trigger.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(trigger, world.getBlockAt(trigger.trigger_x, trigger.trigger_y, trigger.trigger_z));
                }
                dataWriter.allTriggersMap.remove(trigger);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("mytrigger-player-ddel")) {
            Trigger mytrig = plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mytrig != null) {
                player.sendMessage("Removing mytrigger: " + ChatColor.GREEN + split[1]);
                World world = plugin.getWorld(mytrig.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(mytrig, world.getBlockAt(mytrig.trigger_x, mytrig.trigger_y, mytrig.trigger_z));
                }
                dataWriter.allTriggersMap.remove(mytrig);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("mytrigger-admin-ddel")) {
            Trigger mytrig = plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), split[2], player.getWorld().getName());
            if (mytrig != null) {
                player.sendMessage("Removing mytrigger: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                World world = plugin.getWorld(mytrig.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(mytrig, world.getBlockAt(mytrig.trigger_x, mytrig.trigger_y, mytrig.trigger_z));
                }
                dataWriter.allTriggersMap.remove(mytrig);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The MyTrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("mytrigger-world-ddel")) {
            Trigger mytrig = plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), split[2], split[3]);
            if (mytrig != null) {
                player.sendMessage("Removing mytrigger: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                World world = plugin.getWorld(mytrig.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(mytrig, world.getBlockAt(mytrig.trigger_x, mytrig.trigger_y, mytrig.trigger_z));
                }
                dataWriter.allTriggersMap.remove(mytrig);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The MyTrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("redtrig-player-ddel")) {
            Trigger redtrig = plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (redtrig != null) {
                player.sendMessage("Removing redstone trigger: " + ChatColor.GREEN + split[1]);
                World world = plugin.getWorld(redtrig.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(redtrig, world.getBlockAt(redtrig.trigger_x, redtrig.trigger_y, redtrig.trigger_z));
                }
                dataWriter.allTriggersMap.remove(redtrig);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Redstone Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("redtrig-admin-ddel")) {
            Trigger redtrig = plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), split[2], player.getWorld().getName());
            if (redtrig != null) {
                player.sendMessage("Removing redstone trigger: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                World world = plugin.getWorld(redtrig.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(redtrig, world.getBlockAt(redtrig.trigger_x, redtrig.trigger_y, redtrig.trigger_z));
                }
                dataWriter.allTriggersMap.remove(redtrig);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Redstone Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("redtrig-world-ddel")) {
            Trigger redtrig = plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), split[2], split[3]);
            if (redtrig != null) {
                player.sendMessage("Removing redstone trigger: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                World world = plugin.getWorld(redtrig.trigger_world);
                if (world != null) {
                    plugin.triggerhelper.updateOverlapsUpdateBlockMap(redtrig, world.getBlockAt(redtrig.trigger_x, redtrig.trigger_y, redtrig.trigger_z));
                }
                dataWriter.allTriggersMap.remove(redtrig);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Redstone Trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("zone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Player zone: " + ChatColor.GREEN + split[1]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Player Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("zone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), split[2], "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Player zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Player Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("zone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), split[2], "", split[3]);
            if (z != null) {
                player.sendMessage("Removing Player zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Player Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("myzone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing myzone: " + ChatColor.GREEN + split[1]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The MyZone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("myzone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), split[2], "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing ,myzone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The MyZone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("myzone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), "", split[2], split[3]);
            if (z != null) {
                player.sendMessage("Removing myzone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The MyZone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("mzone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Mob zone: " + ChatColor.GREEN + split[1]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("mzone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), split[2], "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Mob zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("mzone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), split[2], "", split[3]);
            if (z != null) {
                player.sendMessage("Removing Mob zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("ezone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Living Entity zone: " + ChatColor.GREEN + split[1]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entity Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("ezone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), split[2], "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Living Entity zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entity Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("ezone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), "", split[2], split[3]);
            if (z != null) {
                player.sendMessage("Removing Living Entity zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entity Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("azone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Aggressive Mob zone: " + ChatColor.GREEN + split[1]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("azone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), split[2], "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Aggressive Mob zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("azone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), "", split[2], split[3]);
            if (z != null) {
                player.sendMessage("Removing Aggressive Mob zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("pzone-player-ddel")) {
            Zone z = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Passive Mob zone: " + ChatColor.GREEN + split[1]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("pzone-admin-ddel")) {
            Zone z = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), split[2], "", player.getWorld().getName());
            if (z != null) {
                player.sendMessage("Removing Passive Mob zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + player.getWorld().getName());
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + player.getWorld().getName() + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("pzone-world-ddel")) {
            Zone z = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), "", split[2], split[3]);
            if (z != null) {
                player.sendMessage("Removing Passive Mob zone: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " by " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " in " + ChatColor.GREEN + split[3]);
                plugin.zonehelper.deleteOverlaps(z);
                dataWriter.updateOldZoneBlockMap(z);
                dataWriter.allZonesMap.remove(z);
                dataWriter.saveDatabase(false);
                return true;
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob Zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                return true;
            }
        }
        return false;
    }

    private Door getDoor(String doorName, Player player, String doorType) {
        Door d = null;
        String dt = getDoorType(doorType);
        if (dt == null) return null;
        if (dt.equals("ONESTATE")) {
            d = plugin.singlestatedoorhelper.findDoor(doorName, player.getName(), player.getWorld().getName());
        }
        else if (dt.equals("TWOSTATE")) {
            d = plugin.twostatedoorhelper.findDoor(doorName, player.getName(), player.getWorld().getName());
        }
        else if (dt.equals("HYBRIDSTATE")) {
            d = plugin.hdoorhelper.findDoor(doorName, player.getName(), player.getWorld().getName());
        }
        return d;
    }

    private String parseZoneCommand(String zoneType) {
        zoneType = zoneType.toLowerCase();
        if (zoneType.startsWith("a"))
            return "AZONE";
        else if (zoneType.startsWith("e"))
            return "EZONE";
        else if (zoneType.startsWith("p"))
            return "PZONE";
        else if (zoneType.contains("myz"))
            return "MYZONE";
        else if (zoneType.contains("mz"))
            return "MZONE";
        else if (zoneType.contains("u"))
            return "UZONE";
        else
            return "ZONE";
    }

    private Trigger getTrigger(String trigType, String trigName, String player, String world) {
        trigType = trigType.toLowerCase();
        Trigger t = null;
        if (trigType.contains("red"))
            t = plugin.triggerhelper.findTrigger("REDTRIG", trigName, player, world);
        else if (trigType.contains("my"))
            t = plugin.triggerhelper.findTrigger("MYTRIGGER", trigName, player, world);
        else
            t = plugin.triggerhelper.findTrigger("TRIGGER", trigName, player, world);
        return t;
    }

    private String getDoorType(String doorType) {
        if (doorType == null || doorType.equals("")) return null;
        doorType = doorType.toLowerCase();
        if (doorType.startsWith("s") || doorType.startsWith("o") || doorType.startsWith("d"))
            return "ONESTATE";
        else if (doorType.startsWith("t"))
            return "TWOSTATE";
        else if (doorType.startsWith("h"))
            return "HYBRIDSTATE";
        else
            return null;
    }
}