package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;

public class CommandHandler {

    private BlockDoor plugin;
    private DataWriter dataWriter;
    private DoorCommands doorcommands;
    private LinkCommands linkcommands;
    private ConsoleCommands consolecommands;
    private FunctionCommands functioncommands;

    public CommandHandler() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
        doorcommands = new DoorCommands();
        linkcommands = new LinkCommands();
        consolecommands = new ConsoleCommands();
        functioncommands = new FunctionCommands();
    }

    //commands sent from within the game environment
    public boolean inGame(Command cmd, String commandLabel, String[] split, Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        String cmdname = cmd.getName().toLowerCase();
        if (cmdname.equals("dinfo") && (plugin.playerHasPermission(player, "blockdoor.info")) && BlockDoorSettings.getSettings(player).command.equalsIgnoreCase("")) {
            settings.friendlyCommand = "dinfo";
            return functioncommands.commands(split, player, settings);
        }
        else if (cmdname.equals("dhdoor") && (split.length > 1 && split.length < 3) && split[1].equalsIgnoreCase("done")) {
            settings.friendlyCommand = "dhdoor";
            return doorcommands.commands(split, player, settings);
        }
        else if (BlockDoorSettings.getSettings(player).command.equalsIgnoreCase("")) {
            if ((cmdname.equals("dreload")) && (plugin.playerHasPermission(player, "blockdoor.reload"))) {
                settings.friendlyCommand = "dreload";
                return functioncommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("ddoor")) && (plugin.playerHasPermission(player, "blockdoor.door"))) {
                settings.friendlyCommand = "ddoor";
                return doorcommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("dhdoor")) && (plugin.playerHasPermission(player, "blockdoor.hdoor"))) {
                settings.friendlyCommand = "dhdoor";
                return doorcommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("dsave")) && (plugin.playerHasPermission(player, "blockdoor.player.save") || plugin.playerHasPermission(player, "blockdoor.admin.save"))) {
                settings.friendlyCommand = "dsave";
                return functioncommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("dtwostate")) && (plugin.playerHasPermission(player, "blockdoor.player.twostate") || plugin.playerHasPermission(player, "blockdoor.admin.twostate"))) {
                settings.friendlyCommand = "dtwostate";
                return doorcommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("dtrig")) && (plugin.playerHasPermission(player, "blockdoor.trigger"))) {
                settings.friendlyCommand = "dtrig";
                return TriggerCommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("dmytrig")) && (plugin.playerHasPermission(player, "blockdoor.mytrigger"))) {
                settings.friendlyCommand = "dmytrig";
                return TriggerCommands.commands(split, player, settings);
            }
            else if ((cmdname.equals("dredtrig")) && (plugin.playerHasPermission(player, "blockdoor.redstonetrigger"))) {
                settings.friendlyCommand = "dredtrig";
                return TriggerCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dtoggle")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.toggle")) {
                    settings.friendlyCommand = "admin-dtoggle";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.toggle") || plugin.playerHasPermission(player, "blockdoor.admin.toggle"))) {
                    settings.friendlyCommand = "player-dtoggle";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.toggle"))) {
                    player.sendMessage("Usage: /dtoggle <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.toggle"))) {
                    player.sendMessage("Usage: /dtoggle <doorname>");
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dtoggle2")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.toggle2")) {
                    settings.friendlyCommand = "admin-dtoggle2";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.toggle2") || plugin.playerHasPermission(player, "blockdoor.admin.toggle2"))) {
                    settings.friendlyCommand = "player-dtoggle2";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.toggle2"))) {
                    player.sendMessage("Usage: /dtoggle2 <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.toggle2"))) {
                    player.sendMessage("Usage: /dtoggle2 <doorname>");
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dtoggle3")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.toggle3")) {
                    settings.friendlyCommand = "admin-dtoggle3";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.toggle3") || plugin.playerHasPermission(player, "blockdoor.admin.toggle3"))) {
                    settings.friendlyCommand = "player-dtoggle3";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.toggle3"))) {
                    player.sendMessage("Usage: /dtoggle3 <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.toggle3"))) {
                    player.sendMessage("Usage: /dtoggle3 <doorname>");
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dopen")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.open")) {
                    settings.friendlyCommand = "admin-dopen";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.open") || plugin.playerHasPermission(player, "blockdoor.admin.open"))) {
                    settings.friendlyCommand = "player-dopen";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.open"))) {
                    player.sendMessage("Usage: /dopen <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.open"))) {
                    player.sendMessage("Usage: /dopen <doorname>");
                    return true;
                }
                else {
                    player.sendMessage(ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dclose")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.close")) {
                    settings.friendlyCommand = "admin-dclose";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.close") || plugin.playerHasPermission(player, "blockdoor.admin.close"))) {
                    settings.friendlyCommand = "player-dclose";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.close"))) {
                    player.sendMessage("Usage: /dclose <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.close"))) {
                    player.sendMessage("Usage: /dclose <doorname>");
                    return true;
                }
                else {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dopen2")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.open2")) {
                    settings.friendlyCommand = "admin-dopen2";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.open2") || plugin.playerHasPermission(player, "blockdoor.admin.open2"))) {
                    settings.friendlyCommand = "player-dopen2";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.open2"))) {
                    player.sendMessage("Usage: /dopen2 <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.open2"))) {
                    player.sendMessage("Usage: /dopen2 <doorname>");
                    return true;
                }
                else {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dclose2")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.close2")) {
                    settings.friendlyCommand = "admin-dclose2";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.close2") || plugin.playerHasPermission(player, "blockdoor.admin.close2"))) {
                    settings.friendlyCommand = "player-dclose2";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.close2"))) {
                    player.sendMessage("Usage: /dclose2 <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.close2"))) {
                    player.sendMessage("Usage: /dclose2 <doorname>");
                    return true;
                }
                else {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dopen3")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.open3")) {
                    settings.friendlyCommand = "admin-dopen3";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.open3") || plugin.playerHasPermission(player, "blockdoor.admin.open3"))) {
                    settings.friendlyCommand = "player-dopen3";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.open3"))) {
                    player.sendMessage("Usage: /dopen3 <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.open3"))) {
                    player.sendMessage("Usage: /dopen3 <doorname>");
                    return true;
                }
                else {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dclose3")) {
                if (split.length == 2 && plugin.playerHasPermission(player, "blockdoor.admin.close3")) {
                    settings.friendlyCommand = "admin-dclose3";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length == 1 && (plugin.playerHasPermission(player, "blockdoor.player.close3") || plugin.playerHasPermission(player, "blockdoor.admin.close3"))) {
                    settings.friendlyCommand = "player-dclose3";
                    return doorcommands.commands(split, player, settings);
                }
                else if (split.length > 2 && (plugin.playerHasPermission(player, "blockdoor.admin.close3"))) {
                    player.sendMessage("Usage: /dclose3 <doorname> <playerName>");
                    return true;
                }
                else if (split.length != 1 && (plugin.playerHasPermission(player, "blockdoor.player.close3"))) {
                    player.sendMessage("Usage: /dclose3 <doorname>");
                    return true;
                }
                else {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("dlink")) {
                if (split.length > 0) {
                    if ((split.length > 5 || split.length < 4) && !split[0].equalsIgnoreCase("uzone")) {
                        player.sendMessage(ChatColor.BLUE + "Usage:");
                        player.sendMessage(ChatColor.GREEN + "/dlink <triggerType><trigname><doorname><type><doorType>");
                        player.sendMessage(ChatColor.BLUE + "Valid Trigger types:");
                        player.sendMessage(ChatColor.GREEN + "trigger, redtrig, mytrigger, ezone,");
                        player.sendMessage(ChatColor.GREEN + "azone, mzone, myzone, pzone, uzone");
                        player.sendMessage(ChatColor.BLUE + "Valid types:");
                        player.sendMessage(ChatColor.GREEN + "open, close, toggle");
                        player.sendMessage(ChatColor.BLUE + "Valid Door Types:");
                        player.sendMessage(ChatColor.GREEN + "1 = One State , 2 = Two State, 3 = Hybrid State");
                        return true;
                    }
                    else if ((split.length > 6 || split.length < 5) && split[0].equalsIgnoreCase("uzone")) {
                        player.sendMessage(ChatColor.BLUE + "Usage:");
                        player.sendMessage(ChatColor.GREEN + "/dlink <triggerType><trigname><doorname><type><doorType>");
                        player.sendMessage(ChatColor.BLUE + "Valid Trigger types:");
                        player.sendMessage(ChatColor.GREEN + "trigger, redtrig, mytrigger, ezone,");
                        player.sendMessage(ChatColor.GREEN + "azone, mzone, myzone, pzone, uzone");
                        player.sendMessage(ChatColor.BLUE + "Valid types:");
                        player.sendMessage(ChatColor.GREEN + "open, close, toggle");
                        player.sendMessage(ChatColor.BLUE + "Valid Door Types:");
                        player.sendMessage(ChatColor.GREEN + "1 = One State , 2 = Two State, 3 = Hybrid State");
                        return true;
                    }
                    else if (!(split[0].equalsIgnoreCase("trigger") || split[0].equalsIgnoreCase("pzone") || split[0].equalsIgnoreCase("azone") ||
                            split[0].equalsIgnoreCase("ezone") || split[0].equalsIgnoreCase("mzone") || split[0].equalsIgnoreCase("myzone") ||
                            split[0].equalsIgnoreCase("zone") || split[0].equalsIgnoreCase("trig") || split[0].equalsIgnoreCase("redtrig") ||
                            split[0].equalsIgnoreCase("redtrigger") || split[0].equalsIgnoreCase("mytrigger") || split[0].equalsIgnoreCase("mytrig") || split[0].equalsIgnoreCase("uzone"))) {

                        player.sendMessage(ChatColor.BLUE + "Usage:");
                        player.sendMessage(ChatColor.GREEN + "/dlink <triggerType><trigname><doorname><type><doorType>");
                        player.sendMessage(ChatColor.BLUE + "Valid Trigger types:");
                        player.sendMessage(ChatColor.GREEN + "trigger, redtrig, mytrigger, ezone,");
                        player.sendMessage(ChatColor.GREEN + "azone, mzone, myzone, pzone, uzone");
                        player.sendMessage(ChatColor.BLUE + "Valid types:");
                        player.sendMessage(ChatColor.GREEN + "open, close, toggle");
                        player.sendMessage(ChatColor.BLUE + "Valid Door Types:");
                        player.sendMessage(ChatColor.GREEN + "1 = One State , 2 = Two State, 3 = Hybrid State");
                        return true;
                    }
                    else if (split[0].equalsIgnoreCase("uzone") && plugin.playerHasPermission(player, "blockdoor.link.uzone")) {
                        if (split.length == 6) {
                            settings.friendlyCommand = "stateSelected-Uzone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 5) {
                            settings.friendlyCommand = "singleState-Uzone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if ((split.length < 5 || split.length > 6)) {
                            player.sendMessage("Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                            player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                            return true;
                        }
                    }
                    else if ((split[0].equalsIgnoreCase("trigger") || split[0].equalsIgnoreCase("trig")) && plugin.playerHasPermission(player, "blockdoor.link.trigger")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-Trigger";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-Trigger";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if ((split[0].equalsIgnoreCase("mytrigger") || split[0].equalsIgnoreCase("mytrig")) && plugin.playerHasPermission(player, "blockdoor.link.mytrigger")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-MyTrigger";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-MyTrigger";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("redtrig") && plugin.playerHasPermission(player, "blockdoor.link.redtrig")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-redTrig";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-redTrig";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("zone") && plugin.playerHasPermission(player, "blockdoor.link.zone")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-zone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-zone";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("myzone") && plugin.playerHasPermission(player, "blockdoor.link.myzone")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-myzone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-myzone";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("azone") && plugin.playerHasPermission(player, "blockdoor.link.azone")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-azone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-azone";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("ezone") && plugin.playerHasPermission(player, "blockdoor.link.ezone")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-ezone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-ezone";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("mzone") && plugin.playerHasPermission(player, "blockdoor.link.mzone")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-mzone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-mzone";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else if (split[0].equalsIgnoreCase("pzone") && plugin.playerHasPermission(player, "blockdoor.link.pzone")) {
                        if (split.length == 5) {
                            settings.friendlyCommand = "stateSelected-pzone";
                            return linkcommands.commands(split, player, settings);
                        }
                        else if (split.length == 4) {
                            settings.friendlyCommand = "singleState-pzone";
                            return linkcommands.commands(split, player, settings);
                        }
                    }
                    else {
                        player.sendMessage("BlockDoor: " + ChatColor.RED + "Invalid command or you dont have permission to use " + ChatColor.WHITE + split[0] + ChatColor.RED + " for command: " + ChatColor.WHITE + cmdname);
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.BLUE + "Usage:");
                    player.sendMessage(ChatColor.GREEN + "/dlink <triggerType><trigname><doorname><type><doorType>");
                    player.sendMessage(ChatColor.BLUE + "Valid Trigger types:");
                    player.sendMessage(ChatColor.GREEN + "trigger, redtrig, mytrigger, ezone,");
                    player.sendMessage(ChatColor.GREEN + "azone, mzone, myzone, pzone, uzone");
                    player.sendMessage(ChatColor.BLUE + "Valid types:");
                    player.sendMessage(ChatColor.GREEN + "open, close, toggle");
                    player.sendMessage(ChatColor.BLUE + "Valid Door Types:");
                    player.sendMessage(ChatColor.GREEN + "1 = One State , 2 = Two State, 3 = Hybrid State");
                    return true;
                }
            }
            else if (cmdname.equals("dfill") && (plugin.playerHasPermission(player, "blockdoor.fill"))) {
                settings.friendlyCommand = "dfill";
                return doorcommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dempty") && (plugin.playerHasPermission(player, "blockdoor.empty"))) {
                settings.friendlyCommand = "dempty";
                return doorcommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dlist")) {
                if (plugin.playerHasPermission(player, "blockdoor.admin.list")) {
                    settings.friendlyCommand = "admin-dlist";
                    return functioncommands.commands(split, player, settings);
                }
                else if (plugin.playerHasPermission(player, "blockdoor.player.list")) {
                    settings.friendlyCommand = "player-dlist";
                    return functioncommands.commands(split, player, settings);
                }
                else {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
            }
            else if (cmdname.equals("ddel")) {
                if (!plugin.playerHasPermission(player, "blockdoor.player.delete")) {
                    player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
                else if (split.length < 2) {
                    if (plugin.playerHasPermission(player, "blockdoor.admin.delete")) {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                    else {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                }
                else if (split[0].equalsIgnoreCase("uzone") && (plugin.playerHasPermission(player, "blockdoor.player.delete") || plugin.playerHasPermission(player, "blockdoor.admin.delete"))) {
                    if (split.length == 3) {
                        settings.friendlyCommand = "uzone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4 && plugin.playerHasPermission(player, "blockdoor.admin.delete")) {
                        settings.friendlyCommand = "uzone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 5 && plugin.playerHasPermission(player, "blockdoor.admin.delete")) {
                        settings.friendlyCommand = "uzone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                }
                else if (split[0].equalsIgnoreCase("link") && (plugin.playerHasPermission(player, "blockdoor.player.delete") || plugin.playerHasPermission(player, "blockdoor.admin.delete"))) {
                    if (split.length == 5) {
                        settings.friendlyCommand = "link-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 6 && plugin.playerHasPermission(player, "blockdoor.admin.delete")) {
                        settings.friendlyCommand = "link-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 7 && plugin.playerHasPermission(player, "blockdoor.admin.delete")) {
                        settings.friendlyCommand = "link-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                }
                else if (!plugin.playerHasPermission(player, "blockdoor.admin.delete") && split.length > 2) {
                    player.sendMessage(ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                    return true;
                }
                else if (split.length > 4) {
                    if (plugin.playerHasPermission(player, "blockdoor.player.delete")) {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                    else {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                }
                else if (split[0].equalsIgnoreCase("door")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "door-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "door-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "door-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("hdoor")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "hdoor-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "hdoor-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "hdoor-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("twostate") || split[0].equalsIgnoreCase("tsdoor")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "twostate-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "twostate-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "twostate-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("trigger") || split[0].equalsIgnoreCase("trig")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "trigger-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "trigger-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "trigger-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("mytrigger") || split[0].equalsIgnoreCase("mytrig")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "mytrigger-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "mytrigger-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "mytrigger-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("redtrig") || split[0].equalsIgnoreCase("redtrigger")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "redtrig-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "redtrig-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "redtrig-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("zone")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "zone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "zone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "zone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("myzone")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "myzone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "myzone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "myzone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("mzone")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "mzone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "mzone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "mzone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("ezone")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "ezone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "ezone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "ezone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("azone")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "azone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "azone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "azone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else if (split[0].equalsIgnoreCase("pzone")) {
                    if (split.length == 2) {
                        settings.friendlyCommand = "pzone-player-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 3) {
                        settings.friendlyCommand = "pzone-admin-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                    else if (split.length == 4) {
                        settings.friendlyCommand = "pzone-world-ddel";
                        return functioncommands.commands(split, player, settings);
                    }
                }
                else {
                    if (plugin.playerHasPermission(player, "blockdoor.player.delete")) {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                    else {
                        return showDelUsage(split.length != 0 ? split[0] : "", player);
                    }
                }
            }
            else if (cmdname.equals("dzone") && (plugin.playerHasPermission(player, "blockdoor.zone"))) {
                settings.friendlyCommand = "dzone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dmyzone") && (plugin.playerHasPermission(player, "blockdoor.myzone"))) {
                settings.friendlyCommand = "dmyzone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dmzone") && (plugin.playerHasPermission(player, "blockdoor.mzone"))) {
                settings.friendlyCommand = "dmzone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dezone") && (plugin.playerHasPermission(player, "blockdoor.ezone"))) {
                settings.friendlyCommand = "dezone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dazone") && (plugin.playerHasPermission(player, "blockdoor.azone"))) {
                settings.friendlyCommand = "dazone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dpzone") && (plugin.playerHasPermission(player, "blockdoor.pzone"))) {
                settings.friendlyCommand = "dpzone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("duzone") && (plugin.playerHasPermission(player, "blockdoor.uzone"))) {
                settings.friendlyCommand = "duzone";
                return ZoneCommands.commands(split, player, settings);
            }
            else if (cmdname.equals("dcancel") && (plugin.playerHasPermission(player, "blockdoor.cancel"))) {
                player.sendMessage("BlockDoor: " + ChatColor.RED + "No active BlockDoor commands to cancel");
            }
            else {
                player.sendMessage("BlockDoor: " + ChatColor.RED + "You dont have permissions for command: " + ChatColor.WHITE + cmdname);
                return true;
            }
        }
        else {
            if (!cmdname.equals("dhdoor"))
                player.sendMessage("BlockDoor command '" + ChatColor.BLUE + settings.friendlyCommand + ChatColor.WHITE + "' canceled.");
            cancelCommands(player);
        }
        /*
         * Another possible solution to /dcancel. This would require the user enter the EXACT command to cancel it. Currently
         * any blockdoor command can cancel another blockdoor command.
         *         
         else if(settings.friendlyCommand.toLowerCase().contains(cmdname.toLowerCase())){
            player.sendMessage("BlockDoor command '" + ChatColor.BLUE + settings.friendlyCommand + ChatColor.WHITE + "' canceled.");
            cancelCommands(player);
         }
         else{
            player.sendMessage("Complete or /dcancel the " + ChatColor.BLUE + BlockDoorSettings.getSettings(player).friendlyCommand + ChatColor.WHITE + " command before using another command.");
            return true;
         }
         */
        return true;
    }

    private boolean showDelUsage(String command, Player player) {
        if (command.equalsIgnoreCase("link")) {
            player.sendMessage(ChatColor.BLUE + "Usage:");
            if (plugin.playerHasPermission(player, "blockdoor.admin.delete"))
                player.sendMessage(ChatColor.GREEN + "/ddel link <triggerType:uzoneTrigger><triggerName><doorType><doorName><player><world>");
            else
                player.sendMessage(ChatColor.GREEN + "/ddel link <triggerType:uzoneTrigger><triggerName><doorType><doorName>");
            return true;
        }
        else if (command.equalsIgnoreCase("uzone")) {
            player.sendMessage(ChatColor.BLUE + "Usage:");
            if (plugin.playerHasPermission(player, "blockdoor.admin.delete"))
                player.sendMessage(ChatColor.GREEN + "/ddel <uzone><zonename><ownername><trigger><world>");
            else
                player.sendMessage(ChatColor.GREEN + "/ddel <uzone><zonename><trigger>");
            return true;
        }
        else {
            player.sendMessage(ChatColor.BLUE + "Usage:");
            player.sendMessage(ChatColor.GREEN + "/ddel <object> <objectname>");
            player.sendMessage(ChatColor.BLUE + "Valid Object types:");
            player.sendMessage(ChatColor.GREEN + "trigger, redtrig, mytrigger, ezone,");
            player.sendMessage(ChatColor.GREEN + "azone, mzone, myzone, pzone, uzone");
            player.sendMessage(ChatColor.GREEN + "link, door, twostate, hdoor");
            return true;
        }
    }

    /*
     * inConsole function ->
     * Commands sent from outside of the game. IE the console
     * Three commands currently in here /dlist, /ddel and /dreload.
     * 
     * Other commands like /dempty, /dfill, /dlink, and /dtoggle
     * could be included here but they have a very limited usefulness
     * for the work required to incorporate them. So they were left out
     * at this time. User demand will dictate their potential inclusion.
     */
    public boolean inConsole(CommandSender sender, Command cmd, String[] split) {
        return consolecommands.command(sender, cmd, split);
    }

    public void cancelCommands(Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        if (settings.command != "") {
            if (settings.notFound == 0) {
                if (settings.door != null) {
                    if (settings.command.equals("door") && settings.door instanceof SingleStateDoor) {
                        dataWriter.allSSDoorsMap.put((SingleStateDoor) settings.door, (SingleStateDoor) settings.door);
                    }
                    else if (settings.command.equals("twostate") && settings.door instanceof TwoStateDoor) {
                        dataWriter.allTSDoorsMap.put((TwoStateDoor) settings.door, (TwoStateDoor) settings.door);
                    }
                    else if (settings.command.equals("hdoor") && settings.door instanceof HDoor) {
                        HDoor hdoor = dataWriter.allHDoorsMap.get(settings.door);
                        if (hdoor.hDoorContents.size() > 0) {
                            dataWriter.allHDoorsMap.get(settings.door).coordsSet = true;
                            if (player.isOnline())
                                player.sendMessage("The Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' was saved.");
                        }
                        else {
                            dataWriter.allHDoorsMap.remove(hdoor);
                            if (player.isOnline())
                                player.sendMessage("The Hybrid Door: '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' was canceled.");
                        }
                        dataWriter.saveDatabase(false);
                    }
                }
            }
            else if (settings.notFound == 1) {
                if (settings.door != null) {
                    if (settings.command.equals("door") && settings.door instanceof SingleStateDoor) {
                        dataWriter.allSSDoorsMap.remove(settings.door);
                    }
                    else if (settings.command.equals("twostate") && settings.door instanceof TwoStateDoor) {
                        dataWriter.allTSDoorsMap.remove(settings.door);
                    }
                    if (settings.command.equals("hdoor") && settings.door instanceof HDoor) {
                        if (((HDoor) settings.door).hDoorContents.size() == 0) {
                            if (player.isOnline())
                                player.sendMessage("The Hybrid Door: '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' was canceled.");
                            dataWriter.allHDoorsMap.remove(settings.door);
                        }
                        else {
                            if (player.isOnline())
                                player.sendMessage("The Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' was saved.");
                            dataWriter.saveDatabase(false);
                        }
                    }
                }
                else if ((settings.command.contains("zone")) && settings.zone != null) {
                    dataWriter.allZonesMap.remove(settings.zone);
                }
            }
            BlockDoorSettings.clearSettings(player);
        }
    }
}