package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class ConsoleCommands {

    /*
     * inConsole function ->
     * Commands sent from outside of the game. IE the console
     * Three commands currently in here /dlist, /ddel and /dreload.
     * 
     * Other commands like /dempty, /dfill, /dlink, and /dtoggle
     * could be included here but they have a very limited usefulness
     * for the work required to incorporate them. So they were left out
     * at this time. User demand will dictate their potential inclusion.
     */

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public ConsoleCommands() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public boolean command(CommandSender sender, Command cmd, String[] split) {
        String cmdname = cmd.getName().toLowerCase();
        if (cmdname.equals("dreload")) {
            sender.sendMessage("One moment, Reloading BlockDoor datafiles...");
            dataWriter.setReload(true);
            dataWriter.reloadFile();
            sender.sendMessage("Finished Reloading BlockDoor datafiles...");
            sender.sendMessage("Check log for errors if necessary.");
        }
        else if (cmdname.equals("dlist")) {
            // prepare the command for use with a non-console method.
            // essentially just moves all the commands entered by the user over one in an array and adds ALL to the first command arg.
            // so the DLister class can handle the input correctly and not error.
            if (split.length == 0) {
                String[] consoleSplit = new String[1];
                consoleSplit[0] = "all";
                boolean admin = true;
                plugin.dlisterhelper.commandList(sender, consoleSplit, admin);
            }
            else if (split.length == 1 && !split[0].equalsIgnoreCase("all")) {
                String[] consoleSplit = new String[2];
                consoleSplit[0] = "all";
                consoleSplit[1] = split[0];
                boolean admin = true;
                plugin.dlisterhelper.commandList(sender, consoleSplit, admin);
            }
            else if (split.length == 2 && !split[0].equalsIgnoreCase("all")) {
                String[] consoleSplit = new String[3];
                consoleSplit[0] = "all";
                consoleSplit[1] = split[0];
                consoleSplit[2] = split[1];
                boolean admin = true;
                plugin.dlisterhelper.commandList(sender, consoleSplit, admin);
            }
            else if (split.length == 3 && !split[0].equalsIgnoreCase("all")) {
                String[] consoleSplit = new String[4];
                consoleSplit[0] = "all";
                consoleSplit[1] = split[0];
                consoleSplit[2] = split[1];
                consoleSplit[3] = split[2];
                boolean admin = true;
                plugin.dlisterhelper.commandList(sender, consoleSplit, admin);
            }
            else {
                sender.sendMessage("Usage: /dlist(type)(player)(page)");
            }
        }
        else if (cmdname.equals("ddel")) {
            if (split.length == 4) {
                if (split[0].equalsIgnoreCase("door")) {
                    Door d = plugin.singlestatedoorhelper.findDoor(split[1].toLowerCase(), split[2], split[3]);
                    if (d != null) {
                        sender.sendMessage("Removing door: " + split[1] + " by " + split[2] + " in " + split[3]);
                        dataWriter.deleteLinks(d);
                        plugin.singlestatedoorhelper.deleteOverlaps((SingleStateDoor) d);
                        dataWriter.updateOldSSDBlockMap((SingleStateDoor) d);
                        dataWriter.allSSDoorsMap.remove(d);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The door - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("hdoor")) {
                    Door d = plugin.hdoorhelper.findDoor(split[1].toLowerCase(), split[2], split[3]);
                    if (d != null) {
                        sender.sendMessage("Removing hybrid door: " + split[1] + " by " + split[2] + " in " + split[3]);
                        dataWriter.updateOldHDBlockMap((HDoor) d);
                        dataWriter.deleteLinks(d);
                        plugin.hdoorhelper.deleteOverlaps((HDoor) d);
                        dataWriter.allHDoorsMap.remove(d);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The hybrid door - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("twostate")) {
                    Door d = plugin.twostatedoorhelper.findDoor(split[1].toLowerCase(), split[2], split[3]);
                    if (d != null) {
                        sender.sendMessage("Removing two state door: " + split[1] + " by " + split[2] + " in " + split[3]);
                        dataWriter.updateOldTSDBlockMap((TwoStateDoor) d);
                        dataWriter.deleteLinks(d);
                        plugin.twostatedoorhelper.deleteOverlaps((TwoStateDoor) d);
                        dataWriter.allTSDoorsMap.remove(d);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The two state door - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("trigger")) {
                    Trigger trigger = plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), split[2], split[3]);
                    if (trigger != null) {
                        sender.sendMessage("Removing trigger: " + split[1] + " by " + split[2] + " in " + split[3]);
                        World world = plugin.getWorld(trigger.trigger_world);
                        if (world != null) {
                            plugin.triggerhelper.updateOverlapsUpdateBlockMap(trigger, world.getBlockAt(trigger.trigger_x, trigger.trigger_y, trigger.trigger_z));
                        }
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Trigger - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("mytrigger")) {
                    Trigger trigger = plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), split[2], split[3]);
                    if (trigger != null) {
                        sender.sendMessage("Removing mytrigger: " + split[1] + " by " + split[2] + " in " + split[3]);
                        World world = plugin.getWorld(trigger.trigger_world);
                        if (world != null) {
                            plugin.triggerhelper.updateOverlapsUpdateBlockMap(trigger, world.getBlockAt(trigger.trigger_x, trigger.trigger_y, trigger.trigger_z));
                        }
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage(ChatColor.RED + "The MyTrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " by " + ChatColor.WHITE + split[2] + ChatColor.RED + " in " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("redtrig")) {
                    Trigger trigger = plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), split[2], split[3]);
                    if (trigger != null) {
                        sender.sendMessage("Removing redstone trigger: " + split[1] + " by " + split[2] + " in " + split[3]);
                        World world = plugin.getWorld(trigger.trigger_world);
                        if (world != null) {
                            plugin.triggerhelper.updateOverlapsUpdateBlockMap(trigger, world.getBlockAt(trigger.trigger_x, trigger.trigger_y, trigger.trigger_z));
                        }
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Redstone Trigger - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("zone")) {
                    Zone z = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), split[2], "", split[3]);
                    if (z != null) {
                        sender.sendMessage("Removing Player zone: " + split[1] + " by " + split[2] + " in " + split[3]);
                        plugin.zonehelper.deleteOverlaps(z);
                        dataWriter.updateOldZoneBlockMap(z);
                        dataWriter.allZonesMap.remove(z);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Player Zone - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("myzone")) {
                    Zone z = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), split[2], "", split[3]);
                    if (z != null) {
                        sender.sendMessage("Removing myzone: " + split[1] + " by " + split[2] + " in " + split[3]);
                        plugin.zonehelper.deleteOverlaps(z);
                        dataWriter.updateOldZoneBlockMap(z);
                        dataWriter.allZonesMap.remove(z);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The MyZone - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("mzone")) {
                    Zone z = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), split[2], "", split[3]);
                    if (z != null) {
                        sender.sendMessage("Removing Mob zone: " + split[1] + " by " + split[2] + " in " + split[3]);
                        plugin.zonehelper.deleteOverlaps(z);
                        dataWriter.updateOldZoneBlockMap(z);
                        dataWriter.allZonesMap.remove(z);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Mob Zone - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("ezone")) {
                    Zone z = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), split[2], "", split[3]);
                    if (z != null) {
                        sender.sendMessage("Removing Living Entity zone: " + split[1] + " by " + split[2] + " in " + split[3]);
                        plugin.zonehelper.deleteOverlaps(z);
                        dataWriter.updateOldZoneBlockMap(z);
                        dataWriter.allZonesMap.remove(z);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Living Entity Zone - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("azone")) {
                    Zone z = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), split[2], "", split[3]);
                    if (z != null) {
                        sender.sendMessage("Removing Aggressive Mob zone: " + split[1] + " by " + split[2] + " in " + split[3]);
                        plugin.zonehelper.deleteOverlaps(z);
                        dataWriter.updateOldZoneBlockMap(z);
                        dataWriter.allZonesMap.remove(z);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Aggressive Mob Zone - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else if (split[0].equalsIgnoreCase("pzone")) {
                    Zone z = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), split[2], "", split[3]);
                    if (z != null) {
                        sender.sendMessage("Removing Passive Mob zone: " + split[1] + " by " + split[2] + " in " + split[3]);
                        plugin.zonehelper.deleteOverlaps(z);
                        dataWriter.updateOldZoneBlockMap(z);
                        dataWriter.allZonesMap.remove(z);
                        dataWriter.saveDatabase(false);
                    }
                    else {
                        sender.sendMessage("The Passive Mob Zone - " + split[1] + " by " + split[2] + " in " + split[3] + " was not found (check spelling)");
                    }
                }
                else {
                    sender.sendMessage("Usage: /ddel <door/trigger/mytrigger/zone/myzone/ezone/pzone/azone/mzone/uzone>");
                    sender.sendMessage("<name> <player> <trigger> <world>");
                }
            }
            else if (split.length == 5 && split[0].equalsIgnoreCase("uzone")) {
                Zone z = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), split[2], split[3], split[4]);
                if (z != null) {
                    sender.sendMessage("Removing Selected Entity zone: " + split[1] + " by " + split[2] + " triggered by " + split[3] + " in " + split[4]);
                    plugin.zonehelper.deleteOverlaps(z);
                    dataWriter.updateOldZoneBlockMap(z);
                    dataWriter.allZonesMap.remove(z);
                    dataWriter.saveDatabase(false);
                }
                else {
                    sender.sendMessage("The Selected Entity Zone - " + split[1] + " by " + split[2] + " triggered by " + split[3] + " in " + split[4] + " was not found (check spelling)");
                }

            }
            else if (split.length == 7 && split[0].equalsIgnoreCase("link")) {
                split[1] = split[1].toLowerCase();
                String uzoneLink[] = split[1].split(":");
                String playerName = split[5];
                String linkWorld = split[6];
                int linkIndex = -1;
                if (uzoneLink[0].equalsIgnoreCase("uzone") && uzoneLink.length > 1) {
                    Zone uzone = plugin.zonehelper.findZone("UZONE", split[2], playerName, uzoneLink[1], linkWorld);
                    linkIndex = plugin.zonehelper.findLink(uzone, getDoor(split[4], playerName, linkWorld, getDoorType(split[3])));
                    if (linkIndex != -1) {
                        sender.sendMessage("Removing link on Uzone: " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                        uzone.links.remove(linkIndex);
                        dataWriter.saveDatabase(false);
                        return true;
                    }
                    else {
                        sender.sendMessage(ChatColor.RED + "Could not find Uzone: " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
                else if (uzoneLink[0].equalsIgnoreCase("uzone")) {
                    sender.sendMessage("Usage: /ddel link <triggerType(:uzoneTrigger)> <triggerName> <doorType> <doorName> <player> <world>");
                    return true;
                }
                else if (split[1].contains("zone")) {
                    String zoneType = parseZoneCommand(split[1]);
                    Zone z = plugin.zonehelper.findZone(zoneType, split[2], playerName, "", linkWorld);
                    if (z != null) {
                        linkIndex = plugin.zonehelper.findLink(z, getDoor(split[4], playerName, linkWorld, getDoorType(split[3])));
                        if (linkIndex != -1) {
                            sender.sendMessage("Removing link on " + zoneType + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                            z.links.remove(linkIndex);
                            dataWriter.saveDatabase(false);
                            return true;
                        }
                        else {
                            sender.sendMessage(ChatColor.RED + "Could not find " + zoneType + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                            return true;
                        }
                    }
                    return true;
                }
                else if (split[1].contains("trig")) {
                    Trigger t = getTrigger(split[1], playerName, linkWorld);
                    if (t != null) {
                        linkIndex = plugin.triggerhelper.findLink(t, getDoor(split[4], playerName, linkWorld, getDoorType(split[3])));
                        if (linkIndex != -1) {
                            sender.sendMessage("Removing link on " + t.trigger_Type + ": " + ChatColor.GREEN + split[2] + ChatColor.WHITE + " linked to door: " + ChatColor.GREEN + split[4]);
                            t.links.remove(linkIndex);
                            dataWriter.saveDatabase(false);
                        }
                        else {
                            sender.sendMessage(ChatColor.RED + "Could not find " + t.trigger_Type + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                            return true;
                        }
                    }
                    else {
                        sender.sendMessage(ChatColor.RED + "Could not find " + split[1] + ": " + ChatColor.YELLOW + split[2] + ChatColor.RED + " linked to door: " + ChatColor.YELLOW + split[4]);
                        return true;
                    }
                }
            }
            else {
                sender.sendMessage("Usage: /ddel <door/trigger/mytrigger/zone/myzone/ezone/pzone/azone/mzone/uzone>");
                sender.sendMessage("<name> <player> <trigger> <world>");
            }
        }
        return true;
    }

    private String parseZoneCommand(String zoneType) {
        zoneType = zoneType.toLowerCase();
        if (zoneType.startsWith("a"))
            return "AZONE";
        else if (zoneType.startsWith("e"))
            return "EZONE";
        else if (zoneType.startsWith("p"))
            return "PZONE";
        else if (zoneType.contains("myz"))
            return "MYZONE";
        else if (zoneType.contains("mz"))
            return "MZONE";
        else if (zoneType.contains("uz")) {
            return "UZONE";
        }
        else
            return "ZONE";
    }

    private Trigger getTrigger(String trigType, String player, String world) {
        Trigger t = null;
        if (trigType.contains("red"))
            t = plugin.triggerhelper.findTrigger("REDTRIG", trigType, player, world);
        else if (trigType.contains("my"))
            t = plugin.triggerhelper.findTrigger("MYTRIGGER", trigType, player, world);
        else
            t = plugin.triggerhelper.findTrigger("TRIGGER", trigType, player, world);
        return t;
    }

    private Door getDoor(String doorName, String playerName, String worldName, String doorType) {
        Door d = null;
        if (getDoorType(doorType).equals("ONESTATE")) {
            d = plugin.singlestatedoorhelper.findDoor(doorName, playerName, worldName);
        }
        else if (getDoorType(doorType).equals("TWOSTATE")) {
            d = plugin.twostatedoorhelper.findDoor(doorName, playerName, worldName);
        }
        else if (getDoorType(doorType).equals("HYBRIDSTATE")) {
            d = plugin.hdoorhelper.findDoor(doorName, playerName, worldName);
        }
        return d;
    }

    private String getDoorType(String doorType) {
        if (doorType.startsWith("s") || doorType.startsWith("S"))
            return "ONESTATE";
        else if (doorType.startsWith("t") || doorType.startsWith("t"))
            return "TWOSTATE";
        else if (doorType.startsWith("h") || doorType.startsWith("H"))
            return "HYBRIDSTATE";
        else
            return null;
    }
}