package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;

public class TriggerCommands {

    public static boolean commands(String[] split, Player player, BlockDoorSettings settings) {
        if (settings.friendlyCommand.equals("dtrig")) {
            if (split.length == 1) {
                settings.command = "trigger";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating trigger: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("dmytrig")) {
            if (split.length == 1) {
                settings.command = "mytrigger";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating My-Trigger: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("dredtrig")) {
            if (split.length == 1) {
                settings.command = "redstonetrigger";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating Redstone trigger: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
        }
        return false;
    }
}