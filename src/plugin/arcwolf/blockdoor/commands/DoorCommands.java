package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Doors.HDoor;
import plugin.arcwolf.blockdoor.Doors.SingleStateDoor;
import plugin.arcwolf.blockdoor.Doors.TwoStateDoor;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;

public class DoorCommands {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public DoorCommands() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public boolean commands(String[] split, Player player, BlockDoorSettings settings) {
        if (settings.friendlyCommand.equals("ddoor")) {
            if (split.length == 1) {
                settings.command = "door";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("dtwostate")) {
            if (split.length == 1) {
                settings.command = "twostate";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(split[0], player.getName(), player.getWorld().getName());
                if (twostate != null) {
                    twostate.softLock();
                    twostate.close();
                }
                player.sendMessage("Creating Two State Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else if (split.length == 2) {
                TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(split[1], player.getName(), player.getWorld().getName());
                if (twostate != null) {
                    if (split[0].equalsIgnoreCase("unlock")) {
                        twostate.softLock();
                        twostate.world = plugin.getWorld(twostate.door_world);
                        twostate.close();
                        dataWriter.saveDatabase(false);
                        player.sendMessage(ChatColor.WHITE + "BlockDoor: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " is now " + ChatColor.GREEN + split[0].toUpperCase() + "ED");
                        return true;
                    }
                    else if (split[0].equalsIgnoreCase("lock")) {
                        twostate.world = plugin.getWorld(twostate.door_world);
                        plugin.twostatedoorhelper.lock(twostate);
                        dataWriter.saveDatabase(false);
                        player.sendMessage(ChatColor.WHITE + "BlockDoor: " + ChatColor.GREEN + split[1] + ChatColor.WHITE + " is now " + ChatColor.GREEN + split[0].toUpperCase() + "ED");
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "Two State Door " + ChatColor.WHITE + split[1] + ChatColor.RED + " not found!");
                    return true;
                }
            }
        }
        else if (settings.friendlyCommand.equals("dhdoor")) {
            if (split.length == 1) {
                settings.command = "hdoor";
                settings.name = split[0].toLowerCase();
                Door d = plugin.hdoorhelper.findDoor(split[0].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null)
                    player.sendMessage("Creating Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                else
                    player.sendMessage("Editing Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                settings.door = plugin.hdoorhelper.findOrCreateDoor(split[0].toLowerCase(), player.getName(), player.getWorld().getName(), player);
                if (settings.door.coordsSet)
                    settings.door.close();
                return true;
            }
            else if (split.length == 2 && (split[1].equalsIgnoreCase("done"))) {
                Door d = plugin.hdoorhelper.findDoor(split[0].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    d.coordsSet = true;
                    dataWriter.saveDatabase(false);
                    player.sendMessage("The Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' was saved.");
                    settings.command = "";
                }
                else {
                    if (!settings.name.equals(""))
                        player.sendMessage(ChatColor.RED + "Wrong Hybrid Door name: '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' is currently being edited.");
                    else
                        player.sendMessage(ChatColor.RED + "No Hybrid Door currently being edited.");
                }
                return true;
            }// Disabled admin hdoor edit for now
             // Could cause CME's if two people edit the same door at once
             //else if (split.length == 2 && !(split[1].equalsIgnoreCase("done"))) {
             //int id = plugin.hdoorhelper.findDoor(split[0].toLowerCase(), split[1], player.getWorld().getName());
             //if (id != -1) {
             //    player.sendMessage("Editing " + ChatColor.GREEN + split[1] + "'s" + ChatColor.WHITE + " Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
             //    settings.index = plugin.hdoorhelper.findDoor(split[0].toLowerCase(), split[1], player.getWorld().getName(), player);
             //}
             //else {
             //    player.sendMessage(ChatColor.RED + "Hybrid Door name: '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' does not exist");
             //}
             //return true;
             //}
             //else if (split.length == 3 && (split[2].equalsIgnoreCase("done"))) {
             //int id = plugin.hdoorhelper.findDoor(split[0].toLowerCase(), split[1], player.getWorld().getName());
             //if (id != -1) {
             //   HDoor hdoor = dataWriter.hdoor.get(id);
             //    dataWriter.writeItem(hdoor, "HDOOR", hdoor.creator, hdoor.name, hdoor.d_world);
             //    player.sendMessage(ChatColor.GREEN + "'s" + ChatColor.WHITE + " Hybrid Door: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' was saved.");
             //    settings.command = "";
             //}
             //else {
             //    if (!settings.name.equals(""))
             //        player.sendMessage(ChatColor.RED + "Wrong Hybrid Door name: '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' is currently being edited.");
             //    else
             //        player.sendMessage(ChatColor.RED + "No Hybrid Door is currently being edited.");
             //}
             //return true;
             //}
        }
        else if (settings.friendlyCommand.equals("admin-dtoggle")) {
            settings.name = split[0].toLowerCase();
            SingleStateDoor door = plugin.singlestatedoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (door != null) {
                door.world = plugin.getWorld(door.door_world);
                if ((door.door_world.equals(player.getWorld().getName())) && (door.door_creator.equals(split[1]))) {
                    door.toggle();
                    dataWriter.saveDatabase(false);
                    player.sendMessage("Toggling: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant toggle '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + door.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dtoggle")) {
            settings.name = split[0].toLowerCase();
            SingleStateDoor door = plugin.singlestatedoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (door != null) {
                door.world = plugin.getWorld(door.door_world);
                if ((door.door_world.equals(player.getWorld().getName())) && (door.door_creator.equals(player.getName()))) {
                    door.toggle();
                    dataWriter.saveDatabase(false);
                    player.sendMessage("Toggling: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant toggle '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + door.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dtoggle2")) {
            settings.name = split[0].toLowerCase();
            TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (twostate != null) {
                twostate.world = plugin.getWorld(twostate.door_world);
                if ((twostate.door_world.equals(player.getWorld().getName())) && (twostate.door_creator.equals(split[1]))) {
                    twostate.toggle();
                    player.sendMessage("Toggling: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant toggle '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + twostate.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dtoggle2")) {
            settings.name = split[0].toLowerCase();
            TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (twostate != null) {
                twostate.world = plugin.getWorld(twostate.door_world);
                if ((twostate.door_world.equals(player.getWorld().getName())) && (twostate.door_creator.equals(player.getName()))) {
                    twostate.toggle();
                    player.sendMessage("Toggling: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant toggle '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + twostate.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dtoggle3")) {
            settings.name = split[0].toLowerCase();
            Door d = plugin.hdoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (d != null) {
                HDoor hdoor = (HDoor) d;
                hdoor.world = plugin.getWorld(hdoor.door_world);
                if ((hdoor.door_world.equals(player.getWorld().getName())) && (hdoor.door_creator.equals(split[1]))) {
                    hdoor.toggle();
                    player.sendMessage("Toggling: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant toggle '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + hdoor.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dtoggle3")) {
            settings.name = split[0].toLowerCase();
            Door d = plugin.hdoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (d != null) {
                HDoor hdoor = (HDoor) d;
                hdoor.world = plugin.getWorld(hdoor.door_world);
                if ((hdoor.door_world.equals(player.getWorld().getName())) && (hdoor.door_creator.equals(player.getName()))) {
                    hdoor.toggle();
                    player.sendMessage("Toggling: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant toggle '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + hdoor.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dopen")) {
            settings.name = split[0].toLowerCase();
            SingleStateDoor door = plugin.singlestatedoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (door != null) {
                door.world = plugin.getWorld(door.door_world);
                if ((door.door_world.equals(player.getWorld().getName())) && (door.door_creator.equals(split[1]))) {
                    door.open();
                    dataWriter.saveDatabase(false);
                    player.sendMessage("Opening: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Open '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + door.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dopen")) {
            settings.name = split[0].toLowerCase();
            SingleStateDoor door = plugin.singlestatedoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (door != null) {
                door.world = plugin.getWorld(door.door_world);
                if ((door.door_world.equals(player.getWorld().getName())) && (door.door_creator.equals(player.getName()))) {
                    door.open();
                    dataWriter.saveDatabase(false);
                    player.sendMessage("Opening: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Open '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + door.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dclose")) {
            settings.name = split[0].toLowerCase();
            SingleStateDoor door = plugin.singlestatedoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (door != null) {
                door.world = plugin.getWorld(door.door_world);
                if ((door.door_world.equals(player.getWorld().getName())) && (door.door_creator.equals(split[1]))) {
                    door.close();
                    dataWriter.saveDatabase(false);
                    player.sendMessage("Closing: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Close '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + door.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dclose")) {
            settings.name = split[0].toLowerCase();
            SingleStateDoor door = plugin.singlestatedoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (door != null) {
                door.world = plugin.getWorld(door.door_world);
                if ((door.door_world.equals(player.getWorld().getName())) && (door.door_creator.equals(player.getName()))) {
                    door.close();
                    dataWriter.saveDatabase(false);
                    player.sendMessage("Closing: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Close '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + door.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dopen2")) {
            settings.name = split[0].toLowerCase();
            TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (twostate != null) {
                twostate.world = plugin.getWorld(twostate.door_world);
                if ((twostate.door_world.equals(player.getWorld().getName())) && (twostate.door_creator.equals(split[1]))) {
                    twostate.open();
                    player.sendMessage("Opening: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Open '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + twostate.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dopen2")) {
            settings.name = split[0].toLowerCase();
            TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (twostate != null) {
                twostate.world = plugin.getWorld(twostate.door_world);
                if ((twostate.door_world.equals(player.getWorld().getName())) && (twostate.door_creator.equals(player.getName()))) {
                    twostate.open();
                    player.sendMessage("Opening: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Open '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + twostate.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dclose2")) {
            settings.name = split[0].toLowerCase();
            TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (twostate != null) {
                twostate.world = plugin.getWorld(twostate.door_world);
                if ((twostate.door_world.equals(player.getWorld().getName())) && (twostate.door_creator.equals(split[1]))) {
                    twostate.close();
                    player.sendMessage("Closing: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Close '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + twostate.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dclose2")) {
            settings.name = split[0].toLowerCase();
            TwoStateDoor twostate = plugin.twostatedoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (twostate != null) {
                twostate.world = plugin.getWorld(twostate.door_world);
                if ((twostate.door_world.equals(player.getWorld().getName())) && (twostate.door_creator.equals(player.getName()))) {
                    twostate.close();
                    player.sendMessage("Closing: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Close '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + twostate.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dopen3")) {
            settings.name = split[0].toLowerCase();
            Door d = plugin.hdoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (d != null) {
                HDoor hdoor = (HDoor) d;
                hdoor.world = plugin.getWorld(hdoor.door_world);
                if ((hdoor.door_world.equals(player.getWorld().getName())) && (hdoor.door_creator.equals(split[1]))) {
                    hdoor.open();
                    player.sendMessage("Opening: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Open '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + hdoor.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dopen3")) {
            settings.name = split[0].toLowerCase();
            Door d = plugin.hdoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (d != null) {
                HDoor hdoor = (HDoor) d;
                hdoor.world = plugin.getWorld(hdoor.door_world);
                if ((hdoor.door_world.equals(player.getWorld().getName())) && (hdoor.door_creator.equals(player.getName()))) {
                    hdoor.open();
                    player.sendMessage("Opening: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Open '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + hdoor.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("admin-dclose3")) {
            settings.name = split[0].toLowerCase();
            Door d = plugin.hdoorhelper.findDoor(settings.name, split[1], player.getWorld().getName());
            if (d != null) {
                HDoor hdoor = (HDoor) d;
                hdoor.world = plugin.getWorld(hdoor.door_world);
                if ((hdoor.door_world.equals(player.getWorld().getName())) && (hdoor.door_creator.equals(split[1]))) {
                    hdoor.close();
                    player.sendMessage("Closing: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Close '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + hdoor.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("player-dclose3")) {
            settings.name = split[0].toLowerCase();
            Door d = plugin.hdoorhelper.findDoor(settings.name, player.getName(), player.getWorld().getName());
            if (d != null) {
                HDoor hdoor = (HDoor) d;
                hdoor.world = plugin.getWorld(hdoor.door_world);
                if ((hdoor.door_world.equals(player.getWorld().getName())) && (hdoor.door_creator.equals(player.getName()))) {
                    hdoor.close();
                    player.sendMessage("Closing: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                    return true;
                }
                else {
                    player.sendMessage("Cant Close '" + ChatColor.RED + settings.name + ChatColor.WHITE + "' created by '" + ChatColor.RED + split[1] + ChatColor.WHITE + "'");
                    player.sendMessage("Door in " + ChatColor.RED + hdoor.door_world + ChatColor.WHITE + " you are in " + ChatColor.RED + player.getWorld().getName());
                    return true;
                }
            }
            else {
                player.sendMessage(split[0] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("dfill")) {
            SingleStateDoor door = null;
            if (split.length == 2) {
                String decodedItem = "";
                String[] encodedItem = null;
                door = plugin.singlestatedoorhelper.findDoor(split[0].toLowerCase(), player.getName(), player.getWorld().getName());
                if (split[1].contains(":")) {
                    encodedItem = split[1].split(":");
                    decodedItem = plugin.itemcodeshelper.getItem(encodedItem[0], encodedItem[1]);
                }
                else {
                    decodedItem = plugin.itemcodeshelper.getItem(split[1], "-1");
                }
                if (decodedItem.contains("INVALID")) {
                    player.sendMessage(ChatColor.RED + decodedItem + " = " + split[1]);
                    return true;
                }
                else {
                    if (door != null) {
                        door.fill = decodedItem;
                        dataWriter.saveDatabase(false);
                        player.sendMessage("Setting door (" + ChatColor.GREEN + door.door_name + ChatColor.WHITE + ") fill type to: " + ChatColor.GREEN + door.fill);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[0] + ChatColor.RED + " was not found");
                        return true;
                    }
                }
            }
        }
        else if (settings.friendlyCommand.equals("dempty")) {
            SingleStateDoor door = null;
            if (split.length == 2) {
                String decodedItem = "";
                String[] encodedItem = null;
                door = plugin.singlestatedoorhelper.findDoor(split[0].toLowerCase(), player.getName(), player.getWorld().getName());
                if (split[1].contains(":")) {
                    encodedItem = split[1].split(":");
                    decodedItem = plugin.itemcodeshelper.getItem(encodedItem[0], encodedItem[1]);
                }
                else {
                    decodedItem = plugin.itemcodeshelper.getItem(split[1], "-1");
                }
                if (decodedItem.contains("INVALID")) {
                    player.sendMessage(ChatColor.RED + decodedItem + " = " + split[1]);
                    return true;
                }
                else {
                    if (door != null) {
                        door.empty = decodedItem;
                        dataWriter.saveDatabase(false);
                        player.sendMessage("Setting door (" + ChatColor.GREEN + door.door_name + ChatColor.WHITE + ") empty type to: " + ChatColor.GREEN + door.empty);
                        return true;
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[0] + ChatColor.RED + " was not found");
                        return true;
                    }
                }
            }
        }
        return false;
    }
}