package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;

public class ZoneCommands {

    public static boolean commands(String[] split, Player player, BlockDoorSettings settings) {

        if (settings.friendlyCommand.equals("dzone")) {
            if (split.length == 1) {
                settings.command = "zone";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating Player zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        else if (settings.friendlyCommand.equals("dmyzone")) {
            if (split.length == 1) {
                settings.command = "myzone";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating My-zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        else if (settings.friendlyCommand.equals("dmzone")) {
            if (split.length == 1) {
                settings.command = "mzone";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating Mob zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        else if (settings.friendlyCommand.equals("dezone")) {
            if (split.length == 1) {
                settings.command = "ezone";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating Living Entity zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        else if (settings.friendlyCommand.equals("dazone")) {
            if (split.length == 1) {
                settings.command = "azone";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating Aggressive Mob zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        else if (settings.friendlyCommand.equals("dpzone")) {
            if (split.length == 1) {
                settings.command = "pzone";
                settings.name = split[0].toLowerCase();
                settings.select = 1;
                player.sendMessage("Creating Passive Mob zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        else if (settings.friendlyCommand.equals("duzone")) {
            if (split.length == 2) {
                settings.command = "uzone";
                settings.name = split[0].toLowerCase();
                settings.trigger = split[1];
                settings.select = 1;
                player.sendMessage("Creating Selected Entity zone: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'" + " triggered by '" + ChatColor.GREEN + split[1] + ChatColor.WHITE + "'");
                return true;
            }
            else
                return false;
        }
        return false;
    }
}