package plugin.arcwolf.blockdoor.commands;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;
import plugin.arcwolf.blockdoor.BlockDoor.Types;
import plugin.arcwolf.blockdoor.Link;
import plugin.arcwolf.blockdoor.Doors.Door;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class LinkCommands {

    private BlockDoor plugin;
    private DataWriter dataWriter;

    public LinkCommands() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    public boolean commands(String[] split, Player player, BlockDoorSettings settings) {
        String world = player.getWorld().getName();
        if (settings.friendlyCommand.equals("stateSelected-Uzone")) {
            Zone uzone = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), player.getName(), split[2], player.getWorld().getName());
            if (uzone != null) {
                try {
                    int state = Integer.parseInt(split[5]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[3].toLowerCase(), player.getName(), world);
                        if (d != null) {
                            if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(uzone, d);
                                if (linkIndex == -1) {
                                    uzone.links.add(new Link(d, split[4], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[4].equalsIgnoreCase("TOGGLE"))
                                        uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[4].equalsIgnoreCase("OPEN"))
                                        uzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[4].equalsIgnoreCase("CLOSE"))
                                        uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(uzone, d);
                                if (linkIndex == -1) {
                                    uzone.links.add(new Link(d, split[4], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[4].equalsIgnoreCase("TOGGLE"))
                                        uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[4].equalsIgnoreCase("OPEN"))
                                        uzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[4].equalsIgnoreCase("CLOSE"))
                                        uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(uzone, d);
                                if (linkIndex == -1) {
                                    uzone.links.add(new Link(d, split[4], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[4].equalsIgnoreCase("TOGGLE"))
                                        uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[4].equalsIgnoreCase("OPEN"))
                                        uzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[4].equalsIgnoreCase("CLOSE"))
                                        uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Selected Entity zone - " + ChatColor.WHITE + split[1] + " triggered by: " + split[2] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-Uzone")) {
            Zone uzone = plugin.zonehelper.findZone("UZONE", split[1].toLowerCase(), player.getName(), split[2], player.getWorld().getName());
            if (uzone != null) {
                try {
                    Door d = plugin.singlestatedoorhelper.findDoor(split[3].toLowerCase(), player.getName(), player.getWorld().getName());
                    if (d != null) {
                        if (split[4].equalsIgnoreCase("toggle") || split[4].equalsIgnoreCase("open") || split[4].equalsIgnoreCase("close")) {
                            int linkIndex = plugin.zonehelper.findLink(uzone, d);
                            if (linkIndex == -1) {
                                uzone.links.add(new Link(d, split[4], DoorTypes.ONESTATE));
                                dataWriter.saveDatabase(false);
                                player.sendMessage("Added Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                return true;
                            }
                            else {
                                if (split[4].equalsIgnoreCase("TOGGLE"))
                                    uzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                else if (split[4].equalsIgnoreCase("OPEN"))
                                    uzone.links.get(linkIndex).linkType = Types.OPEN;
                                else if (split[4].equalsIgnoreCase("CLOSE"))
                                    uzone.links.get(linkIndex).linkType = Types.CLOSE;
                                else {
                                    player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                    return false;
                                }
                                dataWriter.saveDatabase(false);
                                player.sendMessage("Updated Selected Entity zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' triggering on: '" + ChatColor.GREEN + split[2] + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[3].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[4].toLowerCase());
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                            player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[3] + ChatColor.RED + " was not found");
                        return true;
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <zonename> <trigger> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink uzone zonename johndoe doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Selected Entity zone - " + ChatColor.WHITE + split[1] + " triggered by: " + split[2] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-Trigger")) {
            Trigger trigger = plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (trigger != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(trigger, d);
                                if (linkIndex == -1) {
                                    trigger.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        trigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        trigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(trigger, d);
                                if (linkIndex == -1) {
                                    trigger.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        trigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        trigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Two State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(trigger, d);
                                if (linkIndex == -1) {
                                    trigger.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        trigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        trigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                        return true;
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-Trigger")) {
            Trigger trigger = plugin.triggerhelper.findTrigger("TRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (trigger != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.triggerhelper.findLink(trigger, d);
                        if (linkIndex == -1) {
                            trigger.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                trigger.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                trigger.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                trigger.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink trigger triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-MyTrigger")) {
            Trigger mytrigger = plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mytrigger != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(mytrigger, d);
                                if (linkIndex == -1) {
                                    mytrigger.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(mytrigger, d);
                                if (linkIndex == -1) {
                                    mytrigger.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Two State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(mytrigger, d);
                                if (linkIndex == -1) {
                                    mytrigger.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The mytrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-MyTrigger")) {
            Trigger mytrigger = plugin.triggerhelper.findTrigger("MYTRIGGER", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (mytrigger != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.triggerhelper.findLink(mytrigger, d);
                        if (linkIndex == -1) {
                            mytrigger.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                mytrigger.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                mytrigger.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                mytrigger.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated mytrigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink mytrigger triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The mytrigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-redTrig")) {
            Trigger redtrig = plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (redtrig != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(redtrig, d);
                                if (linkIndex == -1) {
                                    redtrig.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        redtrig.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(redtrig, d);
                                if (linkIndex == -1) {
                                    redtrig.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        redtrig.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Two State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.triggerhelper.findLink(redtrig, d);
                                if (linkIndex == -1) {
                                    redtrig.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        redtrig.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid State door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The redstone trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-redTrig")) {
            Trigger redtrig = plugin.triggerhelper.findTrigger("REDTRIG", split[1].toLowerCase(), player.getName(), player.getWorld().getName());
            if (redtrig != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.triggerhelper.findLink(redtrig, d);
                        if (linkIndex == -1) {
                            redtrig.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                redtrig.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                redtrig.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                redtrig.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated redstone trigger '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink redtrig triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The redstone trigger - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-zone")) {
            Zone zone = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (zone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(zone, d);
                                if (linkIndex == -1) {
                                    zone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        zone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        zone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        zone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(zone, d);
                                if (linkIndex == -1) {
                                    zone.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        zone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        zone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        zone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(zone, d);
                                if (linkIndex == -1) {
                                    zone.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        zone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        zone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        zone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-zone")) {
            Zone zone = plugin.zonehelper.findZone("ZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (zone != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(zone, d);
                        if (linkIndex == -1) {
                            zone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                zone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                zone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                zone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink zone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-myzone")) {
            Zone myzone = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (myzone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(myzone, d);
                                if (linkIndex == -1) {
                                    myzone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        myzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        myzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(myzone, d);
                                if (linkIndex == -1) {
                                    myzone.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        myzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        myzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(myzone, d);
                                if (linkIndex == -1) {
                                    myzone.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        myzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        myzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The My zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-myzone")) {
            Zone myzone = plugin.zonehelper.findZone("MYZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (myzone != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(myzone, d);
                        if (linkIndex == -1) {
                            myzone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added myzone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                myzone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                myzone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                myzone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated My zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink myzone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The myzone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-azone")) {
            Zone azone = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (azone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(azone, d);
                                if (linkIndex == -1) {
                                    azone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        azone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        azone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        azone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(azone, d);
                                if (linkIndex == -1) {
                                    azone.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        azone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        azone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        azone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(azone, d);
                                if (linkIndex == -1) {
                                    azone.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        azone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        azone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        azone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-azone")) {
            Zone azone = plugin.zonehelper.findZone("AZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (azone != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(azone, d);
                        if (linkIndex == -1) {
                            azone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                azone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                azone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                azone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated Aggressive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink azone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Aggressive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " entered was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-ezone")) {
            Zone ezone = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (ezone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(ezone, d);
                                if (linkIndex == -1) {
                                    ezone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        ezone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        ezone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(ezone, d);
                                if (linkIndex == -1) {
                                    ezone.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        ezone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        ezone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(ezone, d);
                                if (linkIndex == -1) {
                                    ezone.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        ezone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        ezone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entities zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-ezone")) {
            Zone ezone = plugin.zonehelper.findZone("EZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (ezone != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(ezone, d);
                        if (linkIndex == -1) {
                            ezone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                ezone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                ezone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                ezone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated Living Entities zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink ezone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Living Entities zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " entered was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-mzone")) {
            Zone mzone = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (mzone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(mzone, d);
                                if (linkIndex == -1) {
                                    mzone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        mzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        mzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(mzone, d);
                                if (linkIndex == -1) {
                                    mzone.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        mzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        mzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(mzone, d);
                                if (linkIndex == -1) {
                                    mzone.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        mzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        mzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The All Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-mzone")) {
            Zone mzone = plugin.zonehelper.findZone("MZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (mzone != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(mzone, d);
                        if (linkIndex == -1) {
                            mzone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                mzone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                mzone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                mzone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated All Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The All Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("stateSelected-pzone")) {
            Zone pzone = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (pzone != null) {
                try {
                    int state = Integer.parseInt(split[4]);
                    if (state == 1) {
                        Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(pzone, d);
                                if (linkIndex == -1) {
                                    pzone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        pzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        pzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 2) {
                        Door d = plugin.twostatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(pzone, d);
                                if (linkIndex == -1) {
                                    pzone.links.add(new Link(d, split[3], DoorTypes.TWOSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - TwoState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        pzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        pzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                    else if (state == 3) {
                        Door d = plugin.hdoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                        if (d != null) {
                            if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                                int linkIndex = plugin.zonehelper.findLink(pzone, d);
                                if (linkIndex == -1) {
                                    pzone.links.add(new Link(d, split[3], DoorTypes.HYBRIDSTATE));
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - HybridState - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                                else {
                                    if (split[3].equalsIgnoreCase("TOGGLE"))
                                        pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                                    else if (split[3].equalsIgnoreCase("OPEN"))
                                        pzone.links.get(linkIndex).linkType = Types.OPEN;
                                    else if (split[3].equalsIgnoreCase("CLOSE"))
                                        pzone.links.get(linkIndex).linkType = Types.CLOSE;
                                    else {
                                        player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                        return false;
                                    }
                                    dataWriter.saveDatabase(false);
                                    player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                                    return true;
                                }
                            }
                            else {
                                player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                                player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                                return true;
                            }
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "The Hybrid door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                            return true;
                        }
                    }
                } catch (Exception e) {
                    player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                    player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " was not found");
                return true;
            }
        }
        else if (settings.friendlyCommand.equals("singleState-pzone")) {
            Zone pzone = plugin.zonehelper.findZone("PZONE", split[1].toLowerCase(), player.getName(), "", player.getWorld().getName());
            if (pzone != null) {
                Door d = plugin.singlestatedoorhelper.findDoor(split[2].toLowerCase(), player.getName(), player.getWorld().getName());
                if (d != null) {
                    if (split[3].equalsIgnoreCase("toggle") || split[3].equalsIgnoreCase("open") || split[3].equalsIgnoreCase("close")) {
                        int linkIndex = plugin.zonehelper.findLink(pzone, d);
                        if (linkIndex == -1) {
                            pzone.links.add(new Link(d, split[3], DoorTypes.ONESTATE));
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Added Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                        else {
                            if (split[3].equalsIgnoreCase("TOGGLE"))
                                pzone.links.get(linkIndex).linkType = Types.TOGGLE;
                            else if (split[3].equalsIgnoreCase("OPEN"))
                                pzone.links.get(linkIndex).linkType = Types.OPEN;
                            else if (split[3].equalsIgnoreCase("CLOSE"))
                                pzone.links.get(linkIndex).linkType = Types.CLOSE;
                            else {
                                player.sendMessage(ChatColor.RED + "Not a valid link type: " + ChatColor.WHITE + "OPEN, CLOSE, TOGGLE");
                                return false;
                            }
                            dataWriter.saveDatabase(false);
                            player.sendMessage("Updated Passive Mob zone '" + ChatColor.GREEN + split[1].toLowerCase() + ChatColor.WHITE + "' linking to: '" + ChatColor.GREEN + split[2].toLowerCase() + ChatColor.WHITE + "' - " + ChatColor.GREEN + split[3].toLowerCase());
                            return true;
                        }
                    }
                    else {
                        player.sendMessage(ChatColor.BLUE + "Usage: /dlink <triggerType> <triggername> <doorname> <type> <doorType>");
                        player.sendMessage(ChatColor.BLUE + "Ex: /dlink mzone triggername doorname toggle 1");
                        return true;
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "The door - " + ChatColor.WHITE + split[2] + ChatColor.RED + " was not found");
                    return true;
                }
            }
            else {
                player.sendMessage(ChatColor.RED + "The Passive Mob zone - " + ChatColor.WHITE + split[1] + ChatColor.RED + " entered was not found");
                return true;
            }
        }
        return false;
    }
}