package plugin.arcwolf.blockdoor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Utils.PlayerNameComparator;
import plugin.arcwolf.blockdoor.Utils.TypeNameComparator;
import plugin.arcwolf.blockdoor.Zones.Zone;

public class DListerHelper {

    private BlockDoor plugin;
    private DataWriter dataWriter;
    private ArrayList<DLister> listObjects = new ArrayList<DLister>();

    public DListerHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
    }

    //process the user command and output the correct information for sort function
    public void commandList(CommandSender sender, String[] split, boolean admin) {
        String listPlayer = null;
        if (sender instanceof Player)
            listPlayer = ((Player) sender).getDisplayName();
        int page = 1;
        String Type = "default";
        if (admin) { // admin commands
            if (split.length == 0) { //test for /dlist
                createList(sender, page, Type, listPlayer);
            }
            else if (split.length == 1) { //test for /dlist <all> or /dlist <type> or /dlist <page> or /dlist <player>
                if (testSplit(split[0])) {
                    Type = split[0].toLowerCase();
                    createList(sender, page, Type, listPlayer);
                }
                else if (split[0].equalsIgnoreCase("all")) {
                    listPlayer = null;
                    createList(sender, page, Type, listPlayer);
                }
                else {
                    try {
                        page = Integer.parseInt(split[0]);
                        createList(sender, page, Type, listPlayer);
                    } catch (Exception e) {
                        listPlayer = split[0];
                        createList(sender, page, Type, listPlayer);
                    }
                }
            }
            else if (split.length == 2) {// /dlist <all> <type> or /dlist <all> <page> or /dlist <all> <player> 
                                         // /dlist <type> <player> or /dlist <type> <page> or /dlist <player> <page>
                if (split[0].equalsIgnoreCase("all")) {
                    if (testSplit(split[1])) {
                        Type = split[1].toLowerCase();
                        listPlayer = null;
                        createList(sender, page, Type, listPlayer);
                    }
                    else {
                        try {
                            page = Integer.parseInt(split[1]);
                            listPlayer = null;
                            createList(sender, page, Type, listPlayer);
                        } catch (Exception e) {
                            listPlayer = split[1];
                            createList(sender, page, Type, listPlayer);
                        }
                    }
                }
                else if (testSplit(split[0])) {
                    try {
                        page = Integer.parseInt(split[1]);
                        Type = split[0].toLowerCase();
                        createList(sender, page, Type, listPlayer);
                    } catch (Exception e) {
                        Type = split[0].toLowerCase();
                        listPlayer = split[1];
                        createList(sender, page, Type, listPlayer);
                    }
                }
                else {
                    try {
                        page = Integer.parseInt(split[1]);
                        listPlayer = split[0];
                        createList(sender, page, Type, listPlayer);
                    } catch (Exception e) {
                        if (sender instanceof Player)
                            sender.sendMessage(ChatColor.RED + "Not a valid page");
                        else
                            sender.sendMessage("Not a valid page");
                    }
                }
            }
            else if (split.length == 3) {// /dlist <all> <type> <page> or /dlist <all> <type> <player> or /dlist <all> <player> <page> or /dlist <type> <player> <page>
                if (split[0].equalsIgnoreCase("all")) {
                    if (testSplit(split[1])) {
                        try {
                            page = Integer.parseInt(split[2]);
                            Type = split[1].toLowerCase();
                            listPlayer = null;
                            createList(sender, page, Type, listPlayer);
                        } catch (Exception e) {
                            Type = split[1].toLowerCase();
                            listPlayer = split[2];
                            createList(sender, page, Type, listPlayer);
                        }
                    }
                    else {
                        try {
                            page = Integer.parseInt(split[2]);
                            listPlayer = split[1];
                            createList(sender, page, Type, listPlayer);
                        } catch (Exception e) {
                            if (sender instanceof Player)
                                sender.sendMessage(ChatColor.RED + "Not a valid page");
                            else
                                sender.sendMessage("Not a valid page");
                        }
                    }
                }
                else if (testSplit(split[0])) {
                    try {
                        page = Integer.parseInt(split[2]);
                        listPlayer = split[1];
                        Type = split[0].toLowerCase();
                        createList(sender, page, Type, listPlayer);
                    } catch (Exception e) {
                        if (sender instanceof Player)
                            sender.sendMessage(ChatColor.RED + "Not a valid page");
                        else
                            sender.sendMessage("Not a valid page");
                    }
                }
                else {
                    if (sender instanceof Player)
                        sender.sendMessage(ChatColor.BLUE + "Usage: /dlist (all)(type)(player)(page)");
                    else
                        sender.sendMessage("Usage: /dlist (type)(player)(page)");
                }
            }
            else if (split.length == 4) {// /dlist <all> <type> <player> <page>
                if (split[0].equalsIgnoreCase("all")) {
                    if (testSplit(split[1])) {
                        try {
                            page = Integer.parseInt(split[3]);
                            Type = split[1].toLowerCase();
                            listPlayer = split[2];
                            createList(sender, page, Type, listPlayer);
                        } catch (Exception e) {
                            if (sender instanceof Player)
                                sender.sendMessage(ChatColor.RED + "Not a valid page");
                            else
                                sender.sendMessage("Not a valid page");
                        }
                    }
                    else {
                        if (sender instanceof Player)
                            sender.sendMessage(ChatColor.BLUE + "Usage: /dlist (all)(type)(player)(page)");
                        else
                            sender.sendMessage("Usage: /dlist (type)(player)(page)");
                    }
                }
            }
            else { // something wasnt entered right
                if (sender instanceof Player)
                    sender.sendMessage(ChatColor.BLUE + "Usage: /dlist (all)(type)(player)(page)");
                else
                    sender.sendMessage("Usage: /dlist (type)(player)(page)");
            }
        }
        else { //player commands
            if (split.length == 0) { // /dlist
                createList(sender, page, Type, listPlayer);
            }
            else if (split.length == 1) { // /dlist <page> or /dlist <type>
                if (testSplit(split[0])) {
                    Type = split[0].toLowerCase();
                    createList(sender, page, Type, listPlayer);
                }
                else {
                    try {
                        page = Integer.parseInt(split[0]);
                        createList(sender, page, Type, listPlayer);
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.BLUE + "Usage: /dlist (type)(page)");
                    }
                }
            }
            else if (split.length == 2) { // /dlist <type> <page>
                if (testSplit(split[0])) {
                    try {
                        page = Integer.parseInt(split[1]);
                        Type = split[0].toLowerCase();
                        createList(sender, page, Type, listPlayer);
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.RED + "Not a valid page");
                    }
                }
                else { // something wasnt entered right
                    sender.sendMessage(ChatColor.BLUE + "Usage: /dlist (type)(page)");
                }
            }
            else { // something wasnt entered right
                sender.sendMessage(ChatColor.BLUE + "Usage: /dlist (type)(page)");
            }
        }
    }

    private boolean testSplit(String split) {
        return split.equalsIgnoreCase("door") || split.equalsIgnoreCase("hdoor") || split.equalsIgnoreCase("tsdoor") || split.equalsIgnoreCase("twostate") ||
                split.equalsIgnoreCase("twostatedoor") || split.equalsIgnoreCase("trigger") || split.equalsIgnoreCase("mytrigger") || split.equalsIgnoreCase("trig") || split.equalsIgnoreCase("mytrig") ||
                split.equalsIgnoreCase("redtrig") || split.equalsIgnoreCase("redtrigger") || split.equalsIgnoreCase("zone") || split.equalsIgnoreCase("myzone") || split.equalsIgnoreCase("azone") ||
                split.equalsIgnoreCase("mzone") || split.equalsIgnoreCase("pzone") || split.equalsIgnoreCase("ezone") || split.equalsIgnoreCase("uzone");
    }

    // Sort function will create a pre-sorted list based on the players permission, and what the list should be sorted by.
    //
    // Accepted conditions are (player)(uzone,ezone,mzone,pzone,azone,zone,door,trigger,all)(listPlayer)(page#)
    //
    private void createList(CommandSender sender, int page, String Type, String listPlayer) {
        if (Type.equalsIgnoreCase("default")) {
            defaultListCompile(listPlayer);
            Collections.sort(listObjects, new TypeNameComparator());
        }
        else {
            namedListCompile(listPlayer, Type);
            Collections.sort(listObjects, new PlayerNameComparator());
        }
        outputList(sender, page);
    }

    private void defaultListCompile(String listPlayer) {
        List<?> gList = null;
        for(int i = 0; i < 5; i++) {
            if (i == 0)
                gList = Arrays.asList(dataWriter.allSSDoorsMap.values().toArray());
            else if (i == 1)
                gList = Arrays.asList(dataWriter.allHDoorsMap.values().toArray());
            else if (i == 2)
                gList = Arrays.asList(dataWriter.allTSDoorsMap.values().toArray());
            else if (i == 3)
                gList = Arrays.asList(dataWriter.allTriggersMap.values().toArray());
            else if (i == 4)
                gList = Arrays.asList(dataWriter.allZonesMap.values().toArray());

            generateList(listPlayer, gList);
        }
    }

    private void namedListCompile(String listPlayer, String Type) {
        List<?> gList = null;
        if (Type.equalsIgnoreCase("door"))
            gList = Arrays.asList(dataWriter.allSSDoorsMap.values().toArray());
        else if (Type.equalsIgnoreCase("hdoor"))
            gList = Arrays.asList(dataWriter.allHDoorsMap.values().toArray());
        else if (Type.equalsIgnoreCase("tsdoor") || Type.equalsIgnoreCase("twostatedoor") || Type.equalsIgnoreCase("twostate"))
            gList = Arrays.asList(dataWriter.allTSDoorsMap.values().toArray());
        else if (Type.equalsIgnoreCase("trigger") || Type.equalsIgnoreCase("trig"))
            gList = getTriggerList("TRIGGER");
        else if (Type.equalsIgnoreCase("mytrigger") || Type.equalsIgnoreCase("mytrig"))
            gList = getTriggerList("MYTRIGGER");
        else if (Type.equalsIgnoreCase("redtrig") || Type.equalsIgnoreCase("redtrigger"))
            gList = getTriggerList("REDTRIG");
        else if (Type.equalsIgnoreCase("zone")) {
            gList = getZoneList("ZONE");
        }
        else if (Type.equalsIgnoreCase("myzone")) {
            gList = getZoneList("MYZONE");
        }
        else if (Type.equalsIgnoreCase("ezone")) {
            gList = getZoneList("EZONE");
        }
        else if (Type.equalsIgnoreCase("azone")) {
            gList = getZoneList("AZONE");
        }
        else if (Type.equalsIgnoreCase("pzone")) {
            gList = getZoneList("PZONE");
        }
        else if (Type.equalsIgnoreCase("mzone")) {
            gList = getZoneList("MZONE");
        }
        else if (Type.equalsIgnoreCase("uzone")) {
            gList = getZoneList("UZONE");
        }
        else {
            return;
        }

        generateList(listPlayer, gList);
    }

    private List<Zone> getZoneList(String zoneType) {
        List<Zone> temp = new ArrayList<Zone>();
        if (!zoneType.equals("")) {
            for(Zone az : dataWriter.allZonesMap.values()) {
                if (az.zone_Type.equals(zoneType))
                    temp.add(az);
            }
            return temp;
        }
        else
            return null;
    }

    private List<Trigger> getTriggerList(String triggerType) {
        List<Trigger> temp = new ArrayList<Trigger>();
        if (!triggerType.equals("")) {
            for(Trigger at : dataWriter.allTriggersMap.values()) {
                if (at.trigger_Type.equals(triggerType))
                    temp.add(at);
            }
            return temp;
        }
        else
            return null;
    }

    private void generateList(String listPlayer, List<?> gList) {
        for(Object o : gList) {
            if (listPlayer == null) {
                DLister l = new DLister(o.toString());
                listObjects.add(l);
            }
            else {
                DLister l = new DLister(o.toString());
                if (l.playerName.equalsIgnoreCase(listPlayer))
                    listObjects.add(l);
            }
        }
    }

    // Listing with pagination
    private void outputList(CommandSender sender, int page) {
        double lastPage = listObjects.size();
        int pageSize = 0;
        if (sender instanceof Player) // If player is the sender only list 9 lines else use 15 lines.
            pageSize = 9;
        else
            pageSize = 15;
        lastPage = Math.ceil(lastPage / pageSize);
        int startpage = (page - 1) * pageSize;
        if (listObjects.isEmpty()) {
            sender.sendMessage("No search results");
            listObjects.clear();
            listObjects.trimToSize();
        }
        else {
            if (page > lastPage) {
                sender.sendMessage(ChatColor.RED + "NO SUCH PAGE");
            }
            else {
                sender.sendMessage(ChatColor.YELLOW + "------------------- Page " + page + "/" + ((int) lastPage) + " -------------------");
                int index = 0;
                int currentCount = 0;
                int displayedCount = 0;
                while (index < listObjects.size() && displayedCount < pageSize) {
                    if (currentCount >= startpage) {
                        if (!listObjects.get(index).typeName.equalsIgnoreCase("uzone"))
                            sender.sendMessage(ChatColor.WHITE + "T=[" + ChatColor.AQUA + listObjects.get(index).typeName + ChatColor.WHITE + "] N=[" + ChatColor.AQUA + listObjects.get(index).objectName + ChatColor.WHITE + "] P=[" + ChatColor.AQUA + listObjects.get(index).playerName + ChatColor.WHITE + "] W=[" + ChatColor.AQUA + listObjects.get(index).worldName + ChatColor.WHITE + "]");
                        else
                            sender.sendMessage(ChatColor.WHITE + "T=[" + ChatColor.AQUA + listObjects.get(index).typeName + ChatColor.WHITE + "] N=[" + ChatColor.AQUA + listObjects.get(index).objectName + ChatColor.WHITE + "] P=[" + ChatColor.AQUA + listObjects.get(index).playerName + ChatColor.WHITE + "] W=[" + ChatColor.AQUA + listObjects.get(index).worldName + ChatColor.WHITE + "] TGR=[" + ChatColor.AQUA + listObjects.get(index).trigger + ChatColor.WHITE + "]");
                        displayedCount++;
                    }
                    else {
                        currentCount++;
                    }
                    index++;
                }
            }
            listObjects.clear();
            listObjects.trimToSize();
        }
    }
}