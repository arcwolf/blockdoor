package plugin.arcwolf.blockdoor.Doors;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.World;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;
import plugin.arcwolf.blockdoor.Utils.CustomLocation;

public class HDoor extends Door {

    public Map<CustomLocation, DoorState> hDoorContents = new HashMap<CustomLocation, DoorState>();
    public Map<HDoorOverlaps, HDoorOverlaps> hdoorOverlaps = new HashMap<HDoorOverlaps, HDoorOverlaps>();

    public HDoor(String in_name, String in_creator, String in_world) {
        super(in_name, in_creator, in_world, DoorTypes.HYBRIDSTATE);

        overlapAmount = 0;
    }

    public HDoor(String in_string) {
        super(in_string);
        String[] split = in_string.split(":");
        door_type = DoorTypes.HYBRIDSTATE;
        if (split.length == 9) {

            door_creator = split[1];
            door_name = split[2];
            door_world = split[3];
            world = BlockDoor.plugin.getWorld(door_world);
            coordsSet = Boolean.parseBoolean(split[4]);
            isOpen = Boolean.parseBoolean(split[5]);

            overlapAmount = Integer.parseInt(split[6]);

            if (overlapAmount != 0) {
                String[] overlapSplit = split[7].split("\\|");
                HDoorOverlaps ol;
                for(int i = 0; i < overlapSplit.length; i++) {
                    ol = new HDoorOverlaps(overlapSplit[i]);
                    hdoorOverlaps.put(ol, ol);
                }
            }

            String[] doorBlocks = split[8].split("\\|");
            DoorState ds;
            for(int i = 0; i < doorBlocks.length; i++) {
                ds = new DoorState(doorBlocks[i]);
                hDoorContents.put(new CustomLocation(door_world, ds.x, ds.y, ds.z), ds);
            }
        }
        else {
            door_creator = "FAILED";
            coordsSet = false;
        }
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("HDOOR:");
        builder.append(door_creator);
        builder.append(":");
        builder.append(door_name);
        builder.append(":");

        builder.append(door_world);
        builder.append(":");

        builder.append(Boolean.toString(coordsSet));
        builder.append(":");

        builder.append(Boolean.toString(isOpen));
        builder.append(":");

        builder.append(Integer.toString(overlapAmount));
        builder.append(":");

        for(HDoorOverlaps ol : hdoorOverlaps.values()) {
            builder.append(ol.blocksOverlaped);
            builder.append(",");
            builder.append(ol.creator);
            builder.append(",");
            builder.append(ol.name);
            builder.append(",");
            builder.append(ol.world);
            builder.append("|");
        }
        builder.append(":");

        for(Entry<CustomLocation, DoorState> hdb : hDoorContents.entrySet()) {
            builder.append(hdb.getValue().blockID);
            builder.append(",");
            builder.append(String.valueOf(hdb.getValue().offset));
            builder.append(",");
            builder.append(hdb.getValue().x);
            builder.append(",");
            builder.append(hdb.getValue().y);
            builder.append(",");
            builder.append(hdb.getValue().z);
            builder.append("|");
        }

        return builder.toString();
    }

    // Tests for overlap with provided coordinates.
    public boolean doorOverlap(int x, int y, int z) {
        return hDoorContents.containsKey(new CustomLocation(door_world, x, y, z));
    }

    public void toggle() {
        if (isOpen) {
            isOpen = false;
            close();
        }
        else {
            isOpen = true;
            open();
        }
    }

    public void open() {
        isOpen = true;
        BlockDoor.plugin.datawriter.doorPhysics.put(this, true);
        for(Entry<CustomLocation, DoorState> hdb : hDoorContents.entrySet()) {
            // Would prevent one door from erasing an overlapping hdoor
            //if (world.getBlockAt(hdc.x, hdc.y, hdc.z).getTypeId() == hdc.blockID && world.getBlockAt(hdc.x, hdc.y, hdc.z).getData() == hdc.offset) // I dont know if I like this idea here
            updateBlock(world, hdb.getValue().x, hdb.getValue().y, hdb.getValue().z, 0, true);
        }
        BlockDoor.plugin.datawriter.doorPhysics.remove(this);
    }

    public void close() {
        isOpen = false;
        BlockDoor.plugin.datawriter.doorPhysics.put(this, true);
        for(Entry<CustomLocation, DoorState> hdb : hDoorContents.entrySet()) {
            updateBlock(world, hdb.getValue().x, hdb.getValue().y, hdb.getValue().z, hdb.getValue().blockID, hdb.getValue().offset, true);
            updateBlockMap(world, hdb.getValue().x, hdb.getValue().y, hdb.getValue().z, hdb.getValue().blockID, hdb.getValue().offset);
        }
        BlockDoor.plugin.datawriter.doorPhysics.remove(this);
    }

    private void updateBlockMap(World world, int x, int y, int z, int blockID, int offset) {
        BlockDoor.plugin.datawriter.blockMap.put(world.getBlockAt(x, y, z), this);
    }
}
