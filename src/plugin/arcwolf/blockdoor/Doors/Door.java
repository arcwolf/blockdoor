package plugin.arcwolf.blockdoor.Doors;

import org.bukkit.World;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;

public abstract class Door {

    public int overlapAmount;

    public String door_name = "";
    public String door_creator = "";
    public boolean coordsSet;

    public boolean isOpen;
    public String door_world = "";

    public World world;

    public int door_start_x, door_start_y, door_start_z;
    public int door_end_x, door_end_y, door_end_z;
    public DoorTypes door_type;

    protected Door(String in_name, String in_creator, String in_world, DoorTypes in_type) {
        door_name = in_name;
        door_creator = in_creator;

        door_start_x = 0;
        door_start_y = 0;
        door_start_z = 0;

        door_end_x = 0;
        door_end_y = 0;
        door_end_z = 0;

        coordsSet = false;

        isOpen = false;

        door_world = in_world;
        world = BlockDoor.plugin.getWorld(door_world);

        door_type = in_type;
    }

    protected Door(String in_string) {
        // Personal note:
        // This does nothing because I like the logic here split out into the sub classes but I also want to require this constructor for sub classes.
        // Also, its Easier for me to read with logic split up.
    }

    protected final void updateCoordinates(BlockDoorSettings settings) {
        door_start_x = settings.startX;
        door_start_y = settings.startY;
        door_start_z = settings.startZ;
        door_end_x = settings.endX;
        door_end_y = settings.endY;
        door_end_z = settings.endZ;
    }

    public abstract void open();

    public abstract void close();

    public abstract void toggle();

    public abstract boolean doorOverlap(int x, int y, int z);

    public final void updateBlock(World world, int x, int y, int z, int blockId, boolean physicsUpdate) {
        updateBlock(world, x, y, z, blockId, (byte) 0, physicsUpdate);
    }

    public final void updateBlock(World world, int x, int y, int z, int blockId, int blockData, boolean physicsUpdate) {
        if (!world.getBlockAt(x, y, z).getChunk().isLoaded() || world.getPlayers().size() == 0) return;
        if (world.getBlockAt(x, y, z).getTypeId() != blockId || world.getBlockAt(x, y, z).getData() != blockData) {
            world.getBlockAt(x, y, z).setTypeId(blockId, physicsUpdate);
            world.getBlockAt(x, y, z).setData((byte) blockData, physicsUpdate);
        }
        BlockDoor.plugin.datawriter.blockMap.put(world.getBlockAt(x, y, z), this);
    }

    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((door_creator == null) ? 0 : door_creator.hashCode());
        result = prime * result + ((door_name == null) ? 0 : door_name.hashCode());
        result = prime * result + ((door_type == null) ? 0 : door_type.hashCode());
        result = prime * result + ((door_world == null) ? 0 : door_world.hashCode());
        return result;
    }

    public final boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Door other = (Door) obj;
        if (door_creator == null) {
            if (other.door_creator != null) return false;
        }
        else if (!door_creator.equals(other.door_creator)) return false;
        if (door_name == null) {
            if (other.door_name != null) return false;
        }
        else if (!door_name.equals(other.door_name)) return false;
        if (door_type != other.door_type) return false;
        if (door_world == null) {
            if (other.door_world != null) return false;
        }
        else if (!door_world.equals(other.door_world)) return false;
        return true;
    }
}
