package plugin.arcwolf.blockdoor.Doors;

import java.util.HashMap;
import java.util.Map;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;

public class SingleStateDoor extends Door {

    public String fill;
    public String empty;

    public Map<DoorOverlaps, DoorOverlaps> doorOverlaps = new HashMap<DoorOverlaps, DoorOverlaps>();

    public SingleStateDoor(String in_name, String in_creator, String in_world) {
        super(in_name, in_creator, in_world, DoorTypes.ONESTATE);
        fill = "1,0";
        empty = "0,0";
    }

    public SingleStateDoor(String in_string) {
        super(in_string);
        String[] split = in_string.split(":");
        door_type = DoorTypes.ONESTATE;
        if (split.length == 14) {

            door_creator = split[1];
            door_name = split[2];

            door_start_x = Integer.parseInt(split[3]);
            door_start_y = Integer.parseInt(split[4]);
            door_start_z = Integer.parseInt(split[5]);

            door_end_x = Integer.parseInt(split[6]);
            door_end_y = Integer.parseInt(split[7]);
            door_end_z = Integer.parseInt(split[8]);
            coordsSet = Boolean.parseBoolean(split[9]);
            if (!(split[10].contains(","))) {
                split[10] = split[10] + ",0";
            }
            fill = split[10];
            if (!(split[11].contains(","))) {
                split[11] = split[11] + ",0";
            }
            empty = split[11];

            isOpen = Boolean.parseBoolean(split[12]);
            door_world = split[13];
            world = BlockDoor.plugin.getWorld(door_world);
        }
        else if (split.length > 14) {

            door_creator = split[1];
            door_name = split[2];

            door_start_x = Integer.parseInt(split[3]);
            door_start_y = Integer.parseInt(split[4]);
            door_start_z = Integer.parseInt(split[5]);

            door_end_x = Integer.parseInt(split[6]);
            door_end_y = Integer.parseInt(split[7]);
            door_end_z = Integer.parseInt(split[8]);
            coordsSet = Boolean.parseBoolean(split[9]);
            if (!(split[10].contains(","))) {
                split[10] = split[10] + ",0";
            }
            fill = split[10];
            if (!(split[11].contains(","))) {
                split[11] = split[11] + ",0";
            }
            empty = split[11];

            isOpen = Boolean.parseBoolean(split[12]);
            door_world = split[13];
            world = BlockDoor.plugin.getWorld(door_world);

            overlapAmount = Integer.parseInt(split[14]);
            if (overlapAmount != 0) {
                String[] overlapSplit = split[15].split("\\|");
                DoorOverlaps ol;
                for(int i = 0; i < overlapSplit.length; i++) {
                    ol = new DoorOverlaps(overlapSplit[i]);
                    doorOverlaps.put(ol, ol);
                }
            }
        }
        else {
            door_creator = "FAILED";
            coordsSet = false;
        }
    }

    // Tests for overlap with provided coordinates.
    public boolean doorOverlap(int x, int y, int z) {
        return (door_start_x <= x && x <= door_end_x &&
                door_start_y <= y && y <= door_end_y &&
                door_start_z <= z && z <= door_end_z);
    }

    public void toggle() {
        if (isOpen) {
            close();
        }
        else {
            open();
        }
    }

    public void open() {
        String newEmpty = "";
        String newEmpty_off = "";
        String[] split2 = empty.split(",");
        newEmpty = split2[0];
        newEmpty_off = split2[1];
        isOpen = true;
        for(int i = door_start_x; i <= door_end_x; i++)
            for(int j = door_start_y; j <= door_end_y; j++)
                for(int k = door_start_z; k <= door_end_z; k++) {
                    updateBlock(world, i, j, k, Integer.parseInt(newEmpty), Integer.parseInt(newEmpty_off), true);
                }
    }

    public void close() {
        String newFill = "";
        String newFill_off = "";
        String[] split2 = fill.split(",");
        newFill = split2[0];
        newFill_off = split2[1];
        isOpen = false;
        for(int i = door_start_x; i <= door_end_x; i++)
            for(int j = door_start_y; j <= door_end_y; j++)
                for(int k = door_start_z; k <= door_end_z; k++) {
                    updateBlock(world, i, j, k, Integer.parseInt(newFill), Integer.parseInt(newFill_off), true);
                }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DOOR:");
        builder.append(door_creator);
        builder.append(":");
        builder.append(door_name);
        builder.append(":");

        builder.append(Integer.toString(door_start_x));
        builder.append(":");
        builder.append(Integer.toString(door_start_y));
        builder.append(":");
        builder.append(Integer.toString(door_start_z));
        builder.append(":");

        builder.append(Integer.toString(door_end_x));
        builder.append(":");
        builder.append(Integer.toString(door_end_y));
        builder.append(":");
        builder.append(Integer.toString(door_end_z));
        builder.append(":");

        builder.append(Boolean.toString(coordsSet));
        builder.append(":");

        builder.append(fill);
        builder.append(":");

        builder.append(empty);
        builder.append(":");

        builder.append(Boolean.toString(isOpen));
        builder.append(":");

        builder.append(door_world);
        builder.append(":");

        builder.append(Integer.toString(overlapAmount));
        builder.append(":");

        for(DoorOverlaps ol : doorOverlaps.values()) {
            builder.append(ol.creator);
            builder.append(",");
            builder.append(ol.name);
            builder.append(",");
            builder.append(ol.world);
            builder.append("|");
        }

        return builder.toString();
    }
}