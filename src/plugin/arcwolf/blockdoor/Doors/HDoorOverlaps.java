package plugin.arcwolf.blockdoor.Doors;

/* Used to keep track of overlaps. If an overlap is listed in here it can be tested for the number
 * of blocks in that door overlaps.
 */

public class HDoorOverlaps extends DoorOverlaps{

    public int blocksOverlaped = 0;

    public HDoorOverlaps(HDoor hd) {
        super(hd);
        blocksOverlaped = hd.hdoorOverlaps.size();
    }

    public HDoorOverlaps(int in_numberOverlaps, HDoor hd) {
        super(hd);
        blocksOverlaped = in_numberOverlaps;
    }

    public HDoorOverlaps(String in_string) {
        super(in_string);
        String[] split = in_string.split(",");
        if (split.length == 4) {
            blocksOverlaped = Integer.parseInt(split[0]);
        }
    }
}