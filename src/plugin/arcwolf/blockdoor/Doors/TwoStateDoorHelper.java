package plugin.arcwolf.blockdoor.Doors;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.Locks;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.CustomLocation;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Utils.SpecialCaseCheck;

public class TwoStateDoorHelper {

    private BlockDoor plugin;
    private DataWriter dataWriter;
    private Map<TwoStateDoor, TwoStateDoor> allTSDoorsMap;

    public TwoStateDoorHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
        allTSDoorsMap = dataWriter.allTSDoorsMap;
    }

    //Returns a Door for the player, making a new one if it has to
    public TwoStateDoor findOrCreateDoor(String in_name, String in_creator, String in_world, Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        TwoStateDoor tsd = new TwoStateDoor(in_name, in_creator, in_world, "UNLOCKED");
        if (allTSDoorsMap.containsKey(tsd))
            return allTSDoorsMap.get(tsd);
        else {
            allTSDoorsMap.put(tsd, tsd);
            settings.notFound = 1;
            return tsd;
        }
    }

    //Returns a Door for the player
    public TwoStateDoor findDoor(String in_name, String in_creator, String in_world) {
        return (TwoStateDoor) allTSDoorsMap.get(new TwoStateDoor(in_name, in_creator, in_world, "UNLOCKED"));
    }

    public TwoStateDoor findDoor(DoorOverlaps ol) {
        return (TwoStateDoor) allTSDoorsMap.get(new TwoStateDoor(ol.name, ol.creator, ol.world, "UNLOCKED"));
    }

    //Lock all doors in database. Used for reload and first load function
    public void lock() {
        for(TwoStateDoor tsdoor : allTSDoorsMap.values()) {
            tsdoor.locks = Locks.LOCKED;
            tsdoor.overlapAmount = -(Math.abs(tsdoor.overlapAmount));
        }
    }

    //lock all doors based on the provided door
    public void lock(TwoStateDoor twoStateDoor) {
        twoStateDoor.locks = Locks.LOCKED;
        if (twoStateDoor.overlapAmount != 0) twoStateDoor.overlapAmount = -(Math.abs(twoStateDoor.overlapAmount));
        for(DoorOverlaps overlap : twoStateDoor.doorOverlaps.values()) {
            ((TwoStateDoor) overlap.d).locks = Locks.LOCKED;
            overlap.d.overlapAmount = -(Math.abs(overlap.d.overlapAmount));
        }
    }

    public void saveTwoState(TwoStateDoor twoStateDoor, Player player) {
        int start_x = twoStateDoor.door_start_x;
        int start_y = twoStateDoor.door_start_y;
        int start_z = twoStateDoor.door_start_z;
        int end_x = twoStateDoor.door_end_x;
        int end_y = twoStateDoor.door_end_y;
        int end_z = twoStateDoor.door_end_z;
        boolean isOpen = twoStateDoor.isOpen;
        if (!isOpen) {
            twoStateDoor.closedState.clear();
            twoStateDoor.closedStateIndex.clear();
        }
        else {
            twoStateDoor.openState.clear();
            twoStateDoor.openStateIndex.clear();
        }
        World world = player.getWorld();

        int invalids = 0;
        int blockID = 0;
        int index = 0;
        for(int x = start_x; x <= end_x; x++)
            for(int y = start_y; y <= end_y; y++)
                for(int z = start_z; z <= end_z; z++) {
                    CustomLocation cloc = new CustomLocation(world.getName(), x, y, z);
                    Block block = world.getBlockAt(x, y, z);
                    blockID = block.getTypeId();
                    dataWriter.blockMap.put(block, twoStateDoor);
                    if (dataWriter.isEnableSpecialBlocks()) {
                        if (!SpecialCaseCheck.isInvalidBlock(blockID)) {
                            if (!isOpen)
                                twoStateDoor.addClosedBlock(cloc, new DoorState(block), index);
                            else
                                twoStateDoor.addOpenBlock(cloc, new DoorState(block), index);
                        }
                        else {
                            if (!isOpen)
                                twoStateDoor.addClosedBlock(cloc, new DoorState(world, x, y, z, 0, (byte) 0), index);
                            else
                                twoStateDoor.addOpenBlock(cloc, new DoorState(world, x, y, z, 0, (byte) 0), index);
                            invalids++;
                        }
                    }
                    else {
                        if (!SpecialCaseCheck.isInvalidBlock(blockID) && !SpecialCaseCheck.isSpecialBlock(blockID)) {
                            if (!isOpen)
                                twoStateDoor.addClosedBlock(cloc, new DoorState(block), index);
                            else
                                twoStateDoor.addOpenBlock(cloc, new DoorState(block), index);
                        }
                        else {
                            if (!isOpen)
                                twoStateDoor.addClosedBlock(cloc, new DoorState(world, x, y, z, 0, (byte) 0), index);
                            else
                                twoStateDoor.addOpenBlock(cloc, new DoorState(world, x, y, z, 0, (byte) 0), index);
                            invalids++;
                        }
                    }
                    index++;
                }
        dataWriter.saveDatabase(false);
        if (invalids != 0) {
            player.sendMessage(ChatColor.GOLD + "" + invalids + ChatColor.RED + " invalid blocks detected inside door area.");
            player.sendMessage(ChatColor.RED + "They were not saved.");
        }
        BlockDoorSettings.clearSettings(player);
    }

    public void deleteOverlaps(TwoStateDoor twoStateDoor) {
        for(DoorOverlaps overlap : twoStateDoor.doorOverlaps.values()) {
            for(DoorOverlaps o : ((TwoStateDoor) overlap.d).doorOverlaps.values()) {
                if (o.d.equals(twoStateDoor)) {
                    ((TwoStateDoor) overlap.d).doorOverlaps.remove(o);
                    if (overlap.d.overlapAmount >= 0 && twoStateDoor.locks != Locks.UNLOCKED)
                        overlap.d.overlapAmount = ((TwoStateDoor) overlap.d).doorOverlaps.size();
                    else
                        overlap.d.overlapAmount = -(((TwoStateDoor) overlap.d).doorOverlaps.size());
                    break;
                }
            }
        }
    }

    public void createDoor(BlockDoorSettings settings, Player player, Block blockClicked) {
        int blockX = (int) blockClicked.getLocation().getX();
        int blockY = (int) blockClicked.getLocation().getY();
        int blockZ = (int) blockClicked.getLocation().getZ();
        TwoStateDoor tsd = findOrCreateDoor(settings.name, player.getName(), player.getWorld().getName(), player);

        if (settings.select == 1) {
            settings.door = tsd;
            player.sendMessage("Setting door start: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
            settings.startX = blockX;
            settings.startY = blockY;
            settings.startZ = blockZ;

            settings.select = 2;
        }
        else if (settings.select == 2) {
            player.sendMessage("Setting door end: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
            //Checking to make sure start > end to make 'for loops' easier later
            if (settings.startX > blockX) {
                settings.endX = settings.startX;
                settings.startX = blockX;
            }
            else {
                settings.endX = blockX;
            }

            if (settings.startY > blockY) {
                settings.endY = settings.startY;
                settings.startY = blockY;
            }
            else {
                settings.endY = blockY;
            }

            if (settings.startZ > blockZ) {
                settings.endZ = settings.startZ;
                settings.startZ = blockZ;
            }
            else {
                settings.endZ = blockZ;
            }

            int xDist = Math.abs(settings.endX - settings.startX);
            int yDist = Math.abs(settings.endY - settings.startY);
            int zDist = Math.abs(settings.endZ - settings.startZ);

            if (dataWriter.getMax_TwoStateSize() != -1 && ((xDist + 1 > dataWriter.getMax_TwoStateSize()) || (yDist + 1 > dataWriter.getMax_TwoStateSize()) || (zDist + 1 > dataWriter.getMax_TwoStateSize()))) {
                player.sendMessage(ChatColor.RED + "TWO STATE DOOR DIMENSIONS REJECTED for: '" + ChatColor.WHITE + settings.name + ChatColor.RED + "'");
                if (tsd.coordsSet) {
                    if (!tsd.isLocked()) lock(tsd);
                }
                else {
                    allTSDoorsMap.remove(tsd);
                }
                BlockDoorSettings.clearSettings(player);
            }
            else {
                if (tsd.coordsSet) {
                    dataWriter.updateOldTSDBlockMap(tsd); // Update old blockmap records
                }
                boolean found = false;
                int index = 0;
                int invalids = 0;
                int blockID;
                TwoStateDoor tempDoor = null;
                World world = plugin.getWorld(tsd.door_world);
                Map<Block, Object> tempBlockMap = new HashMap<Block, Object>();
                HashMap<DoorOverlaps, DoorOverlaps> tempOverlaps = new HashMap<DoorOverlaps, DoorOverlaps>();
                LinkedHashMap<CustomLocation, DoorState> closedState = new LinkedHashMap<CustomLocation, DoorState>();
                Map<Integer, CustomLocation> closedStateIndex = new HashMap<Integer, CustomLocation>();
                LinkedHashMap<CustomLocation, DoorState> openedState = new LinkedHashMap<CustomLocation, DoorState>();
                Map<Integer, CustomLocation> openedStateIndex = new HashMap<Integer, CustomLocation>();

                // Test door and update maps
                search:
                for(int x = settings.startX; x <= settings.endX; x++)
                    for(int z = settings.startZ; z <= settings.endZ; z++)
                        for(int y = settings.startY; y <= settings.endY; y++) {
                            Block block = tsd.world.getBlockAt(x, y, z);
                            Object obj = dataWriter.blockMap.get(block);

                            blockID = block.getTypeId();
                            CustomLocation cloc = new CustomLocation(world.getName(), x, y, z);
                            DoorState closed = new DoorState(block);
                            DoorState open = new DoorState(world, x, y, z, 0, (byte) 0);
                            if (dataWriter.isEnableSpecialBlocks()) {
                                if (!SpecialCaseCheck.isInvalidBlock(blockID)) {
                                    closedState.put(cloc, closed);
                                    closedStateIndex.put(index, cloc);
                                    openedState.put(cloc, open);
                                    openedStateIndex.put(index, cloc);
                                    index++;
                                }
                                else {
                                    closedState.put(cloc, open); // Use open state to set block to air because invalid
                                    closedStateIndex.put(index, cloc);
                                    openedState.put(cloc, open);
                                    openedStateIndex.put(index, cloc);
                                    index++;
                                    invalids++;
                                }
                            }
                            else {
                                if (!SpecialCaseCheck.isSpecialBlock(blockID) && !SpecialCaseCheck.isInvalidBlock(blockID)) {
                                    closedState.put(cloc, closed);
                                    closedStateIndex.put(index, cloc);
                                    openedState.put(cloc, open);
                                    openedStateIndex.put(index, cloc);
                                    index++;
                                }
                                else {
                                    closedState.put(cloc, open); // Use open state to set block to air because invalid
                                    closedStateIndex.put(index, cloc);
                                    openedState.put(cloc, open);
                                    openedStateIndex.put(index, cloc);
                                    index++;
                                    invalids++;
                                }
                            }

                            if (obj == null) {
                                tempBlockMap.put(block, tsd);
                                continue;
                            }
                            else if (obj instanceof TwoStateDoor) {
                                TwoStateDoor testtsd = (TwoStateDoor) obj;
                                if (!testtsd.equals(tsd)) { // Should never be false
                                    if (!dataWriter.isOverlapTwoStateDoors() || !testtsd.door_creator.equals(tsd.door_creator)) {
                                        tempDoor = testtsd;
                                        found = true;
                                        break search;
                                    }
                                    else {
                                        for(TwoStateDoor d : dataWriter.allTSDoorsMap.values()) {
                                            if (!d.equals(tsd) && d.doorOverlap(x, y, z)) {
                                                DoorOverlaps tsdo = new DoorOverlaps(d);
                                                tempOverlaps.put(tsdo, tsdo);
                                            }
                                        }
                                        tempBlockMap.put(block, tsd);
                                    }
                                }
                            }
                            else if (obj instanceof SingleStateDoor) {
                                SingleStateDoor tsdoor = (SingleStateDoor) obj;
                                if (tsdoor.doorOverlap(x, y, z)) {
                                    player.sendMessage("Overlap of Door [" + ChatColor.GREEN + tsdoor.door_name + ChatColor.WHITE + "] Created by [" +
                                                    ChatColor.GREEN + tsdoor.door_creator + ChatColor.WHITE + "]");
                                    player.sendMessage("Starting at [X= " + ChatColor.AQUA + tsdoor.door_start_x + ChatColor.WHITE + " Y= " +
                                                    ChatColor.AQUA + tsdoor.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                                    tsdoor.door_start_z + ChatColor.WHITE + "]");
                                    player.sendMessage("Ending at [X= " + ChatColor.AQUA + tsdoor.door_end_x + ChatColor.WHITE + " Y= " +
                                                    ChatColor.AQUA + tsdoor.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                                    tsdoor.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                                                    tsdoor.door_world + ChatColor.WHITE + "]");
                                    found = true;
                                    break search;
                                }
                            }
                            else if (obj instanceof HDoor) {
                                HDoor hd = (HDoor) obj;
                                if (hd.doorOverlap(x, y, z)) {
                                    player.sendMessage("Overlap of Hybrid Door [" + ChatColor.GREEN + hd.door_name + ChatColor.WHITE + "] Created by [" +
                                                    ChatColor.GREEN + hd.door_creator + ChatColor.WHITE + "]");
                                    player.sendMessage("in world [" + ChatColor.AQUA + hd.door_world + ChatColor.WHITE + "]");
                                    found = true;
                                    break search;
                                }
                            }
                            else if (obj instanceof Trigger) {
                                Trigger t = (Trigger) obj;
                                plugin.triggerhelper.showOverlapMessage(t, player);
                                found = true;
                                break search;
                            }
                        }
                if (found) {
                    if (tempDoor != null) {
                        player.sendMessage("Overlap of Door [" + ChatColor.GREEN + tempDoor.door_name + ChatColor.WHITE + "] Created by [" +
                                ChatColor.GREEN + tempDoor.door_creator + ChatColor.WHITE + "]");
                        player.sendMessage("Starting at [X= " + ChatColor.AQUA + tempDoor.door_start_x + ChatColor.WHITE + " Y= " +
                                ChatColor.AQUA + tempDoor.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                tempDoor.door_start_z + ChatColor.WHITE + "]");
                        player.sendMessage("Ending at [X= " + ChatColor.AQUA + tempDoor.door_end_x + ChatColor.WHITE + " Y= " +
                                ChatColor.AQUA + tempDoor.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                tempDoor.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                                tempDoor.door_world + ChatColor.WHITE + "]");
                    }
                    player.sendMessage(ChatColor.RED + "DOOR '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' REJECTED");
                    if (tsd.coordsSet) {
                        if (!tsd.isLocked()) lock(tsd);
                    }
                    else {
                        allTSDoorsMap.remove(tsd);
                    }
                    BlockDoorSettings.clearSettings(player);
                }
                else {
                    // Update door Coordinates
                    tsd.updateCoordinates(settings);
                    // Set door contents
                    tsd.closedState = closedState;
                    tsd.closedStateIndex = closedStateIndex;
                    tsd.openState = openedState;
                    tsd.openStateIndex = openedStateIndex;

                    // Remove old overlaps
                    for(DoorOverlaps ol : tsd.doorOverlaps.values()) {
                        ((TwoStateDoor)ol.d).doorOverlaps.remove(new DoorOverlaps(tsd));
                        ol.d.overlapAmount = ((TwoStateDoor)ol.d).doorOverlaps.size();
                    }

                    // Update door overlaps
                    tsd.doorOverlaps = tempOverlaps;
                    tsd.overlapAmount = tempOverlaps.size();

                    // Update new overlaps of doors
                    for(DoorOverlaps ol : tsd.doorOverlaps.values()) {
                        TwoStateDoor oltsd = (TwoStateDoor) ol.d;
                        DoorOverlaps o = new DoorOverlaps(tsd);
                        oltsd.doorOverlaps.put(o, o);
                        oltsd.overlapAmount = oltsd.doorOverlaps.size();
                    }

                    // Update blockmap
                    dataWriter.blockMap.putAll(tempBlockMap);

                    // Cleanup and save
                    BlockDoorSettings.clearSettings(player);
                    tsd.coordsSet = true;
                    tsd.softLock();
                    dataWriter.saveDatabase(false);

                    // Notify of invalids
                    if (invalids != 0) {
                        player.sendMessage(ChatColor.GOLD + "" + invalids + ChatColor.RED + " invalid blocks detected inside door area.");
                        player.sendMessage(ChatColor.RED + "They were not saved.");
                    }
                }
            }
        }
    }

    public boolean addBlock(Block block, Player p) {
        int blockX = block.getLocation().getBlockX();
        int blockY = block.getLocation().getBlockY();
        int blockZ = block.getLocation().getBlockZ();
        int blockID = block.getTypeId();
        byte blockOff = block.getData();
        Object obj = dataWriter.blockMap.get(block);
        if (obj == null) return false;
        TwoStateDoor twostate = null;
        if (obj instanceof TwoStateDoor) {
            twostate = (TwoStateDoor) obj;
            CustomLocation cloc = new CustomLocation(twostate.door_world, blockX, blockY, blockZ);
            updateDoor(cloc, twostate, blockID, blockOff);
        }
        return false;
    }

    public void removeBlock(BlockBreakEvent event, BlockDoorSettings settings) {
        //Two State Door listener
        Object obj = dataWriter.blockMap.get(event.getBlock());
        if (obj == null) return;
        TwoStateDoor twostate = null;
        if (obj instanceof TwoStateDoor) twostate = (TwoStateDoor) obj;
        if (twostate == null) return;

        int x_Pos = event.getBlock().getLocation().getBlockX();
        int y_Pos = event.getBlock().getLocation().getBlockY();
        int z_Pos = event.getBlock().getLocation().getBlockZ();
        String player = event.getPlayer().getName();
        if (twostate.locks.equals(Locks.UNLOCKED)) {
            if (player.equals(twostate.door_creator) || (plugin.playerHasPermission(event.getPlayer(), "blockdoor.admin.twostate"))) {
                CustomLocation cloc = new CustomLocation(twostate.door_world, x_Pos, y_Pos, z_Pos);
                updateDoor(cloc, twostate, 0, (byte) 0);
            }
            else
                event.getPlayer().sendMessage("Only " + twostate.door_creator + " may edit this door.");
        }
        else {
            event.getPlayer().sendMessage("BlockDoor:" + ChatColor.RED + " Canceled, All doors in this area are locked.");
            event.setCancelled(true);
        }
        //end two state door listener
    }

    public void updateDoor(CustomLocation cloc, TwoStateDoor twostate, int blockID, byte blockOff) {
        if (!twostate.isOpen) {
            DoorState ds = twostate.closedState.get(cloc);
            if (ds == null) return;
            ds.blockID = blockID;
            ds.offset = blockOff;
            if (SpecialCaseCheck.isDoor(blockID)) {
                cloc = new CustomLocation(twostate.door_world, cloc.getX(), cloc.getY() + 1, cloc.getZ());
                ds = twostate.closedState.get(cloc);
                if (ds != null) {
                    int bof = blockOff;
                    bof += 8;
                    ds.blockID = blockID;
                    ds.offset = (byte) bof;
                }
            }
        }
        else {
            DoorState ds = twostate.openState.get(cloc);
            if (ds == null) return;
            ds.blockID = blockID;
            ds.offset = blockOff;
            if (SpecialCaseCheck.isDoor(blockID)) {
                cloc = new CustomLocation(twostate.door_world, cloc.getX(), cloc.getY() + 1, cloc.getZ());
                ds = twostate.openState.get(cloc);
                if (ds != null) {
                    int bof = blockOff;
                    bof += 8;
                    ds.blockID = blockID;
                    ds.offset = (byte) bof;
                }
            }
        }
    }
}