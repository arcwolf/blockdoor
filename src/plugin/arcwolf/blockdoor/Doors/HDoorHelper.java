package plugin.arcwolf.blockdoor.Doors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.CustomLocation;
import plugin.arcwolf.blockdoor.Utils.DataWriter;

public class HDoorHelper {

    private BlockDoor plugin;
    private Map<HDoor, HDoor> allHDoorsMap;
    private DataWriter dataWriter;

    public HDoorHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
        allHDoorsMap = dataWriter.allHDoorsMap;
    }

    /**
     * Either finds a pre-existing door for the player or makes a new one if no
     * door is found.
     * 
     * @param in_name
     * @param in_creator
     * @param in_world
     * @param player
     * @return A door object
     */
    public HDoor findOrCreateDoor(String in_name, String in_creator, String in_world, Player player) {
        HDoor d = new HDoor(in_name, in_creator, in_world);
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        if (allHDoorsMap.containsKey(d))
            d = allHDoorsMap.get(d);
        else
            settings.notFound = 1;
        allHDoorsMap.put(d, d);
        openAllOverlapping(d);
        return d;
    }

    /**
     * Finds a door object record
     * 
     * @param in_name
     * @param in_creator
     * @param in_world
     * @return A door object or null if no door found
     */
    public HDoor findDoor(String in_name, String in_creator, String in_world) {
        return (HDoor) allHDoorsMap.get(new HDoor(in_name, in_creator, in_world));
    }

    public HDoor findDoor(HDoorOverlaps hdl) {
        return findDoor(hdl.name, hdl.creator, hdl.world);
    }

    /**
     * Finds a door based on given coordinates and door
     * 
     * @param x
     * @param y
     * @param z
     * @param hdoor
     * @return A Hybrid Door Block record or null if no record found
     */
    public DoorState findCoordinates(int x, int y, int z, HDoor hdoor) {
        if (hdoor != null) {
            CustomLocation cLoc = new CustomLocation(hdoor.door_world, x, y, z);
            return hdoor.hDoorContents.get(cLoc);
        }
        return null;
    }

    /**
     * @param hdoor
     * @return closed Door or null if none found
     */
    public HDoor findClosedDoor(HDoor hdoor) {
        for(HDoorOverlaps ol : hdoor.hdoorOverlaps.values()) {
            HDoor test = (HDoor) ol.d; // dataWriter.allHDoorsMap.get(new HDoor(ol.name, ol.creator, ol.world));
            if (!test.isOpen) return test;
        }
        if (!hdoor.isOpen) return hdoor;
        return null;
    }

    public void openAllOverlapping(HDoor hd) {
        if (hd.world == null)
            hd.world = plugin.getWorld(hd.door_world);
        if (hd.overlapAmount != 0) {
            for(HDoorOverlaps hdo : hd.hdoorOverlaps.values()) {
                hdo.d.open();
            }
        }
        hd.close();
    }

    /*
     * If a door record has been deleted all the overlaps associated with that door are
     * also deleted here.
     */
    public void deleteOverlaps(HDoor hd) {
        for(HDoor door : allHDoorsMap.values()) {
            if (door.overlapAmount > 0) {
                HDoorOverlaps hdl = new HDoorOverlaps(hd);
                door.hdoorOverlaps.remove(hdl);
                door.overlapAmount--;
                dataWriter.saveDatabase(false);
            }
        }
    }

    /*
     * If a door has been recorded as being overlapped and no longer overlaps a
     * certain block that information is modified here.
     */
    public void deleteOverlaps(HDoor in_hdoor, int in_x, int in_y, int in_z) {
        List<HDoor> doorsRemoved = new ArrayList<HDoor>();
        HDoor overlapDoor;
        HDoorOverlaps hdol;
        for(HDoorOverlaps in_hdoorOverlap : in_hdoor.hdoorOverlaps.values()) {
            overlapDoor = allHDoorsMap.get(new HDoor(in_hdoorOverlap.name, in_hdoorOverlap.creator, in_hdoorOverlap.world));
            if (overlapDoor.doorOverlap(in_x, in_y, in_z)) {
                hdol = overlapDoor.hdoorOverlaps.get(new HDoorOverlaps(in_hdoor));
                if (hdol == null) continue;
                if (hdol.blocksOverlaped - 1 == 0) {
                    overlapDoor.hdoorOverlaps.remove(hdol);
                    overlapDoor.overlapAmount--;
                    doorsRemoved.add(overlapDoor);
                }
                else {
                    hdol.blocksOverlaped--;
                    in_hdoorOverlap.blocksOverlaped--;
                }
            }
        }
        for(HDoor door : doorsRemoved) {
            hdol = in_hdoor.hdoorOverlaps.get(new HDoorOverlaps(door));
            if (hdol == null) continue;
            in_hdoor.hdoorOverlaps.remove(hdol);
            in_hdoor.overlapAmount--;
        }
    }

    /*
     * If an overlap is detected the overlapping door is cataloged, counted, and
     * the number of blocks that overlap recorded.
     */
    public HDoor findOverlaps(HDoor in_door, int x, int y, int z) {
        String doorWorld = in_door.door_world;
        HDoor lastDoor = null;
        for(HDoor hd : allHDoorsMap.values()) {
            if (hd.doorOverlap(x, y, z) && hd.door_world.equals(doorWorld)) {
                if (!in_door.door_creator.equals(hd.door_creator)) return hd; // Prevents door overlapping other owners' doors
                int newDoorOverlapNum = in_door.overlapAmount;
                int otherDoorOverlapNum = hd.overlapAmount;
                boolean duplicate = false;
                HDoorOverlaps overlap = in_door.hdoorOverlaps.get(new HDoorOverlaps(hd));
                if (overlap != null) {
                    duplicate = true;
                    if (dataWriter.isOverlapHDoors())
                        overlap.blocksOverlaped++;
                    else
                        return hd;
                }
                if (!duplicate && !in_door.equals(hd)) {
                    if (dataWriter.isOverlapHDoors()) {
                        HDoorOverlaps hdl = new HDoorOverlaps(1, hd);
                        in_door.hdoorOverlaps.put(hdl, hdl);
                        if (newDoorOverlapNum >= 0)
                                in_door.overlapAmount = in_door.hdoorOverlaps.size();
                    }
                    else
                        return hd;
                }
                duplicate = false;
                overlap = hd.hdoorOverlaps.get(new HDoorOverlaps(in_door));
                if (overlap != null) {
                    duplicate = true;
                    if (dataWriter.isOverlapHDoors())
                        overlap.blocksOverlaped++;
                    else
                        return hd;
                }
                if (!duplicate && !in_door.equals(hd)) {
                    if (dataWriter.isOverlapHDoors()) {
                        HDoorOverlaps hdl = new HDoorOverlaps(1, in_door);
                        hd.hdoorOverlaps.put(hdl, hdl);
                        if (otherDoorOverlapNum >= 0)
                                    hd.overlapAmount = hd.hdoorOverlaps.size();
                    }
                    else
                        return hd;
                }
                lastDoor = hd;
            }
        }
        return lastDoor;
    }

    public boolean addBlock(Block block, Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        int blockX = block.getLocation().getBlockX();
        int blockY = block.getLocation().getBlockY();
        int blockZ = block.getLocation().getBlockZ();
        HDoor overlapDoor = null;
        TwoStateDoor tsd = null;
        SingleStateDoor ssd = null;
        Object obj = dataWriter.blockMap.get(block);
        if (obj != null) {
            if (obj instanceof TwoStateDoor)
                tsd = (TwoStateDoor) obj;
            else if (obj instanceof SingleStateDoor)
                ssd = (SingleStateDoor) obj;
        }
        if (tsd == null && ssd == null) {
            int blockId = block.getTypeId();
            byte blockData = block.getData();
            if (!plugin.itemcodeshelper.getItem(Integer.toString(blockId), Integer.toString(blockData)).contains("INVALID")) {
                HDoor door = (HDoor) settings.door;
                if (dataWriter.getMax_hdoorSize() == -1 || !(door.hDoorContents.size() >= dataWriter.getMax_hdoorSize())) {
                    overlapDoor = (HDoor) plugin.hdoorhelper.findOverlaps(door, blockX, blockY, blockZ);
                    if (plugin.datawriter.isOverlapHDoors() && overlapDoor != null) {
                        if (overlapDoor.door_creator.equals(player.getName())) {
                            player.sendMessage("Adding block " + ChatColor.AQUA + blockId + ":" + blockData + ChatColor.WHITE + " at x:" + ChatColor.AQUA + blockX + ChatColor.WHITE + " y:" + ChatColor.AQUA + blockY + ChatColor.WHITE + " z:" + ChatColor.AQUA + blockZ + ChatColor.WHITE + " to hybrid door " + ChatColor.AQUA + door.door_name);
                            door.hDoorContents.put(new CustomLocation(block.getLocation()), new DoorState(block));
                            dataWriter.addToBlockMap(block, door);
                            if (!door.coordsSet)
                                door.coordsSet = true;
                        }
                        else {
                            player.sendMessage(ChatColor.RED + "Overlapping " + ChatColor.AQUA + overlapDoor.door_creator + "'s " + overlapDoor.door_name + ChatColor.RED + " not allowed!");
                            return true;
                        }
                    }
                    else if ((!plugin.datawriter.isOverlapHDoors() || plugin.datawriter.isOverlapHDoors()) && overlapDoor == null) {
                        player.sendMessage("Adding block " + ChatColor.AQUA + blockId + ":" + blockData + ChatColor.WHITE + " at x:" + ChatColor.AQUA + blockX + ChatColor.WHITE + " y:" + ChatColor.AQUA + blockY + ChatColor.WHITE + " z:" + ChatColor.AQUA + blockZ + ChatColor.WHITE + " to hybrid door " + ChatColor.AQUA + door.door_name);
                        door.hDoorContents.put(new CustomLocation(block.getLocation()), new DoorState(block));
                        dataWriter.addToBlockMap(block, door);
                        if (!door.coordsSet)
                            door.coordsSet = true;
                    }
                    else {
                        player.sendMessage(ChatColor.GOLD + "Overlap of HDoors is disabled in the config!");
                        player.sendMessage(ChatColor.RED + "Block was not saved!");
                        return true;
                    }

                }
                else {
                    player.sendMessage(ChatColor.RED + "Max Hdoor size reached for " + ChatColor.AQUA + door.door_name);
                    player.sendMessage(ChatColor.RED + "Remove a block or " + ChatColor.WHITE + "/dhdoor " + ChatColor.AQUA + door.door_name + " done " + ChatColor.RED + " to quit.");
                    return true;
                }
            }
            else {
                player.sendMessage("BlockDoor: " + ChatColor.RED + "Placement canceled, not a valid block type!");
                return true;
            }
        }
        else {
            if (tsd != null)
                player.sendMessage("BlockDoor: " + ChatColor.RED + "Placement canceled, Overlap with " + ChatColor.WHITE + tsd.door_creator + "'s " + tsd.door_name + ChatColor.RED + " not allowed!");
            else if (ssd != null)
                player.sendMessage("BlockDoor: " + ChatColor.RED + "Placement canceled, Overlap with " + ChatColor.WHITE + ssd.door_creator + "'s " + ssd.door_name + ChatColor.RED + " not allowed!");
            return true;
        }
        return false;
    }

    public void removeBlock(BlockBreakEvent event, BlockDoorSettings settings) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        int blockX = block.getLocation().getBlockX();
        int blockY = block.getLocation().getBlockY();
        int blockZ = block.getLocation().getBlockZ();
        int blockId = block.getTypeId();
        int blockData = block.getData();
        Object obj = dataWriter.blockMap.get(block);
        HDoor hd = null;
        if (obj == null) return;
        if (obj instanceof HDoor) hd = (HDoor) findClosedDoor((HDoor) obj);
        if (hd != null) {
            if (settings.command.equalsIgnoreCase("hdoor")) {
                if (hd.equals(settings.door)) {
                    DoorState hBlocks = plugin.hdoorhelper.findCoordinates(blockX, blockY, blockZ, hd);
                    if (hBlocks != null) {
                        if ((plugin.playerHasPermission(player, "blockdoor.admin.delete")) || (hd.door_creator.equals(player.getName()) && plugin.playerHasPermission(player, "blockdoor.dhdoor"))) {
                            player.sendMessage("Removing block " + ChatColor.AQUA + blockId + ":" + blockData + ChatColor.WHITE + " at x:" + ChatColor.AQUA + blockX + ChatColor.WHITE + " y:" + ChatColor.AQUA + blockY + ChatColor.WHITE + " z:" + ChatColor.AQUA + blockZ + ChatColor.WHITE + " from hybrid door " + ChatColor.AQUA + hd.door_name);
                            plugin.hdoorhelper.deleteOverlaps(hd, blockX, blockY, blockZ);
                            hd.hDoorContents.remove(new CustomLocation(block.getLocation()));
                            dataWriter.blockMap.remove(block);
                            dataWriter.updateOldHDBlockMap(hd);
                            dataWriter.addHybridDoorToBlockMap(hd);
                            if (hd.hDoorContents.size() == 0)
                                hd.coordsSet = false;
                        }
                        else {
                            player.sendMessage("BlockDoor: " + ChatColor.RED + "Canceled, you do not have permission to remove:");
                            player.sendMessage(ChatColor.AQUA + hd.door_creator + "'s " + ChatColor.RED + "Hybrid door block.");
                            event.setCancelled(true);
                        }
                    }
                    else {
                        //TODO do something here?
                    }
                }
                else {
                    //TODO do something here?
                }
            }
            else {
                event.getPlayer().sendMessage("BlockDoor: " + ChatColor.RED + "Canceled, use " + ChatColor.WHITE + "/dhdoor " + hd.door_name + ChatColor.RED + " to edit:");
                event.getPlayer().sendMessage(ChatColor.AQUA + hd.door_creator + "'s " + ChatColor.RED + "Hybrid door block.");
                event.setCancelled(true);
            }
        }
    }
}
