package plugin.arcwolf.blockdoor.Doors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.BlockDoor.DoorTypes;
import plugin.arcwolf.blockdoor.BlockDoor.Locks;
import plugin.arcwolf.blockdoor.Utils.CustomLocation;
import plugin.arcwolf.blockdoor.Utils.DataWriter;
import plugin.arcwolf.blockdoor.Utils.SpecialCaseCheck;

public class TwoStateDoor extends Door {

    public Locks locks;

    public LinkedHashMap<CustomLocation, DoorState> closedState = new LinkedHashMap<CustomLocation, DoorState>();
    public Map<Integer, CustomLocation> closedStateIndex = new HashMap<Integer, CustomLocation>();
    public LinkedHashMap<CustomLocation, DoorState> openState = new LinkedHashMap<CustomLocation, DoorState>();
    public Map<Integer, CustomLocation> openStateIndex = new HashMap<Integer, CustomLocation>();
    public Map<DoorOverlaps, DoorOverlaps> doorOverlaps = new HashMap<DoorOverlaps, DoorOverlaps>();

    public TwoStateDoor(String in_name, String in_creator, String in_world, String in_lockset) {
        super(in_name, in_creator, in_world, DoorTypes.TWOSTATE);

        overlapAmount = 0;

        if (in_lockset.equalsIgnoreCase("LOCKED")) {
            locks = Locks.LOCKED;
        }
        else if (in_lockset.equalsIgnoreCase("UNLOCKED")) {
            locks = Locks.UNLOCKED;
        }
    }

    public TwoStateDoor(String in_string) {
        super(in_string);
        String[] split = in_string.split(":");
        door_type = DoorTypes.TWOSTATE;
        if (split.length == 16) {
            door_creator = split[1];
            door_name = split[2];

            door_start_x = Integer.parseInt(split[3]);
            door_start_y = Integer.parseInt(split[4]);
            door_start_z = Integer.parseInt(split[5]);

            door_end_x = Integer.parseInt(split[6]);
            door_end_y = Integer.parseInt(split[7]);
            door_end_z = Integer.parseInt(split[8]);

            door_world = split[9];
            world = BlockDoor.plugin.getWorld(door_world);
            coordsSet = Boolean.parseBoolean(split[10]);
            isOpen = Boolean.parseBoolean(split[11]);

            if (split[12].equalsIgnoreCase("LOCKED"))
                locks = Locks.LOCKED;
            else if (split[12].equalsIgnoreCase("UNLOCKED"))
                locks = Locks.UNLOCKED;

            overlapAmount = Integer.parseInt(split[13]);

            if (overlapAmount != 0) {
                String[] overlapSplit = split[14].split("\\|");
                DoorOverlaps ol;
                for(int i = 0; i < overlapSplit.length; i++) {
                    ol = new DoorOverlaps(overlapSplit[i]);
                    doorOverlaps.put(ol, ol);
                }
            }

            String[] contentsSplit = split[15].split("\\|");
            DoorState cc;
            CustomLocation cloc;
            for(int i = 0; i < contentsSplit.length; i++) {
                if (contentsSplit[i].contains("$")) {
                    break;
                }
                cc = new DoorState(contentsSplit[i]);
                cloc = new CustomLocation(door_world, cc.x, cc.y, cc.z);
                closedState.put(cloc, cc);
                closedStateIndex.put(i, cloc);
            }
            contentsSplit = split[15].split("\\$");
            String[] ssContents = contentsSplit[1].split("\\|");
            for(int i = 0; i < ssContents.length; i++) {
                cc = new DoorState(ssContents[i]);
                cloc = new CustomLocation(door_world, cc.x, cc.y, cc.z);
                openState.put(cloc, cc);
                openStateIndex.put(i, cloc);
            }
        }
    }

    public void addClosedBlock(CustomLocation cloc, DoorState ds, int index) {
        closedState.put(cloc, ds);
        closedStateIndex.put(index, cloc);
    }

    public void addOpenBlock(CustomLocation cloc, DoorState ds, int index) {
        openState.put(cloc, ds);
        openStateIndex.put(index, cloc);
    }

    // Tests for overlap with provided coordinates.
    public boolean doorOverlap(int x, int y, int z) {
        return (door_start_x <= x && x <= door_end_x &&
                door_start_y <= y && y <= door_end_y &&
                door_start_z <= z && z <= door_end_z);
    }

    public void toggle() {
        if (isOpen) {
            close();
        }
        else {
            open();
        }
    }

    public void open() {
        isOpen = true;
        BlockDoor.plugin.datawriter.doorPhysics.put(this, true);
        twostateBuilder(openState, openStateIndex);
        BlockDoor.plugin.datawriter.doorPhysics.remove(this);
    }

    public void close() {
        BlockDoor.plugin.datawriter.doorPhysics.put(this, true);
        isOpen = false;
        twostateBuilder(closedState, closedStateIndex);
        BlockDoor.plugin.datawriter.doorPhysics.remove(this);
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("TWOSTATEDOOR:");
        builder.append(door_creator);
        builder.append(":");
        builder.append(door_name);
        builder.append(":");

        builder.append(Integer.toString(door_start_x));
        builder.append(":");
        builder.append(Integer.toString(door_start_y));
        builder.append(":");
        builder.append(Integer.toString(door_start_z));
        builder.append(":");

        builder.append(Integer.toString(door_end_x));
        builder.append(":");
        builder.append(Integer.toString(door_end_y));
        builder.append(":");
        builder.append(Integer.toString(door_end_z));
        builder.append(":");

        builder.append(door_world);
        builder.append(":");

        builder.append(Boolean.toString(coordsSet));
        builder.append(":");

        builder.append(Boolean.toString(isOpen));
        builder.append(":");

        builder.append("" + locks);
        builder.append(":");

        builder.append(Integer.toString(overlapAmount));
        builder.append(":");

        for(DoorOverlaps ol : doorOverlaps.values()) {
            builder.append(ol.creator);
            builder.append(",");
            builder.append(ol.name);
            builder.append(",");
            builder.append(ol.world);
            builder.append("|");
        }
        builder.append(":");

        for(DoorState dfs : closedState.values()) {
            builder.append(dfs.blockID);
            builder.append(",");
            builder.append(String.valueOf(dfs.offset));
            builder.append(",");
            builder.append(dfs.x);
            builder.append(",");
            builder.append(dfs.y);
            builder.append(",");
            builder.append(dfs.z);
            builder.append("|");
        }
        builder.append("$");
        for(DoorState dss : openState.values()) {
            builder.append(dss.blockID);
            builder.append(",");
            builder.append(String.valueOf(dss.offset));
            builder.append(",");
            builder.append(dss.x);
            builder.append(",");
            builder.append(dss.y);
            builder.append(",");
            builder.append(dss.z);
            builder.append("|");
        }

        return builder.toString();
    }

    /*
     * New Twostate Door update method:
     * 
     * Loop 1: 
     * Clear all old state using the new state blocks except when the old state block type is a popping block.
     * Set those blocks to 0
     * 
     * Loop 2: Special only Blocks
     * Place all popping blocks into door
     * Make special exception for water and lava. Set those blocks to 0.
     * 
     * Loop 3: Special only Blocks
     * Place all water and lava blocks
     * Check to make sure all blocks have been correctly placed.
     * If not remove from record.
     * 
     * All popping issues are left to be resolved by multiple event checks in the listener classes.
     */

    private void twostateBuilder(LinkedHashMap<CustomLocation, DoorState> newState, Map<Integer, CustomLocation> stateIndex) {
        BlockDoor plugin = BlockDoor.plugin;
        DataWriter dataWriter = plugin.datawriter;
        int x, y, z, blockID;
        byte offset;
        List<DoorState> popBlocks = new ArrayList<DoorState>();
        // If an unlocked door is in the mix of overlaps, switch this door to
        // unlocked and lock the other doors.
        if (overlapAmount > 0 && locks == Locks.LOCKED)
            softLock();
        coordsSet = false; // For physics checks

        // Erase Old Door State with New Door State minus the Popping Block Types
        for(int i = newState.size() - 1; i >= 0; i--) {
            CustomLocation index = stateIndex.get(i);
            x = newState.get(index).x;
            y = newState.get(index).y;
            z = newState.get(index).z;
            blockID = newState.get(index).blockID;
            offset = newState.get(index).offset;
            if (!isPopBlock(blockID))
                updateBlock(world, x, y, z, blockID, offset, false);
            else {
                updateBlock(world, x, y, z, 0, false);
                popBlocks.add(new DoorState(world, x, y, z, blockID, offset));
            }
        }

        if (dataWriter.isEnableSpecialBlocks()) {
            // Create all popping blocks in new door state
            for(DoorState ds : popBlocks) {
                updateBlock(world, ds.x, ds.y, ds.z, ds.blockID, ds.offset, false);
            }

            // Test for door state completeness. Remove offending records from state.
            for(DoorState ds : popBlocks) {
                x = ds.x;
                y = ds.y;
                z = ds.z;
                Location loc = new Location(world, x, y, z);
                blockID = ds.blockID;
                offset = ds.offset;
                if (blockID == 0) continue;
                if (loc.getBlock().getTypeId() == blockID || SpecialCaseCheck.isWater(loc.getBlock().getTypeId()) || SpecialCaseCheck.isLava(loc.getBlock().getTypeId()))
                    continue;
                else {
                    ds.blockID = 0;
                    ds.offset = 0;
                }
            }
        }
        coordsSet = true; // For physics checks
    }

    /*
     * Locking and unlocking changes the count from positive to negative based
     * on the lock state of the overlapping doors.
     * Used in the detection of unlocked doors for door toggling.
     * Negative values will be ignored for checks by toggling while positive
     * values are checked for unlocked doors.
     */

    // lock all doors based on the provided door except the provided door
    public void softLock() {
        locks = Locks.UNLOCKED;
        overlapAmount = Math.abs(overlapAmount);
        for(DoorOverlaps overlap : doorOverlaps.values()) {
            ((TwoStateDoor) overlap.d).locks = Locks.LOCKED;
            overlap.d.overlapAmount = Math.abs(overlap.d.overlapAmount);
        }
    }

    public boolean isLocked() {
        return locks.equals(Locks.LOCKED);
    }

    /**
     * Checks if a given blockID is a known pop block
     * 
     * @param blockID
     * @return true if blockID is known pop block
     */
    private boolean isPopBlock(int blockId) {
        return SpecialCaseCheck.isSpecialBlock(blockId);
    }
}