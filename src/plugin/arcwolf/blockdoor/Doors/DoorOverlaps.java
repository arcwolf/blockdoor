package plugin.arcwolf.blockdoor.Doors;

/* Used to keep track of door overlaps. 
 * 
 * In Two State Door this is used for testing for its lock state. If a door is toggled that was locked 
 * and it overlaps an unlocked door then the locked door will be unlocked and the unlocked door will
 * be locked. Thereby making sure that the visible door is always the unlocked door.
 * 
 * In Single State Door this is used to keep track of single state door overlaps. Primarily for blockmap
 * updating.
 */

public class DoorOverlaps{

    public String creator = "", name = "", world = "";
    public Door d = null;

    public DoorOverlaps(Door in_door) {
        d = in_door;
        creator = d.door_creator;
        name = d.door_name;
        world = d.door_world;
    }

    public DoorOverlaps(String in_string) {
        String[] split = in_string.split(",");
        if (split.length == 3) {
            creator = split[0];
            name = split[1];
            world = split[2];
        }
        else if (split.length == 4) {
            creator = split[1];
            name = split[2];
            world = split[3];
        }
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((creator == null) ? 0 : creator.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((world == null) ? 0 : world.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        DoorOverlaps other = (DoorOverlaps) obj;
        if (creator == null) {
            if (other.creator != null) return false;
        }
        else if (!creator.equals(other.creator)) return false;
        if (name == null) {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) return false;
        if (world == null) {
            if (other.world != null) return false;
        }
        else if (!world.equals(other.world)) return false;
        return true;
    }
}