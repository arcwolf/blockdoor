package plugin.arcwolf.blockdoor.Doors;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import plugin.arcwolf.blockdoor.BlockDoor;
import plugin.arcwolf.blockdoor.Triggers.Trigger;
import plugin.arcwolf.blockdoor.Utils.BlockDoorSettings;
import plugin.arcwolf.blockdoor.Utils.DataWriter;

public class SingleStateDoorHelper {

    private BlockDoor plugin;
    private DataWriter dataWriter;
    private Map<SingleStateDoor, SingleStateDoor> allSSDoorsMap;

    public SingleStateDoorHelper() {
        plugin = BlockDoor.plugin;
        dataWriter = plugin.datawriter;
        allSSDoorsMap = plugin.datawriter.allSSDoorsMap;
    }

    // Returns a Door for the player, making a new one if need be.
    public SingleStateDoor findOrCreateDoor(String in_name, String in_creator, String in_world, Player player) {
        BlockDoorSettings settings = BlockDoorSettings.getSettings(player);
        SingleStateDoor ssd = new SingleStateDoor(in_name, in_creator, in_world);
        if (allSSDoorsMap.containsKey(ssd))
            return allSSDoorsMap.get(ssd);
        else {
            allSSDoorsMap.put(ssd, ssd);
            settings.notFound = 1;
            return ssd;
        }
    }

    //Returns a Door for the player
    public SingleStateDoor findDoor(String in_name, String in_creator, String in_world) {
        return (SingleStateDoor) allSSDoorsMap.get(new SingleStateDoor(in_name, in_creator, in_world));
    }

    public SingleStateDoor findDoor(DoorOverlaps ol) {
        return (SingleStateDoor) allSSDoorsMap.get(new SingleStateDoor(ol.name, ol.creator, ol.world));
    }

    public void deleteOverlaps(SingleStateDoor singleStateDoor) {
        for(DoorOverlaps overlap : singleStateDoor.doorOverlaps.values()) {
            for(DoorOverlaps o : ((SingleStateDoor)overlap.d).doorOverlaps.values()) {
                if (o.d.equals(singleStateDoor)) {
                    ((SingleStateDoor)overlap.d).doorOverlaps.remove(o);
                    overlap.d.overlapAmount = ((SingleStateDoor)overlap.d).doorOverlaps.size();
                    break;
                }
            }
        }
    }

    public void createDoor(BlockDoorSettings settings, Player player, Block blockClicked) {
        int blockX = (int) blockClicked.getLocation().getX();
        int blockY = (int) blockClicked.getLocation().getY();
        int blockZ = (int) blockClicked.getLocation().getZ();
        SingleStateDoor ssd = findOrCreateDoor(settings.name, player.getName(), player.getWorld().getName(), player);

        if (settings.select == 1) {
            settings.door = ssd;
            player.sendMessage("Setting door start: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
            settings.startX = blockX;
            settings.startY = blockY;
            settings.startZ = blockZ;

            settings.select = 2;
        }
        else if (settings.select == 2) {
            player.sendMessage("Setting door end: '" + ChatColor.GREEN + settings.name + ChatColor.WHITE + "'");
            //Checking to make sure start > end to make for loops easier later
            if (settings.startX > blockX) {
                settings.endX = settings.startX;
                settings.startX = blockX;
            }
            else {
                settings.endX = blockX;
            }

            if (settings.startY > blockY) {
                settings.endY = settings.startY;
                settings.startY = blockY;
            }
            else {
                settings.endY = blockY;
            }

            if (settings.startZ > blockZ) {
                settings.endZ = settings.startZ;
                settings.startZ = blockZ;
            }
            else {
                settings.endZ = blockZ;
            }

            int xDist = Math.abs(settings.endX - settings.startX);
            int yDist = Math.abs(settings.endY - settings.startY);
            int zDist = Math.abs(settings.endZ - settings.startZ);

            if (dataWriter.getMax_DoorSize() != -1 && ((xDist + 1 > dataWriter.getMax_DoorSize()) || (yDist + 1 > dataWriter.getMax_DoorSize()) || (zDist + 1 > dataWriter.getMax_DoorSize()))) {
                player.sendMessage(ChatColor.RED + "DOOR DIMENSIONS REJECTED for: '" + ChatColor.WHITE + settings.name + ChatColor.RED + "'");
                if (!ssd.coordsSet) {
                    allSSDoorsMap.remove(ssd);
                }
                BlockDoorSettings.clearSettings(player);
            }
            else {
                if (ssd.coordsSet) {
                    dataWriter.updateOldSSDBlockMap(ssd);
                }
                boolean found = false;
                Map<Block, Object> tempBlockMap = new HashMap<Block, Object>();
                HashMap<DoorOverlaps, DoorOverlaps> tempOverlaps = new HashMap<DoorOverlaps, DoorOverlaps>();
                SingleStateDoor tempDoor = null;
                search:
                for(int x = settings.startX; x <= settings.endX; x++)
                    for(int z = settings.startZ; z <= settings.endZ; z++)
                        for(int y = settings.startY; y <= settings.endY; y++) {
                            Block block = ssd.world.getBlockAt(x, y, z);
                            Object obj = dataWriter.blockMap.get(block);
                            if (obj == null) {
                                tempBlockMap.put(block, ssd);
                                continue;
                            }
                            else if (obj instanceof SingleStateDoor) {
                                SingleStateDoor testssd = (SingleStateDoor) obj;
                                if (!testssd.equals(ssd)) {
                                    if (!dataWriter.isOverlapDoors() || !testssd.door_creator.equals(ssd.door_creator)) {
                                        tempDoor = testssd;
                                        found = true;
                                        break search;
                                    }
                                    else {
                                        for(SingleStateDoor d : dataWriter.allSSDoorsMap.values()) {
                                            if (!d.equals(ssd) && d.doorOverlap(x, y, z)) {
                                                DoorOverlaps ol = new DoorOverlaps(d);
                                                tempOverlaps.put(ol, ol);
                                            }
                                        }
                                        tempBlockMap.put(block, ssd);
                                    }
                                }
                            }
                            else if (obj instanceof TwoStateDoor) {
                                TwoStateDoor tsdoor = (TwoStateDoor) obj;
                                if (tsdoor.doorOverlap(x, y, z)) {
                                    player.sendMessage("Overlap of Two State Door [" + ChatColor.GREEN + tsdoor.door_name + ChatColor.WHITE + "] Created by [" +
                                                    ChatColor.GREEN + tsdoor.door_creator + ChatColor.WHITE + "]");
                                    player.sendMessage("Starting at [X= " + ChatColor.AQUA + tsdoor.door_start_x + ChatColor.WHITE + " Y= " +
                                                    ChatColor.AQUA + tsdoor.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                                    tsdoor.door_start_z + ChatColor.WHITE + "]");
                                    player.sendMessage("Ending at [X= " + ChatColor.AQUA + tsdoor.door_end_x + ChatColor.WHITE + " Y= " +
                                                    ChatColor.AQUA + tsdoor.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                                    tsdoor.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                                                    tsdoor.door_world + ChatColor.WHITE + "]");
                                    found = true;
                                    break search;
                                }
                            }
                            else if (obj instanceof HDoor) {
                                HDoor hd = (HDoor) obj;
                                if (hd.doorOverlap(x, y, z)) {
                                    player.sendMessage("Overlap of Hybrid Door [" + ChatColor.GREEN + hd.door_name + ChatColor.WHITE + "] Created by [" +
                                                    ChatColor.GREEN + hd.door_creator + ChatColor.WHITE + "]");
                                    player.sendMessage("in world [" + ChatColor.AQUA + hd.door_world + ChatColor.WHITE + "]");
                                    found = true;
                                    break search;
                                }
                            }
                            else if (obj instanceof Trigger) {
                                Trigger t = (Trigger) obj;
                                plugin.triggerhelper.showOverlapMessage(t, player);
                                found = true;
                                break search;
                            }
                        }
                if (found) {
                    if (tempDoor != null) {
                        player.sendMessage("Overlap of Door [" + ChatColor.GREEN + tempDoor.door_name + ChatColor.WHITE + "] Created by [" +
                                ChatColor.GREEN + tempDoor.door_creator + ChatColor.WHITE + "]");
                        player.sendMessage("Starting at [X= " + ChatColor.AQUA + tempDoor.door_start_x + ChatColor.WHITE + " Y= " +
                                ChatColor.AQUA + tempDoor.door_start_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                tempDoor.door_start_z + ChatColor.WHITE + "]");
                        player.sendMessage("Ending at [X= " + ChatColor.AQUA + tempDoor.door_end_x + ChatColor.WHITE + " Y= " +
                                ChatColor.AQUA + tempDoor.door_end_y + ChatColor.WHITE + " Z= " + ChatColor.AQUA +
                                tempDoor.door_end_z + ChatColor.WHITE + "] in world [" + ChatColor.AQUA +
                                tempDoor.door_world + ChatColor.WHITE + "]");
                    }
                    player.sendMessage(ChatColor.RED + "DOOR '" + ChatColor.WHITE + settings.name + ChatColor.RED + "' REJECTED");
                    if (!ssd.coordsSet) {
                        allSSDoorsMap.remove(ssd);
                    }
                    BlockDoorSettings.clearSettings(player);
                }
                else {
                    // Update door Coordinates
                    ssd.updateCoordinates(settings);

                    // Remove old overlaps
                    for(DoorOverlaps ol : ssd.doorOverlaps.values()) {
                        ((SingleStateDoor)ol.d).doorOverlaps.remove(new DoorOverlaps(ssd));
                        ol.d.overlapAmount = ((SingleStateDoor)ol.d).doorOverlaps.size();
                    }

                    // Update door overlaps
                    ssd.doorOverlaps = tempOverlaps;
                    ssd.overlapAmount = tempOverlaps.size();

                    // Update new overlaps of doors
                    for(DoorOverlaps ol : ssd.doorOverlaps.values()) {
                        SingleStateDoor d = (SingleStateDoor) ol.d;
                        DoorOverlaps o = new DoorOverlaps(ssd);
                        d.doorOverlaps.put(o, o);
                        d.overlapAmount = d.doorOverlaps.size();
                    }

                    // Update blockmap
                    dataWriter.blockMap.putAll(tempBlockMap);

                    // Cleanup and save
                    ssd.coordsSet = true;
                    BlockDoorSettings.clearSettings(player);
                    dataWriter.saveDatabase(false);
                }
            }
        }
    }
}