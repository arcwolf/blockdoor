package plugin.arcwolf.blockdoor.Doors;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

// Block Data for the state of a door object

public class DoorState{

    public int x, y, z, blockID;
    public byte offset;
    public World world;

    public DoorState(World in_world, int in_x, int in_y, int in_z, int in_blockID, byte in_offset) {
        world = in_world;
        x = in_x;
        y = in_y;
        z = in_z;
        blockID = in_blockID;
        offset = in_offset;
    }

    public DoorState(Block block) {
        world = block.getWorld();
        x = block.getX();
        y = block.getY();
        z = block.getZ();
        blockID = block.getTypeId();
        offset = block.getData();
    }

    public DoorState(Location location) {
        world = location.getWorld();
        x = location.getBlockX();
        y = location.getBlockY();
        z = location.getBlockZ();
        blockID = location.getBlock().getTypeId();
        offset = location.getBlock().getData();
    }

    public DoorState(String in_string) {
        String[] split = in_string.split(",");
        if (split.length == 5) {
            blockID = Integer.parseInt(split[0]);
            offset = Byte.valueOf(split[1]);
            x = Integer.parseInt(split[2]);
            y = Integer.parseInt(split[3]);
            z = Integer.parseInt(split[4]);
        }
    }

    public boolean doorOverlap(int in_x, int in_y, int in_z) {
        return x == in_x && y == in_y && z == in_z;
    }
}