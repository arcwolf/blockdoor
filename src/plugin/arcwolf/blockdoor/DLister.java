package plugin.arcwolf.blockdoor;

public class DLister {

    public String typeName;
    public String playerName;
    public String objectName;
    public String trigger;
    public String worldName;

    public DLister(String in_String) {
        String[] split = in_String.split(":");

        if (split.length >= 3) {
            typeName = split[0];
            playerName = split[1];
            objectName = split[2];
            trigger = "";

            if (typeName.equalsIgnoreCase("UZONE")) {
                trigger = split[3];
            }

            if (typeName.equalsIgnoreCase("TRIGGER"))
                worldName = split[7];
            else if (typeName.equalsIgnoreCase("MYTRIGGER"))
                worldName = split[7];
            else if (typeName.equalsIgnoreCase("DOOR"))
                worldName = split[13];
            else if (typeName.equalsIgnoreCase("HDOOR"))
                worldName = split[3];
            else if (typeName.equalsIgnoreCase("TWOSTATEDOOR") || typeName.equalsIgnoreCase("TSDOOR")) {
                typeName = "TSDOOR";
                worldName = split[9];
            }
            else if (typeName.equalsIgnoreCase("REDTRIG"))
                worldName = split[7];
            else if (typeName.equalsIgnoreCase("ZONE"))
                worldName = split[12];
            else if (typeName.equalsIgnoreCase("MYZONE"))
                worldName = split[12];
            else if (typeName.equalsIgnoreCase("EZONE"))
                worldName = split[12];
            else if (typeName.equalsIgnoreCase("AZONE"))
                worldName = split[12];
            else if (typeName.equalsIgnoreCase("PZONE"))
                worldName = split[12];
            else if (typeName.equalsIgnoreCase("MZONE"))
                worldName = split[12];
            else if (typeName.equalsIgnoreCase("UZONE"))
                worldName = split[12];
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getTypeName() {
        return typeName;
    }
}