BlockDoor by Arcwolf

Original HeyO plugin by Ho0ber

This is a port and total rewrite of the BlockDoor plugin created by ho0ber for Hey0 server mod. The basic jest of the 
plugin is to allow players to define and use "doors" made out of blocks. The term "doors" doesn't really cover all the 
possibilities a creative player might come up with though.

* Permissions Support or Server OPs support if no permissions detected
* MultiWorld safe
* 7 Different types of zones now included, 3 mob only zones, 2 Player only zones and 2 entity zones
* 3 Different types of doors. A flood fill door type, a omni-block door type and a hybrid anywhere block door.
* Administrative Commands for console and in-game
* Redstone triggers are available for use.
* Two types on non-redstone triggers, Creator only trigger and any player trigger
* External Item Database saved in binary format for speed.
* Admin editable config file to limit zone and door sizes as well as zone, trigger, and door overlaps

Command List:

 /ddoor [doorname] - This creates a door with the given name and starts door selection. Right click two blocks to select the door region.

 /dtwostate <lock/unlock> [doorname] - This creates a two state door and also allows the player to lock or unlock the door for editing.
 
 /dhdoor [doorname] <done> - This will create a hybrid style door. Once you start creating this door any block you place is remembered and becomes a togglable block.

 /dtrig [triggername] - This starts trigger selection. Right click to select a trigger block, which will toggle the door when right clicked.

 /dmytrig [triggername] - This starts trigger selection. Right click to select a trigger block, which will toggle the door when right clicked.

 /dfill [doorname] <blockid or blockname:offset> - Sets the specified door to be made of the given block.

 /dempty [doorname] <blockid or blockname:offset> - Sets the specified door to be made of the given block.

 /dzone [zonename] - This creates a trigger zone which toggles the door when a player enters or exits. Right click two blocks to select the zone region.

 /dmyzone [zonename] - This creates a per-player trigger zone which only toggles for admins or the player who created it. Right click two blocks to select the zone region.

 /dmzone [zonename] - This creates an any Mob type triggered zone which toggles the door when mobs enter or exit. Right click two blocks to select the zone region.

 /dezone [zonename] - This creates a zone that toggles a door if any living entity (mobs or players) enter it. Right click two blocks to select the zone region.

 /dazone [zonename] - This creates a zone that toggles a door if any aggressive mob enters it. Right click two blocks to select the zone region.

 /dpzone [zonename] - This creates a zone that toggles a door if any passive mob enters it. Right click two blocks to select the zone region.

 /duzone [zonename] [trigger] - This creates a zone that toggles a door if the specified trigger enters the zone. Right click two blocks to select the zone region.

 /dtoggle [doorname] <creatorName>- Toggles the specified door. This state is also what is saved to file, and will be the state when the plugin reloads.

 /dtoggle2 [twostatedoorname] <creatorName>- Toggles the specified two state door. Like single state doors, this will save the state the door was in to file for reloads.
 
 /dtoggle3 [hybriddoorname] <creatorName>- Toggles the specified hybrid door. Like the other 2 toggle commands this will save the state the door was in to file for reloads.

 /dopen [doorname] <creatorName>- Opens the specified door. This state is also what is saved to file, and will be the state when the plugin reloads.
 
 /dclose [doorname] <creatorName>- Closes the specified door. This state is also what is saved to file, and will be the state when the plugin reloads.
 
 /dopen2 [twostatedoorname] <creatorName>- Opens the specified two state door. This state is also what is saved to file, and will be the state when the plugin reloads.
 
 /dclose2 [twostatedoorname] <creatorName>- Closes the specified two state door. This state is also what is saved to file, and will be the state when the plugin reloads.
 
 /dopen3 [hybriddoorname] <creatorName>- Opens the specified hybrid door. This state is also what is saved to file, and will be the state when the plugin reloads.
 
 /dclose2 [hybriddoorname] <creatorName>- Closes the specified hybrid door. This state is also what is saved to file, and will be the state when the plugin reloads.
 
 /dlist <all> <type> <player> <page> - Lists all doors, zones, and triggers created by you or if you are an administrator doors, zones and triggers created by you and other players.

 /ddel [link/door/twostate/hdoor/trigger/mytrigger/zone/myzone/mzone/pzone/azone/ezone] [name] <player> <world> - Deletes specified door, trigger, or zone 
 /ddel link <triggerOrZoneType:uzoneTrigger> <triggerName> <doorType> <doorName> - Deletes specified link to a door
 /ddel link <triggerOrZoneType:uzoneTrigger> <triggerName> <doorType> <doorName> <player> - Deletes specified link to a door
 /ddel link <triggerOrZoneType:uzoneTrigger> <triggerName> <doorType> <doorName> <player> <world> - Deletes specified link to a door
 (*admin permissions required to delete other players objects)

 /dlink [redtrig/mytrigger/trigger/zone/myzone/mzone/azone/pzone/ezone/uzone] [triggername] [doorname] (trigger) [toggle/open/close] <1/2/3> - Used to connect triggers and zones to a give door (or doors)

 /dredtrig [triggername] - Creates a Redstone trigger. The redstone power source must be right clicked on for it to be set. 
 Any click other then a right click on the object  during the use of the command will CANCEL the command. Once the redstone 
 trigger is set it will be monitored for changes in state. From on to off and so forth.

 /dinfo - Lists information about a clicked object or a zone the player walks into. dinfo will always display the an 
 unlocked door. The unlocked door is always visible. 

 /dreload - Reloads the blockdoor database, config file and itemdatabase.
 
 /dcancel - Cancels any active player command - DEPRECATED but still usable. Any blockdoor command entered while another 
 blockdoor command is in effect will cancel the active command
 
 /dsave - allows a player to force a save of a twostate door state. Click a twostate door and its currently visible state will be saved to the database.


 Console commands:  (must be enabled in config file)
 
 /dlist <type> <player> <page> 

 /ddel [door/twostate/trigger/mytrigger/zone/myzone/mzone/pzone/azone/ezone] [name] <player> <world>

 /dreload
     
Permissions Nodes:

###Bukkit permissions###
blockdoor.*
blockdoor.player
blockdoor.admin
blockdoor.player.*
blockdoor.admin.*
blockdoor.link
blockdoor.link.*
########################
blockdoor.door
blockdoor.player.twostate
blockdoor.admin.twostate
blockdoor.hdoor
blockdoor.trigger
blockdoor.mytrigger
blockdoor.admin.breaktriggers
blockdoor.player.breaktriggers
blockdoor.redstonetrigger
blockdoor.link.trigger
blockdoor.link.mytrigger
blockdoor.link.redtrig
blockdoor.link.zone
blockdoor.link.myzone
blockdoor.link.azone
blockdoor.link.pzone
blockdoor.link.ezone
blockdoor.link.mzone
blockdoor.link.uzone
blockdoor.admin.toggle
blockdoor.player.toggle
blockdoor.admin.toggle2
blockdoor.player.toggle2
blockdoor.player.toggle3
blockdoor.admin.toggle3
blockdoor.admin.open
blockdoor.player.open
blockdoor.admin.open2
blockdoor.player.open2
blockdoor.player.open3
blockdoor.admin.open3
blockdoor.admin.close
blockdoor.player.close
blockdoor.admin.close2
blockdoor.player.close2
blockdoor.player.close3
blockdoor.admin.close3
blockdoor.fill
blockdoor.empty
blockdoor.zone
blockdoor.myzone
blockdoor.azone
blockdoor.pzone
blockdoor.ezone
blockdoor.mzone
blockdoor.uzone
blockdoor.info
blockdoor.usetriggers
blockdoor.usezones
blockdoor.player.list
blockdoor.admin.list
blockdoor.admin.delete
blockdoor.player.delete
blockdoor.reload
blockdoor.cancel
blockdoor.admin.save
blockdoor.player.save
blockdoor.admin.myzone 
blockdoor.admin.mytrigger

***************************
Config File Example & explanations:
***************************

#Max size for X Y Z
# -1 to override
max_zone_size=20     		<- Max size of 20 blocks cubed.
max_door_size=10     		<- Max size of 10 blocks cubed.
max_hdoor_size=50    		<- Max of 50 blocks total.
max_twostate_size=20 		<- Max size of 20 blocks cubed.
#
#Advanced Users
#
allow_overlap_zones=false
allow_overlap_doors=false
allow_overlap_twostatedoors=false
allow_overlap_triggers=false
allow_overlap_redstone=false
allow_special_blocks=false
allow_console_commands=false
allow_overlap_hdoors=false

*** Explanation ***

Each of the max size settings except hdoor size control how big a door or zone can be. A size of 20 means that the zone or
door can be no larger then 20 blocks X by 20 blocks Y by 20 blocks Z. So, anything larger then 20x20x20x is rejected and
the user will be told this and the door creation will be canceled. 

The hdoor size is for the max number of blocks in a door. Because of the nature of a hdoor having each block at any
position in a single world the suggested max number of blocks if 50. However this will accept any size. A not of caution
though, the more blocks in a hdoor the slower the hdoor will toggle. The speed of you server will determine the exact
amount you can have in a hdoor. Experiment to find your max hdoor size.

-1 will override any max size setting, causing blockdoor to ignore the max size settings

The overlap settings allow each of the different BlockDoor objects to be overlapped. Each have varying degrees of 
usefulness. However, the usefulness needs to be weighed against the drawbacks of overlapping.

Hdoors can only overlap themselves and no other door type.

Special Blocks setting toggles BlockDoor into allowing the following block types in Two State Doors:

blockID 6: // Sapling
blockID 8: // Flowing Water
blockID 9: // Water Source
blockID 10: // Flowing Lava
blockID 11: // Lava Source
blockID 12: // Sand
blockID 13: // Gravel
blockID 27: // Powered Rail
blockID 28: // Detector Rail
blockID 31: // Grass
blockID 32: // Dead Bush
blockID 37: // Dandelion
blockID 38: // Rose
blockID 39: // Brown Mushroom
blockID 40: // Red Mushroom
blockID 50: // Torch
blockID 51: // Fire
blockID 55: // Redstone Wire
blockID 59: // Wheat
blockID 64: // Wood Door
blockID 65: // Ladder
blockID 66: // Rail
blockID 69: // Lever
blockID 70: // Stone Pressure Plate
blockID 71: // Iron Door
blockID 72: // Wooden Pressure Plate
blockID 75: // Redstone Torch inactive
blockID 76: // Redstone Torch active
blockID 77: // Stone Button
blockID 81: // Cactus
blockID 83: // Sugar Cane
blockID 93: // Redstone Repeater inactive
blockID 94: // Redstone Repeater active
blockID 96: // Trapdoor
blockID 104: // Pumpkin Stem
blockID 105: // Melon Stem
blockID 106: // Vine
blockID 111: // Lily Pad
blockID 115: // Nether Wart
blockID 127: // Cocoa Plant
blockID 131: // Tripwire hook
blockID 132: // tripwire
blockID 140: // flower pot
blockID 141: // Carrot Plant
blockID 142: // Potatoe Plant
blockID 143: // Wood Button
blockID 147: // Light Weight Pressure Plate
blockID 148: // Heavy Weight Pressure Plate
blockID 149: // Redstone Comparator inactive
blockID 150: // Redstone Comparator active
blockID 157: // Activator Rail

These block types have been found to POP if they are not placed correctly. So when a door is toggled it must remove these 
items first and re-create them last or they will pop. Every effort was made to keep these objects 
from popping they can some times still pop. Its very rare but can happen. 

The Following are disallowed blocks in a twostate door:

blockID 26: // Bed
blockID 29: // Sticky Piston
blockID 33: // Piston
blockID 34: // Piston head
blockID 36: // Move by Piston Block
blockID 46: // TNT
blockID 52: // Monster Spawner
blockID 90: // Nether Portal
blockID 92: // Cake
blockID 119: // End Portal
blockID 122: // Dragon egg
blockID 137: // Command Block
blockID 138: // Beacon
blockID 144: // Mob Heads
blockID 145: // Anvil

The final setting is to enable the console commands. If this is set to false console commands will be unavailable.

***************************
Basic list of overlapping issues:
***************************

A door of a different type can not overlap a door of another type. IE A twostate door can not be selected so that it would
overlap a singlestate door.

Any trigger type can not overlap other triggers of different types or overlap doors.

Zones can overlap any blockdoor object.

When two zones overlap the /dinfo command will not know which zone to report if a player is looking for a zone with the 
dinfo command. It will pick the first zone it finds in the overlapping set and report it back to the player. It will ignore 
all other zones in the overlapping area.

When two single state doors overlap a door fight might occur where one door overrides the other door creating unpredictable 
results when the door toggles between open and close. It depends on which door toggles first and which is toggled last.

Twostate Doors are designed to overlap. A huge amount of effort was put into making overlapping work. If a two state door
overlaps some more information is collected on the overlapping doors. Each door in the group is considered a layer. Much like
in photoshop. Only in this case only one door in a group can be unlocked at a time. The unlocked door in the group is ALWAYS
the door that is being edited and visible. When a player unlocks a door, that door is toggled to its closed state. This is to
insure the player knows what door he or she is editing. 
Additionally, the dinfo command will ALWAYS display to the player an Unlocked door if a door is unlocked in the overlapping
group. When a locked door is toggled in the overlapping group and there is a unlocked door in the group, the toggled door
becomes the unlocked door and the previously unlocked door is locked.

Hybrid doors may overlap another hybrid door. In this case whatever hybrid door is being edited at the time will be 
displayed in the dinfo command. Additionally if both overlapping doors are in a locked state and one of the doors is
put into a editing state that door will immediately become visible.

***************************

Other Notes about Two State Doors:

***************************

Beds are not allowed in twostate doors because a bed has a datavalue that is updated with a player enters and exits
the bed. This datavalue update has not event fired that I know of.

Anvils are not allowed in twostate doors because there is no event associated with anvil damage. meaning a simple toggle
and an anvil would be repaired to what ever level it was at when it was placed.

Sapplings and crops will not grow inside a twostate door

Ice will not melt inside a twostate door

Sand or gravel can not fall into a twostate door if that door is locked. However, if a twostate door is unlocked they will
fall naturally and blockdoor will update the doors effected.

Water and lava can be placed inside a twostate door however the liquids will not flow naturally. Too many calculations
were necessary for a door to activaly update the blocks liquids could flow into.

Because of the way two state doors are stored objects with inventories will loose their contents when a door is toggled.

Leaf decay is also not allowed inside a door. Leaves could generate an infinite supply of saplings and 

Fire does not consume blocks inside a twostate door fire has no fire extinguish event called when fire goes out. This 
results in blockdoor not knowing when a fire burns out. Meaning fire would come back the next time a door was toggled. 

Pistons can not push or pull a twostate door.

Endermen can not remove twostate door blocks.

TNT can not damage a locked twostate door. If a door is unlocked all blocked effected by an explosions will be removed
naturally from any twostate door.

Snow will not accumulate on a twostate door.

Mob spawners are not allowed because their spawn information will be lost in transition between states. Too much 
information needed to be stored for just one block type.

Signs are allowed however, their text is lost in transition.

Lighting is taken into account for flowers and mushrooms. If the light level changes on a flower when a transition occurs 
for a two state door. The light level must be within acceptable ranges for the flower or mushroom to be replaced back into 
the door. If it is not then they will be removed from the database and allowed to POP into an item. 

Water is taken into account for sugar cane and lily pads. If water is taken away from either it will be allowed to pop and 
will be removed from the database.

Sand is taken into account for cacti. Without sand the cactus will pop.

Grass or dirt is taken into account for flowers.

Valid blocks for mushrooms are also taken into account. Simply put, if you cant place a mushroom on it normally, then you 
cant create a door that would place a mushroom there abnormally. It will just be removed from the database and be allowed 
to pop or simply wont be planted in the first place.

Valid blocks are taken into account for any of the other special block types. If they cant normally be placed somewhere 
then they wont be allowed to be placed by a door toggle.

The /dsave command allows users to actively edit a door state with a plugin like world edit that doesnt normally generate 
block place or block destroy events. If you want to save a door after editing a door in this way you can use the /dsave 
command to force a save of the currently visible door state. The twostate door you are attempting to edit and save MUST be 
unlocked or /dsave will not save the door and will notify you of the error.
Very handy indeed.

**************************

Quick Notes about Hybrid Doors

**************************

This style of door accepts all the blocks that a single state(classic) door accepts. No effort was made to insure they do 
not pop. 
This door also may not overlap any other style of door. Because of the mechanics involved too much processing and testing 
would have been needed to insure that all the doors operated properly under that condition.
When finished creating the door typeing any blockdoor command will complete the door creation. However the accepted way
is to use /dhdoor done   to signify completion of a hdoor creation process.

**************************

Notes about UZONE's

Valid UZONE triggers are controlled by either the Mobs.txt database in your BlockDoor plugin directory or by the internal 
database. Should something go wrong with the Mobs.txt and it becomes corrupt or incorrectly formatted the plugin will 
default to the internal database.

Internally BlockDoor recognizes these Valid triggers for a uzones:

CraftPig, Pig, CraftCow, Cow, CraftChicken, Chicken, CraftSheep, Sheep, CraftSkeleton, Skeleton, CraftZombie, Zombie, 
CraftPigZombie, PigZombie, CraftCreeper, Creeper, CraftSlime, Slime, CraftSpider, Spider, CraftGiant, Giant, CraftWolf, 
Wolf, CraftSquid, Squid, CraftMagmaCube, MagmaCube, LavaSlime, CraftBlaze, Blaze, EnderDragon, CraftEnderDragon, 
CraftEnderman, Enderman, CraftSnowman, Snowman, Villager, CraftVillager, Testificate, MooShroom, MushroomCow, 
CraftMushroomCow, CaveSpider, CraftCaveSpider and Player Names.
Player names are case sensitive. JohnDoe is not the same as johndoe.

Notes about the Mobs.txt database:

This database contains a list of all the Mob Classes that CraftBukkit uses for the different mobs in minecraft. To add a 
new mob to this database you need to enter the correctly formatted Class name for that mob. 
EX: CraftChicken = chicken
would use the CraftChicken.class in Craftbukkit and assign it to the name chicken. This would allow you to use the name 
chicken in your triggers as a valid reference for all CraftChickens in minecraft. A note of importance, the Class name is 
case sensitive and must be entered exactly the same as it is in the CraftBukkit source code.

Notes about linking a door with a uzone:

This type of linking requires an additional argument. EX:

/dlink uzone uzonename johndoe doorname toggle 1

This would tell BlockDoor to link the single state door "doorname" to the uzone "uzonename - johndoe" and set it to toggle.

Other commands specific to uzones

/ddel uzone uzonename johndoe
/duzone uzonename johndoe

***************************

Basic Tutorial for BlockDoor Object Creation:


***************************
Single State Door Creation:
***************************

Steps:

Create a simple square of cobblestone 4 blocks high by 1 block wide by 4 blocks long.
Type into the minecraft chat box,

/ddoor MyDoor 

Next right click the bottom right most block of the square you just created. Then right click the left top block of the 
square you created.
You will be notified that a door has been created within the area you selected.
Next you will want to create a trigger for the door.
Place a block near the door its position is not important so long as its not on the door its self. Next place a lever or 
button on the block you just placed.
Now type

/dtrig MyTrig

into the minecraft chat box. Next right click the button or lever you placed on the block. You will be notified that you 
have just created a trigger.
Now you will need to link the door to the trigger. To do this type the following into the minecraft chat box.

/dlink trigger MyTrig MyDoor toggle 1

This command arguments basically tell BlockDoor that you want to link a (trigger) called (MyTrigger) to the door (MyDoor) 
and you want it to (toggle) and this door is a (1) single state door.

Now the door is linked to the trigger. You may click the trigger and you will see the door vanish. Click the door again and 
a stone door 4 blocks by 1 block wide by 4 blocks long will appear.
The door is now linked. But its fill is of solid stone. However, you created the door out of cobblestone. To change the 
door to fill with cobblestone you need to type the following into
the minecraft chat box.

/dfill MyDoor cobblestone

You will be notified that the door is now set to fill with cobblestone when it toggles.

Click the lever again until the door toggles back to its filled state. It should now be cobblestone instead of stone.

Congratulations, you have created your first blockdoor objects.



***************************
Two State Door Creation:
***************************

Steps:

Like a single state door, create a cobblestone square 4 by 1 by 4 in size.
Next Type into the minecraft chat box,

/dtwostate MyDoor2

Now, like a single state door, click the top left most corner and the bottom right most corner of the square you have 
created.
You will be notified that a door has been created.

This door is a bit different then a single state door. You do not have to worry about selecting its fill type. Every block 
that is inside the area you selected is memorized. In its current state you can actively edit the doors
contents. Removing and adding blocks as you choose.

When you are finished editing it and do not wish to make any further changes simply type into the minecraft chat box,

/dtwostate lock MyDoor2

You will be notified that the door MyDoor2 is now locked. No further editing is possible on the door until it is again 
unlocked.

To unlock a door type into the minecraft chat box,

/dtwostate unlock MyDoor2

you will again be notified and the door MyDoor2 will be unlocked and made ready to again be edited.

Now create a trigger called MyTrig2 like the single state door example above.

Once a trigger is created link the door to the trigger with the following command,

/dlink trigger MyTrig2 MyDoor2 toggle 2

This commands arguments are nearly the same as a single state door with the exception of the (2) denoting that you want to 
link to a two state door. If you leave out the (2) BlockDoor will not be able to find your door creation or may end up 
linking to the wrong door entirely. Make sure to use a (2) in the dlink command whenever you want to link to a two state 
door.

Now that you have linked to a two state door. You may toggle between the two states. The first state is the cobblestone 
door you created. The second state is empty. When the door is in an unlocked state you can toggle to the second state and 
add blocks to it by simply placing them inside the door area. In the same way you created the first state of this door. 
Simply lock the door again when you are finished editing and the door and its contents will be saved.

Toggle back and forth. You have now made a two state door!


***************************
Hybrid State Door Creation:
***************************

This style of door is little different then the other two types of doors. In this door style instead of selecting a reagon 
for a door to occupy a user simply types the command and places blocks. Anywhere in the same world as the command was 
called a block can be placed and that block will be incorporated into the hybrid door.

So to start:

/dhdoor mydoor

Start placing blocks.

When finished:

/dhdoor mydoor done

will complete the door and commit it to be saved.

The linking process is the same except instead of a 1 or a 2 you simply use a 3 for the argument after the toggle.

EX:

/dlink trigger MyTrig2 mydoor toggle 3

In this way a trigger named MyTrig2 will be linked to the Hybrid door mydoor in the toggle mode.

The 3 signals blockdoor to look for the hybrid door. A 3rd state door style. Thats pretty much it for the hybrid door.

If you wish to add more blocks to the door simply type the hdoor command using the doors name and add blocks to it.

/dhdoor mydoor

and add blocks

/dhdoor mydoor done

when completed and the new blocks will be saved to the existing door.